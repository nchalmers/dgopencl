/* conserv_kernels.cl
 *
 * Contains the GPU variables and kernels to solve hyperbolic conservation laws in two-dimensions.
 * Stores all mesh variables and the following kernels
 * 
 * 1. precompuations
 * 2. surface integral 
 * 3. volume integral 
 * 4. limiter
 * 5. evaluate u 
 *
 */


/***********************
 *
 * PRECOMPUTING
 *
 ***********************/

/* side length computer
 *
 * precomputes the length of each side.
 * THREADS: num_sides
 */ 
__kernel void preval_side_length(__global double *s_length, 
                             __global double *s_V1x,__global double *s_V1y, 
                             __global double *s_V2x,__global double *s_V2y) {
    int idx = get_global_id(0);

    if (idx < num_sides) {
        // compute and store the length of the side
        s_length[idx] = sqrt(pow(s_V1x[idx] - s_V2x[idx],2) + pow(s_V1y[idx] - s_V2y[idx],2));
    }
}

/* minimum height computing
 *
 * computes the smallest height of each cell
 * 
 */

__kernel void preval_min_height(__global double *r,
                                   __global double *V1x,__global double *V1y,
                                   __global double *V2x,__global double *V2y,
                                   __global double *V3x,__global double *V3y) {
    int idx = get_global_id(0);

    if (idx < num_elem) {
        double a, b, c, k;

        //Twice the area
        k = (V2x[idx]-V1x[idx])*(V3y[idx]-V1y[idx])-(V3x[idx]-V1x[idx])*(V2y[idx]-V1y[idx]);

        //Length of each side
        a = sqrt(pow(V1x[idx] - V2x[idx], 2) + pow(V1y[idx] - V2y[idx], 2));
        b = sqrt(pow(V2x[idx] - V3x[idx], 2) + pow(V2y[idx] - V3y[idx], 2));
        c = sqrt(pow(V1x[idx] - V3x[idx], 2) + pow(V1y[idx] - V3y[idx], 2));

        //Smallest height
        r[idx] = min(min(k/a,k/b),k/c);
    }
}


/* jacobian computing
 *
 * precomputes the jacobian determinant for each element.
 * THREADS: num_elem
 */
__kernel void preval_jacobian(__global double *J, 
                          __global double *V1x,__global double *V1y, 
                          __global double *V2x,__global double *V2y, 
                          __global double *V3x,__global double *V3y) {
    int idx = get_global_id(0);

    if (idx < num_elem) {
        double x1, y1, x2, y2, x3, y3;

        // read vertex points
        x1 = V1x[idx];
        y1 = V1y[idx];
        x2 = V2x[idx];
        y2 = V2y[idx];
        x3 = V3x[idx];
        y3 = V3y[idx];

        // calculate jacobian determinant
        J[idx] = (x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1);
    }
}

/* evaluate normal vectors
 *
 * computes the normal vectors for each element along each side.
 * THREADS: num_sides
 *
 */
__kernel void preval_normals(__global double *Nx,__global double *Ny, 
                         __global double *s_V1x,__global double *s_V1y, 
                         __global double *s_V2x,__global double *s_V2y) {

    int idx = get_global_id(0);

    if (idx < num_sides) {
        double x, y, l;
        double sv1x, sv1y, sv2x, sv2y;
    
        sv1x = s_V1x[idx];
        sv1y = s_V1y[idx];
        sv2x = s_V2x[idx];
        sv2y = s_V2y[idx];
    
        // lengths of the vector components
        x = sv2x - sv1x;
        y = sv2y - sv1y;
    
        // normalize
        l = sqrt(pow(x, 2) + pow(y, 2));

        //normals point away from the left element
        // store the result
        Nx[idx] =  y / l;
        Ny[idx] = -x / l;
    }
}

__kernel void preval_normals_direction(__global double *Nx,__global double *Ny, 
                         __global double *V1x,__global double *V1y, 
                         __global double *V2x,__global double *V2y, 
                         __global double *V3x,__global double *V3y,
                         __global int *left_elem,__global int *left_side_number) {

    int idx = get_global_id(0);

    if (idx < num_sides) {
        double new_x, new_y, ddot;
        double initial_x, initial_y, target_x, target_y;
        double x, y;
        int left_idx, side;

        // get left side's vertices
        left_idx = left_elem[idx];
        side     = left_side_number[idx];

        // get the normal vector
        x = Nx[idx];
        y = Ny[idx];
    
        // make it point the correct direction by learning the third vertex point
        switch (side) {
            case 0: 
                target_x = V3x[left_idx];
                target_y = V3y[left_idx];
                initial_x = (V1x[left_idx] + V2x[left_idx]) / 2.;
                initial_y = (V1y[left_idx] + V2y[left_idx]) / 2.;
                break;
            case 1:
                target_x = V1x[left_idx];
                target_y = V1y[left_idx];
                initial_x = (V2x[left_idx] + V3x[left_idx]) / 2.;
                initial_y = (V2y[left_idx] + V3y[left_idx]) / 2.;
                break;
            case 2:
                target_x = V2x[left_idx];
                target_y = V2y[left_idx];
                initial_x = (V1x[left_idx] + V3x[left_idx]) / 2.;
                initial_y = (V1y[left_idx] + V3y[left_idx]) / 2.;
                break;
        }

        // create the vector pointing towards the third vertex point
        new_x = target_x - initial_x;
        new_y = target_y - initial_y;

        // find the dot product between the normal and new vectors
        ddot = x * new_x + y * new_y;
        
        if (ddot > 0) {
            Nx[idx] *= -1;
            Ny[idx] *= -1;
        }
    }
}

__kernel void preval_partials(__global double *V1x,__global double *V1y,
                              __global double *V2x,__global double *V2y,
                              __global double *V3x,__global double *V3y,
                              __global double *xr, __global  double *yr,
                              __global double *xs, __global  double *ys) {
    int idx = get_global_id(0);
    if (idx < num_elem) {
        // evaulate the jacobians of the mappings for the chain rule
        // x = x2 * r + x3 * s + x1 * (1 - r - s)
        xr[idx] = V2x[idx] - V1x[idx];
        yr[idx] = V2y[idx] - V1y[idx];
        xs[idx] = V3x[idx] - V1x[idx];
        ys[idx] = V3y[idx] - V1y[idx];
    }
}


/* initial conditions
 *
 * computes the coefficients for the initial conditions
 * THREADS: num_elem
 */
double L2_projection(double *V, int i, int n) {
    int j;
    double X,Y;

    double C = 0.;
	double U[M];
	//Numerically integrate U0 times the i'th basis function

    for (j = 0; j < n_quad; j++) {
        // get the 2d point on the mesh
        get_coordinates_2d(&X, &Y, V, j);
		
		U0(U,X,Y);
		
        // evaluate U at the integration point
        C += w[j] * U[n] * basis[i * n_quad + j];
    }
    return C;
}

 
__kernel void initial_conditions(__global double *c,  __global double *J,
                                 __global double *V1x,__global double *V1y,
                                 __global double *V2x,__global double *V2y,
                                 __global double *V3x,__global double *V3y) {
    int idx = get_global_id(0);
    int i, n;
    double V[6];

	if (idx < num_elem) {
        V[0] = V1x[idx];
        V[1] = V1y[idx];
        V[2] = V2x[idx];
        V[3] = V2y[idx];
        V[4] = V3x[idx];
        V[5] = V3y[idx];

        for (i = 0; i < NP; i++) {
             for (n = 0; n < M; n++) {
                 c[num_elem * NP * n + i * num_elem + idx] = L2_projection(V, i, n);
             }
        } 
    }
}

//gets the grid coordinates at the j'th integration point for 2d 
 void get_coordinates_2d(double *x, double *y, double *V, int j) {
    // x = x2 * r + x3 * s + x1 * (1 - r - s)
    *x = V[2] * r1[j] + V[4] * r2[j] + V[0] * (1 - r1[j] - r2[j]);
    *y = V[3] * r1[j] + V[5] * r2[j] + V[1] * (1 - r1[j] - r2[j]);
}

 //gets the grid cooridinates at the j'th integration point for 1d 
 void get_coordinates_1d(double *x, double *y, double *V, int j, int left_side) {

    double r1_eval, r2_eval;

    // we need the mapping back to the grid space
    switch (left_side) {
        case 0: 
            r1_eval = 0.5 + 0.5 * r_oned[j];
            r2_eval = 0.;
            break;
        case 1: 
            r1_eval = (1. - r_oned[j]) / 2.;
            r2_eval = (1. + r_oned[j]) / 2.;
            break;
        case 2: 
            r1_eval = 0.;
            r2_eval = 0.5 + 0.5 * r_oned[n_quad1d - 1 - j];
            break;
    }

    // x = x2 * r + x3 * s + x1 * (1 - r - s)
    *x = V[2] * r1_eval + V[4] * r2_eval + V[0] * (1 - r1_eval - r2_eval);
    *y = V[3] * r1_eval + V[5] * r2_eval + V[1] * (1 - r1_eval - r2_eval);
}
 

/***********************
 *
 * BOUNDARY CONDITIONS
 *
 ***********************/
/* Put the boundary conditions for the problem in here.
*/
void inflow_boundary(double *U_left, double *U_right,
                                double *V, double nx, double ny,
                                int j, int left_side, double t) {

    double X, Y;

    // get x, y coordinates
    get_coordinates_1d(&X, &Y, V, j, left_side);

    U_inflow(U_right, X, Y, t);
}

void outflow_boundary(double *U_left, double *U_right,
                                 double *V, double nx, double ny,
                                 int j, int left_side, double t) {
    double X, Y;
    

    // get x, y coordinates
    get_coordinates_1d(&X, &Y, V, j, left_side);
    
    U_outflow(U_right, X, Y, t);
}

void reflecting_boundary(double *U_left, double *U_right,
                                    double *V, double nx, double ny, 
                                    int j, int left_side, double t) {
    double X, Y;

    // get x, y coordinates
    get_coordinates_1d(&X, &Y, V, j, left_side);
    
    U_reflection(U_left, U_right, X, Y, t, nx, ny);
}

/***********************
 *
 * MAIN FUNCTIONS
 *
 ***********************/

/* limiter
 *
 * the standard Barth-Jespersen limiter for p = 1 
 * TODO: The limiter for Euler should use the characteristic variables
 *
 * THREADS: num_elem
 */
 /*
__kernel void limit_c(__global double *C,__global int *elem_s1,
					  __global int *elem_s2,__global int *elem_s3,
                      __global int *left_side_idx,__global int *right_side_idx) {

    int idx = get_global_id(0);
    if (idx < num_elem) { 
        double U_i, U_c, Umin, Umax, alpha, min_alpha;
        int s1, s2, s3;
        int n, i, j, side;
        int neighbor_idx[3];
        // get element neighbors
        s1 = elem_s1[idx];
        s2 = elem_s2[idx];
        s3 = elem_s3[idx];

        // get element neighbor indexes
        neighbor_idx[0] = (left_side_idx[s1] == idx) ? right_side_idx[s1] : left_side_idx[s1];
        neighbor_idx[1] = (left_side_idx[s2] == idx) ? right_side_idx[s2] : left_side_idx[s2];
        neighbor_idx[2] = (left_side_idx[s3] == idx) ? right_side_idx[s3] : left_side_idx[s3];
        
        // make sure we aren't on a boundary element
        for (i = 0; i < 3; i++) {
            neighbor_idx[i] = (neighbor_idx[i] < 0) ? idx : neighbor_idx[i];
        }

        for (n = 0; n < M; n++) {
            // set initial stuff
            U_c = C[num_elem*NP*n + idx] * basis[0];
            Umin = U_c;
            Umax = U_c;

            // get delmin and delmax
            for (i = 0; i < 3; i++) {
                U_i = C[num_elem*NP*n + neighbor_idx[i]] * basis[0];
                
                Umin = (U_i < Umin) ? U_i : Umin;
                Umax = (U_i > Umax) ? U_i : Umax;
            }

            min_alpha = 1.;

			// Gauss points
            for (j = 0; j < n_quad1d; j++){
                for (side = 0; side < 3; side++) {
                	
                    // evaluate U
                    U_i = 0.;
                    for (i = 0; i < NP; i++) {
                        U_i += C[num_elem*NP*n + i*num_elem + idx] 
                             * basis_side[side * n_quad1d * NP + i * n_quad1d + j];
                    }
    				                
                    // pick the correct min_phi
                    if (U_i > U_c) {
                        alpha = min(1., (U_c - Umin)/(U_i - U_c));
                    } else if (U_i < U_c) {
                        alpha = min(1., (U_c - Umax)/(U_i - U_c));
                    } else {
                    	alpha = 1.;
                    }
      
                    // find min_phi
                    min_alpha = (alpha < min_alpha) ? alpha : min_alpha;
                    
                }
            }

            if (min_alpha < 0) {
                min_alpha = 0.;
            }

            // limit the slope
            C[NP * num_elem * n + 1 * num_elem + idx] *= min_alpha;
            C[NP * num_elem * n + 2 * num_elem + idx] *= min_alpha;
        }
    }
}
*/
/* limiter
 *
 * new limiter for p = 1 
 *
 * THREADS: num_elem
 */

double minmod(double a, double b, double c) {

    if ((sign(a) == sign(b)) && (sign(b) == sign(c))) {

        return sign(a)*min(min(fabs(a),fabs(b)),fabs(c));
    } else {
        return 0.;
    }

}

__kernel void limit_c(__global double *C,__global int *elem_s1,
                      __global int *elem_s2,__global int *elem_s3,
                      __global int *left_side_idx,__global int *right_side_idx) {

    int idx = get_global_id(0);
    if (idx < num_elem) { 
        double U0, U1, U2, U3;
        double S1, S2, S3; 
        int s1, s2, s3;
        int n;
        int neighbor_idx[3];
        // get element neighbors
        s1 = elem_s1[idx];
        s2 = elem_s2[idx];
        s3 = elem_s3[idx];

        // get element neighbor indexes
        neighbor_idx[0] = (left_side_idx[s1] == idx) ? right_side_idx[s1] : left_side_idx[s1];
        neighbor_idx[1] = (left_side_idx[s2] == idx) ? right_side_idx[s2] : left_side_idx[s2];
        neighbor_idx[2] = (left_side_idx[s3] == idx) ? right_side_idx[s3] : left_side_idx[s3];

        for (n = 0; n < M; n++) {

            // set initial stuff
            U0 = C[num_elem*NP*n + idx] * basis[0];
            S1 = C[NP * num_elem * n + 1 * num_elem + idx] - sqrt(3.)*C[NP * num_elem * n + 2 * num_elem + idx];
            S2 = C[NP * num_elem * n + 1 * num_elem + idx] + sqrt(3.)*C[NP * num_elem * n + 2 * num_elem + idx];
            S3 = -2*C[NP * num_elem * n + 1 * num_elem + idx];

            //check for boundary
            if (neighbor_idx[0] < 0) {
                U1 = S1 + U0;
            } else {
                U1 = C[num_elem*NP*n + neighbor_idx[0]] * basis[0];
            }
            if (neighbor_idx[1] < 0) {
                U2 = S2 + U0;
            } else {
                U2 = C[num_elem*NP*n + neighbor_idx[1]] * basis[0];
            }
            if (neighbor_idx[2] < 0) {
                U3 = S3 + U0;
            } else {
                U3 = C[num_elem*NP*n + neighbor_idx[2]] * basis[0];
            }

            //minmod along each median
            S1 = minmod(U1-U0, S1, U1-U0);
            S2 = minmod(U2-U0, S2, U2-U0);
            S3 = minmod(U3-U0, S3, U3-U0);

            double V1, V2, V3;
            V1 = minmod(-S2-S3, S1, S1);
            V2 = minmod(-S1-S3, S2, S2);
            V3 = minmod(-S1-S2, S3, S3);

            S1 = V1;
            S2 = V2;
            S3 = V3;

            /*
            double sigma;
            //fix smallest slope and find the next slope component
            if ( (fabs(S1) <= fabs(S2)) && (fabs(S1) <= fabs(S3)) ) {
                sigma = minmod(S2+S1/5, -S3-4*S1/5, -S3-4*S1/5);
                S2 = sigma-S1/5;
                C[NP * num_elem * n + 1 * num_elem + idx] = (S1+S2)/2;
                C[NP * num_elem * n + 2 * num_elem + idx] = (S2-S1)/(2*sqrt(3.));
            } else if ( (fabs(S2) <= fabs(S1)) && (fabs(S2) <= fabs(S3)) ) {
                sigma = minmod(S1+S2/2, -S3-S2/2, -S3-S2/2);
                S3 = -sigma-S2/2;
                C[NP * num_elem * n + 1 * num_elem + idx] = -S3/2;
                C[NP * num_elem * n + 2 * num_elem + idx] = (S2+S3/2)/sqrt(3.);
            } else if ( (fabs(S3) <= fabs(S2)) && (fabs(S3) <= fabs(S1)) ) {
                sigma = minmod(S1+4*S3/5, -S2-S3/5, -S2-S3/5);
                S1 = sigma-4*S3/5;
                C[NP * num_elem * n + 1 * num_elem + idx] = -S3/2;
                C[NP * num_elem * n + 2 * num_elem + idx] = -(S1+S3/2)/sqrt(3.);
            }
    */
            
            //recontruct slope using the two smallest median slopes
            if ((fabs(S3)>=fabs(S1)) && (fabs(S3)>=fabs(S2))){
                C[NP * num_elem * n + 1 * num_elem + idx] = (S1+S2)/2;
                C[NP * num_elem * n + 2 * num_elem + idx] = (S2-S1)/(2*sqrt(3.));
            } else if ((fabs(S2)>=fabs(S1)) && (fabs(S2)>=fabs(S3))) {
                C[NP * num_elem * n + 1 * num_elem + idx] = -S3/2;
                C[NP * num_elem * n + 2 * num_elem + idx] = -(S1+S3/2)/sqrt(3.);
            } else {
                C[NP * num_elem * n + 1 * num_elem + idx] = -S3/2;
                C[NP * num_elem * n + 2 * num_elem + idx] = (S2+S3/2)/sqrt(3.);
            }
            
        }
    }
}


void eval_boundary(double *U_left, double *U_right, 
                              double *V, double nx, double ny,
                              int j, int left_side, double t, int right_idx) {
    switch (right_idx) {
        // reflecting 
        case -1: 
            reflecting_boundary(U_left, U_right,
                V, nx, ny,
                j, left_side, t);
            break;
        // outflow 
        case -2: 
            outflow_boundary(U_left, U_right,
                V, nx, ny,
                j, left_side, t);
            break;
        // inflow 
        case -3: 
            inflow_boundary(U_left, U_right,
                V, nx, ny, 
                j, left_side, t);
            break;
    }
}

/* left & right evaluator
 * 
 * calculate U_left and U_right at the integration point,
 * using boundary conditions when necessary.
 */
 void eval_left_right(__global double *C, double *C_left, double *C_right, 
                             double *U_left, double *U_right,
                             double nx, double ny,
                             double *V, int j, 
                             int left_side, int right_side,
                             int left_idx, int right_idx,
                             double t) { 

    int i, n;

    // set U to 0
    for (n = 0; n < M; n++) {
        U_left[n]  = 0.;
        U_right[n] = 0.;
    }

    //evaluate U at the integration points
    for (i = 0; i < NP; i++) {
        for (n = 0; n < M; n++) {
            U_left[n] += C_left[n*NP + i] * 
                         basis_side[left_side * NP * n_quad1d + i * n_quad1d + j];
        }
    }

    // boundaries are sorted to avoid warp divergence
    if (right_idx < 0) {
        eval_boundary(U_left, U_right, V, nx, ny, j, left_side, t, right_idx);
    } else {
        // evaluate the right side at the integration point
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                U_right[n] += C_right[n*NP + i] * 
                              basis_side[right_side * NP * n_quad1d + i * n_quad1d + n_quad1d - j - 1];
            }
        }
    }
}

/* surface integral evaluation
 *
 * evaluate all the riemann problems for each element.
 * THREADS: num_sides
 */
__kernel void eval_surface(__global double *C, 
                  __global double *rhs_surface_left,__global double *rhs_surface_right, 
                  __global double *side_len,
                  __global double *V1x,__global double *V1y,
                  __global double *V2x,__global double *V2y,
                  __global double *V3x,__global double *V3y,
                  __global int *left_elem,__global int *right_elem,
                  __global int *left_side_number,__global int *right_side_number,
                  __global double *Nx,__global double *Ny, double t) {
    int idx = get_global_id(0);

    if (idx < num_sides) {
        int i, j, n;
        int left_idx, right_idx, left_side, right_side;
        double len, nx, ny;
        double C_left[M * NP], C_right[M * NP];
        double U_left[M], U_right[M];
        double F_n[M];
        double V[6];

		double surface_left[M*NP], surface_right[M*NP];

        
        
        /*
        double F_left[M], F_right[M];
        double alpha[10];

        alpha[0]=1.;
        
        alpha[1]=0.33;
        alpha[2]=0.33;

        alpha[3]=1.;
        alpha[4]=1.;
        alpha[5]=1.;

        alpha[6]=1.;
        alpha[7]=1.;
        alpha[8]=1.;
        alpha[9]=1.;
        */

		/*
		TODO: Idea for reducing the overflowing registers.
		- Compute UL and UR and F_n at one integration point and add it to the total.
		- Maybe put those computations in separate function? It's hard to know whether that will free the register memory. 
		- Loop over each integration point
		*/

        // read edge data
        len = side_len[idx];
        nx  = Nx[idx];
        ny  = Ny[idx];
        left_idx   = left_elem[idx];
        right_idx  = right_elem[idx];
        left_side  = left_side_number[idx];
        right_side = right_side_number[idx];

        // get verticies
        V[0] = V1x[left_idx];
        V[1] = V1y[left_idx];
        V[2] = V2x[left_idx];
        V[3] = V2y[left_idx];
        V[4] = V3x[left_idx];
        V[5] = V3y[left_idx];

        // read coefficients
        if (right_idx > -1) {
            for (n = 0; n < M; n++) {
                for (i = 0; i < NP; i++) {
                    C_left[n*NP + i]  = C[num_elem * NP * n + i * num_elem + left_idx];
                    C_right[n*NP + i] = C[num_elem * NP * n + i * num_elem + right_idx];
                }
            }
        } else { //Element on boundary
            for (n = 0; n < M; n++) {
                for (i = 0; i < NP; i++) {
                    C_left[n*NP + i]  = C[num_elem * NP * n + i * num_elem + left_idx];
                }
            }
        }

		// set RHS to 0
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                surface_left[NP * n + i]  = 0.;
                surface_right[NP * n + i] = 0.;
            }
        }

        // at each integration point
        for (j = 0; j < n_quad1d; j++) {

            // calculate the left and right values along the surface
            eval_left_right(C, C_left, C_right,
                            U_left, U_right,
                            nx, ny,
                            V, j, left_side, right_side,
                            left_idx, right_idx, t);

            // compute F_n(U_left, U_right)
            riemann_solver(F_n, U_left, U_right, V, t, nx, ny, j, left_side);

            /*
            // compute F_left and F_right)
            riemann_solver(F_left, U_left, U_left, V, t, nx, ny, j, left_side);
            // compute F_n(U_left, U_right)
            riemann_solver(F_right, U_right, U_right, V, t, nx, ny, j, left_side);
            */

            // multiply across by phi_i at this integration point
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    //surface_left[NP * n + i]  += -len / 2 * (w_oned[j] * (alpha[i]*F_n[n]+(1.-alpha[i])*F_left[n]) * basis_side[left_side  * NP * n_quad1d + i * n_quad1d + j]);
                    //surface_right[NP * n + i] +=  len / 2 * (w_oned[j] * (alpha[i]*F_n[n]+(1.-alpha[i])*F_right[n]) * basis_side[right_side * NP * n_quad1d + i * n_quad1d + n_quad1d - 1 - j]);
                    surface_left[NP * n + i]  += -len / 2 * (w_oned[j] * F_n[n] * basis_side[left_side  * NP * n_quad1d + i * n_quad1d + j]);
                    surface_right[NP * n + i] +=  len / 2 * (w_oned[j] * F_n[n] * basis_side[right_side * NP * n_quad1d + i * n_quad1d + n_quad1d - 1 - j]);
                }
            } 
        } 
        
        
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                rhs_surface_left[num_sides * NP * n + i * num_sides + idx]  = surface_left[NP*n+i];
                rhs_surface_right[num_sides * NP * n + i*num_sides + idx] = surface_right[NP*n+i];
            }
        }
    } 
}

/* volume integrals
 *
 * evaluates and adds the volume integral to the rhs vector
 * THREADS: num_elem
 */
__kernel void eval_volume(__global double *C,__global double *rhs_volume, 
                          __global double *Xr,__global double *Yr,
                          __global double *Xs,__global double *Ys,
                          __global double *V1x,__global double *V1y,
                          __global double *V2x,__global double *V2y,
                          __global double *V3x,__global double *V3y,
                          __global double *J, double t) {
    int idx = get_global_id(0);

    if (idx < num_elem) {
        double V[6];
        double S[M];
        double C_left[M * NP];
        int i, j, k, n;
        double U[M];
        double flux_x[M], flux_y[M];
        double xr, yr, xs, ys;
        double detJ;

		double volume[M * NP];

		/*
		TODO: Idea for reducing the overflowing registers.
		- Compute F_n at each integration point and store. (This is NxN_quad, not NxNP) (Marginal difference)
		- Maybe put those computations in separate function? It's hard to know whether that will free the register memory. 

		- Loop over each integration point
		
		*/

        // read coefficients
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                C_left[n*NP + i]  = C[num_elem * NP * n + i * num_elem + idx];
            }
        }

        // get element data
        xr = Xr[idx];
        yr = Yr[idx];
        xs = Xs[idx];
        ys = Ys[idx];

        // get jacobian determinant
        detJ = J[idx];
        
        // get verticies
        V[0] = V1x[idx];
        V[1] = V1y[idx];
        V[2] = V2x[idx];
        V[3] = V2y[idx];
        V[4] = V3x[idx];
        V[5] = V3y[idx];

        // set to 0
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                volume[NP * n + i] = 0.;
            }
        }

        // for each integration point
        for (j = 0; j < n_quad; j++) {

            // initialize to zero
            for (n = 0; n < M; n++) {
                U[n] = 0.;
            }

            // calculate at the integration point
            for (k = 0; k < NP; k++) {
                for (n = 0; n < M; n++) {
                    U[n] += C_left[n*NP + k] * basis[n_quad * k + j];
                }
            }

            // evaluate the flux
            eval_flux(U, flux_x, flux_y, V, t, j, -1);

            // get the source term
            source_term(S, U, V, t, j);

            // multiply across by phi_i
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    // add the sum
                    //     [fx fy] * [y_s, -y_r; -x_s, x_r] * [phi_x phi_y]
                    volume[NP * n + i] += 
                              flux_x[n] * ( basis_grad_x[n_quad * i + j] * ys
                                           -basis_grad_y[n_quad * i + j] * yr)
                            + flux_y[n] * (-basis_grad_x[n_quad * i + j] * xs 
                                          + basis_grad_y[n_quad * i + j] * xr);

                    // add the source term S * phi_i
                    volume[NP * n + i] += 
                            S[n] * basis[n_quad * i + j] * w[j] * detJ;
                }
            }
        }
        
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                rhs_volume[num_elem * NP * n + i * num_elem + idx] = volume[NP * n + i];
            }
        }       
    }
}

/***********************
 * ASSEMBLE RHS FUNCTIONS
 ***********************/

/* right hand side
 *
 * computes the sum of the quadrature and the riemann flux for the 
 * coefficients for each element
 * THREADS: num_elem
 */


__kernel void eval_rhs(__global double *k,__global double *rhs_volume,
					   __global double *rhs_surface_left,__global double *rhs_surface_right, 
                       __global int *elem_s1,__global int *elem_s2,__global int *elem_s3,
                       __global int *left_elem,__global double *J, double dt) {
    int idx = get_global_id(0);
    double j;
    int i, s1_idx, s2_idx, s3_idx;
    int n;
    
    /*
	TODO: Idea for reducing the memory usage
	
	- Why do we even use rhs_volume? Isn't this redundant? Why not just use k?
	
	- Each side has an index 1, 2, or 3 in its left and right cell. This gives a partitioning of the edge list... Albeit not the most efficient one. 
	- Essentially it would look like 
		- Add all the edges which are side one
		- Sync threads
		- Add all the edges which are side two
		- Sync threads
		- Add all the edges which are side three
	- This is for the left and right side indicies. It would cause some warp divergence, but it should mean we don't need the _surface variables and don't have a race conditon.
	*/
    
    
    double K[M*NP];

    if (idx < num_elem) {

        // set to 0
        for (i = 0; i < NP; i++) { 
            for (n = 0; n < M; n++) {
                K[NP * n + i] = 0.;
            }
        }

        // read jacobian determinant
        j = J[idx];

        // get the indicies for the riemann contributions for this element
        s1_idx = elem_s1[idx];
        s2_idx = elem_s2[idx];
        s3_idx = elem_s3[idx];
        
        

        // add volume integral
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                K[NP * n + i] += dt/j*rhs_volume[num_elem * NP * n + i * num_elem + idx];
            }
        }

        // for the first edge, add either left or right surface integral
        if (idx == left_elem[s1_idx]) {
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    K[NP * n + i] += dt/j*rhs_surface_left[num_sides*NP*n+i*num_sides + s1_idx];
                }
            }
        } else {
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    K[NP * n + i] += dt/j*rhs_surface_right[num_sides*NP*n+i*num_sides + s1_idx];
                }
            }
        }
        // for the second edge, add either left or right surface integral
        if (idx == left_elem[s2_idx]) {
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    K[NP * n + i] += dt/j*rhs_surface_left[num_sides*NP*n+i*num_sides + s2_idx];
                }
            }
        } else {
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    K[NP * n + i] += dt/j*rhs_surface_right[num_sides*NP*n+i*num_sides + s2_idx];
                }
            }
        }
       // for the third edge, add either left or right surface integral
        if (idx == left_elem[s3_idx]) {
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    K[NP * n + i] += dt/j*rhs_surface_left[num_sides*NP*n+i * num_sides + s3_idx];
                }
            }
        } else {
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    K[NP * n + i] += dt/j*rhs_surface_right[num_sides*NP*n+i * num_sides + s3_idx];
                }
            }
        }
     
        //Write to K
        for (i = 0; i < NP; i++) { 
            for (n = 0; n < M; n++) {
                k[num_elem * NP * n + i * num_elem + idx] = K[NP * n + i];
            }
        }
    }
}



/***********************
 * RK
 ***********************/

/* tempstorage for RK
 * 
 * I need to store u + alpha * k_i into some temporary variable called kstar
 */
__kernel void rk_add(__global double *kstar,__global double *c,__global double *k, double a) {
    int idx = get_global_id(0);
    int i, n;

    if (idx < num_elem) {
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                kstar[num_elem * NP * n + i * num_elem + idx] = c[num_elem * NP * n + i * num_elem + idx] + a * k[num_elem * NP * n + i * num_elem + idx];
            }
        }
    }
}


/* evaluate u
 * 
 * evaluates rho and E at the three vertex points for output
 * THREADS: num_elem
 */
__kernel void eval_u(__global double *C, __global double *Uv1,
                     __global double *Uv2, __global double *Uv3, int n) {
    int idx = get_global_id(0);
    int i;
    double uv1, uv2, uv3;
        
    if (idx < num_elem) {
        // calculate values at the integration points
        uv1 = 0.;
        uv2 = 0.;
        uv3 = 0.;
        for (i = 0; i < NP; i++) {
            uv1 += C[num_elem * NP * n + i * num_elem + idx] * basis_vertex[i * 3 + 0];
            uv2 += C[num_elem * NP * n + i * num_elem + idx] * basis_vertex[i * 3 + 1];
            uv3 += C[num_elem * NP * n + i * num_elem + idx] * basis_vertex[i * 3 + 2];
        }

        // store result
        Uv1[idx] = uv1;
        Uv2[idx] = uv2;
        Uv3[idx] = uv3;
    }
}