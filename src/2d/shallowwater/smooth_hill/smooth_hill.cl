
#define PI 3.14159

/***********************
 *
 * INITIAL CONDITIONS
 *
 ***********************/

/* initial condition function
 *
 * returns the value of the intial condition at point x,y
 */

void U0(double *U, double x, double y) {
    double x0, y0, r;

    x0 = 0.5;
    y0 = 0.5;
    r  = 0.1;

    U[0] = 10 + exp(-(pow(x - x0,2) + pow(y - y0,2))/(2*r*r));
    U[1] = 0.;
    U[2] = 0.;
}

/***********************
*
* INFLOW CONDITIONS
*
************************/

void U_inflow(double *U, double x, double y, double t) {
    U0(U, x, y);
}

/***********************
*
* OUTFLOW CONDITIONS
*
************************/

void U_outflow(double *U, double x, double y, double t) {
    // there are no outflow boundaries in this problem 
}

/***********************
*
* REFLECTING CONDITIONS
*
************************/

void U_reflection(double *U_left, double *U_right, 
                             double x, double y, double t,
                             double nx, double ny) {
    double ddot;

    // set h to be the same
    U_right[0] = U_left[0];

    // and reflect the velocities
    ddot = U_left[1] * nx + U_left[2] * ny;

    U_right[1] = U_left[1] - 2*ddot*nx;
    U_right[2] = U_left[2] - 2*ddot*ny;

}
/***********************
*
* EXACT SOLUTION
*
************************/

void U_exact(double *U, double x, double y, double t) {
    // no exact solution
}

