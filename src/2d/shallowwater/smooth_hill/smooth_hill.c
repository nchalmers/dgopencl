#include "../../main.c"

/* smooth_hill.c
 *
 * shallow water equations
 *
 */

#define PI 3.14159


int limiter = LIMITER;  
double CFL =1.;
int M = 3;

/***********************
 *
 * MAIN FUNCTION
 *
 ***********************/



int main(int argc, char *argv[]) {
    run_dgopencl(argc, argv,"shallowwater.cl","smooth_hill/smooth_hill.cl");
}