
/* shallowwater.cl
 *
 * This file contains the relevant information for making a system to solve
 * the shallow water equations
 *
 * d_t [  h  ] + d_x [       u*h         ] + d_y [        v*h        ] = 0
 * d_t [ u*h ] + d_x [ h*u^2 + 0.5*G*h^2 ] + d_y [       u*v*h       ] = 0
 * d_t [ v*h ] + d_x [      u*v*h        ] + d_y [ h*v^2 + 0.5*G*h^2 ] = 0
 *
 */


#define G 9.8

void get_coordinates_2d(double *x, double *y, double *V, int j);

void get_coordinates_1d(double *x, double *y, double *V, int j, int left_side);


/***********************
 *
 * INITIAL CONDITIONS
 *
 ***********************/

void evalU0(double *U, double *V, int i) {
    int j;
    double X,Y;
    double u0[3];

    U[0] = 0.;
    U[1] = 0.;
    U[2] = 0.;

    for (j = 0; j < n_quad; j++) {

        // get the 2d point on the mesh
        get_coordinates_2d(&X, &Y, V, j);

        // evaluate U0 here
        U0(u0, X, Y);

        // evaluate U at the integration point
        U[0] += w[j] * u0[0] * basis[i * n_quad + j];
        U[1] += w[j] * u0[1] * basis[i * n_quad + j];
        U[2] += w[j] * u0[2] * basis[i * n_quad + j];
    }
}

double eval_c(double *U) {
    return sqrt(U[0]*G);
}

bool is_physical(double *U) {
    return U[0] >= 0.;
}

/* check physical
 *
 * if U isn't physical, replace the solution with the constant average value
 */
__kernel void check_physical(__global double *C_global) {
    int i,j,n,k,side;
    int idx = get_global_id(0);
    double C[M*NP];
    double U[M];

    if (idx < num_elem) {

        //Read in global coefficients
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                C[n*NP + i]  = C_global[num_elem * NP * n + i * num_elem + idx];
            }
        }

        for (j = 0; j < n_quad; j++) {

            // initialize to zero
            for (n = 0; n < M; n++) {
                U[n] = 0.;
            }

            // calculate at the integration point
            for (k = 0; k < NP; k++) {
                for (n = 0; n < M; n++) {
                    U[n] += C[n*NP + k] * basis[n_quad * k + j];
                }
            }

            // check to see if U is physical
            if (!is_physical(U)) {
                // set C[1] to C[NP] to zero
                for (i = 1; i < NP; i++) {
                    C_global[num_elem * NP * 0 + i * num_elem + idx] = 0.;
                    C_global[num_elem * NP * 1 + i * num_elem + idx] = 0.;
                    C_global[num_elem * NP * 2 + i * num_elem + idx] = 0.;

                    C[NP * 0 + i] = 0.;
                    C[NP * 1 + i] = 0.;
                    C[NP * 2 + i] = 0.;
                }

                // rebuild the solution as simply the average value
                U[0] = C[NP * 0 + 0] * basis[0];
                U[1] = C[NP * 1 + 0] * basis[0];
                U[2] = C[NP * 2 + 0] * basis[0];
                return;
            }
        }

        for (side =0;side<3;side++)
            for (j = 0; j < n_quad1d; j++) {
                // set U to 0
                for (n = 0; n < M; n++) {
                    U[n]  = 0.;
                }

                //evaluate U at the integration points
                for (i = 0; i < NP; i++) {
                    for (n = 0; n < M; n++) {
                        U[n] += C[n*NP + i] * basis_side[side * NP * n_quad1d + i * n_quad1d + j];
                    }
                }

                if (!is_physical(U)) {
                // set C[1] to C[NP] to zero
                for (i = 1; i < NP; i++) {
                    C_global[num_elem * NP * 0 + i * num_elem + idx] = 0.;
                    C_global[num_elem * NP * 1 + i * num_elem + idx] = 0.;
                    C_global[num_elem * NP * 2 + i * num_elem + idx] = 0.;

                    C[NP * 0 + i] = 0.;
                    C[NP * 1 + i] = 0.;
                    C[NP * 2 + i] = 0.;
                }

                // rebuild the solution as simply the average value
                U[0] = C[NP * 0 + 0] * basis[0];
                U[1] = C[NP * 1 + 0] * basis[0];
                U[2] = C[NP * 2 + 0] * basis[0];
                return;
            }
        }
        
    }
}


/***********************
 *
 * SHALLOWWATER FLUX
 *
 ***********************/
 /*
  * sets the flux for advection
 */
void eval_flux(double *U, double *flux_x, double *flux_y,
                          double *V, double t, int j, int left_side) {

    double h, uh, vh;

    h  = U[0];
    uh = U[1];
    vh = U[2];

    // flux_1 
    flux_x[0] = uh;
    flux_x[1] = uh*uh/h + 0.5*G*h*h;
    flux_x[2] = uh*vh/h;

    // flux_2
    flux_y[0] = vh;
    flux_y[1] = uh*vh/h;
    flux_y[2] = vh*vh/h + 0.5*G*h*h;
}

/***********************
 *
 * RIEMAN SOLVER
 *
 ***********************/
/* finds the max absolute value of the jacobian for F(u).
 */
double eval_lambda(double *U_left, double *U_right,
                              double *V,      double t,
                              double nx,      double ny,
                              int j, int left_side) {
                              
    double s_left, s_right;
    double c_left, c_right;
    double u_left, v_left;
    double u_right, v_right;
    double left_max, right_max;

    // get c for both sides
    c_left  = eval_c(U_left);
    c_right = eval_c(U_right);

    // find the speeds on each side
    u_left  = U_left[1] / U_left[0];
    v_left  = U_left[2] / U_left[0];
    u_right = U_right[1] / U_right[0];
    v_right = U_right[2] / U_right[0];
    s_left  = nx * u_left  + ny * v_left;
    s_right = nx * u_right + ny * v_right; 
    
    // if speed is positive, want s + c, else s - c
    if (s_left > 0.) {
        left_max = s_left + c_left;
    } else {
        left_max = -s_left + c_left;
    }

    // if speed is positive, want s + c, else s - c
    if (s_right > 0.) {
        right_max = s_right + c_right;
    } else {
        right_max = -s_right + c_right;
    }

    // return the max absolute value of | s +- c |
    if (fabs(left_max) > fabs(right_max)) {
        return fabs(left_max);
    } else { 
        return fabs(right_max);
    }
}

/* local lax-friedrichs riemann solver
 */
void riemann_solver(double *F_n, double *U_left, double *U_right,
                               double *V, double t,
                               double nx, double ny,
                               int j, int left_side) {
    int n;

    double flux_x_l[3], flux_x_r[3];
    double flux_y_l[3], flux_y_r[3];

    // calculate the left and right fluxes
    eval_flux(U_left, flux_x_l, flux_y_l, V, t, j, left_side);
    eval_flux(U_right, flux_x_r, flux_y_r, V, t, j, left_side);

    double lambda = eval_lambda(U_left, U_right, V, t, nx, ny, j, left_side);

    // calculate the riemann problem at this integration point
    for (n = 0; n < M; n++) {
        F_n[n] = 0.5 * ((flux_x_l[n] + flux_x_r[n]) * nx + (flux_y_l[n] + flux_y_r[n]) * ny 
                    + lambda * (U_left[n] - U_right[n]));
    }
}

/***********************
 *
 * CFL CONDITION
 *
 ***********************/
/* global lambda evaluation
 *
 * computes the max eigenvalue of |u + c|, |u|, |u - c|.
 */
__kernel void eval_global_lambda(__global double *C,__global double *lambda, 
                                 __global double *V1x,__global double *V1y,
                                 __global double *V2x,__global double *V2y,
                                 __global double *V3x,__global double *V3y,
                                 __global double *r, double t) {
    int idx = get_global_id(0);
    if (idx < num_elem) { 
        double c, s;

        double U[3];
        // get cell averages
        U[0] = C[num_elem * NP * 0 + idx] * basis[0];
        U[1] = C[num_elem * NP * 1 + idx] * basis[0];
        U[2] = C[num_elem * NP * 2 + idx] * basis[0];

        // evaluate c
        c = eval_c(U);

        // speed of the wave
        s = sqrt(U[1]*U[1] + U[2]*U[2])/U[0];

        // return the max eigenvalue
        if (s > 0) {
            lambda[idx] = r[idx]/(s + c);
        } else {
            lambda[idx] = r[idx]/(-s + c);
        }
    }
}

/*
 * source term
 */
void source_term(double *S, double *U, double *V, double t, int j) {
    int n;
    for (n = 0; n < M; n++) {
        S[n] = 0.;
    }
}