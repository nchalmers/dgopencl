#include "../../main.c"

/* gaussian.c
 *
 * Burgers equation with gaussian pulse initial condition
 *
 */

#define PI 3.14159265358979323

int limiter = NO_LIMITER;  // no limiter
double CFL = 1.;
int local_N = 1;


/***********************
 *
 * MAIN FUNCTION
 *
 ***********************/

int main(int argc, char *argv[]) {
    run_dgopencl(argc, argv,"burgers.cl","gaussian/gaussian.cl");
}
