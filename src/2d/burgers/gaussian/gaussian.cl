
#define PI 3.14159265358979323


void get_velocity(double *A, double x, double y, double t) {
    A[0] = 1.;
    A[1] = 1.;
}


/***********************
 *
 * INITIAL CONDITIONS
 *
 ***********************/

/* initial condition function
 *
 * returns the value of the intial condition at point x,y
 */

void U0(double *U, double x, double y) {
    double sigma = 15.0;

    U[0] = exp(-sigma*((x-0.5)*(x-0.5) + (y-0.5)*(y-0.5)));
}

/***********************
*
* INFLOW CONDITIONS
*
************************/

void U_inflow(double *U, double x, double y, double t) {
    U[0] = 0.;
}

/***********************
*
* OUTFLOW CONDITIONS
*
************************/

void U_outflow(double *U, double x, double y, double t) {
    // there are no outflow boundaries in this problem 
}

/***********************
*
* REFLECTING CONDITIONS
*
************************/

void U_reflection(double *U_left, double *U_right, 
                             double x, double y, double t,
                             double nx, double ny) {
    // there are no reflecting boundaries in this problem
}

/***********************
*
* EXACT SOLUTION
*
************************/

void U_exact(double *U, double x, double y, double t) {
    //no exact solution
}


