
// * advection.cl
// *
// * This file contains the relevant information for making a system to solve
// *
// * d_t [ u ] + d_x [ au ] + d_y [ bu ] = 0
// * 
// *

void get_coordinates_2d(double *x, double *y, double *V, int j);

void get_coordinates_1d(double *x, double *y, double *V, int j, int left_side);


//***********************
// *
// * ADVECTION FLUX
// *
// ***********************/
 //
 //  sets the flux for advection
 //
void eval_flux(double *U, double *flux_x, double *flux_y, 
                          double *V, double t, int j, int left_side) {

    double A[2];
    double X,Y;

    // get the grid points on the mesh
    if (left_side >= 0) {
        get_coordinates_1d(&X, &Y, V, j, left_side);
    } else {
        get_coordinates_2d(&X, &Y, V, j);
    }

    get_velocity(A, X, Y, t);

    // flux_1 
    flux_x[0] = A[0] * U[0];

    // flux_2
    flux_y[0] = A[1] * U[0];
}

//***********************
// *
// * RIEMAN SOLVER
// *
// ***********************/
/* finds the max absolute value of the jacobian for F(u).
 */
double eval_lambda(double *U_left, double *U_right,
                              double *V,      double t,
                              double nx,      double ny,
                              int j,          int left_side) {
                              
    double X,Y;
    double A[2];

    get_coordinates_1d(&X,&Y, V, j, left_side);
    // the speed of the wave
    get_velocity(A, X, Y, t);
    return sqrt(A[0]*A[0] + A[1]*A[1]);
}

void riemann_solver_upwind(double *F_n, double *U_left, double *U_right,
                                      double *V, double t, 
                                      double nx, double ny, 
                                      int j, int left_side) {
    double flux_x_l[1], flux_x_r[1], flux_y_l[1], flux_y_r[1];
    double A[2];
    double X,Y;

    // calculate the left and right fluxes
    eval_flux(U_left , flux_x_l, flux_y_l, V, t, j, left_side);
    eval_flux(U_right, flux_x_r, flux_y_r, V, t, j, left_side);

    // get velocity
    get_coordinates_1d(&X,&Y, V, j, left_side);
    get_velocity(A, X, Y, t);

    // get the direction of the wave wrt the normal
    if (nx * A[0] + ny * A[1] > 0) {
        F_n[0] = flux_x_l[0] * nx + flux_y_l[0] * ny;
    } else {
        F_n[0] = flux_x_r[0] * nx + flux_y_r[0] * ny;
    }
}
// local lax-friedrichs riemann solver
 
void riemann_solver(double *F_n, double *U_left, double *U_right,
                               double *V, double t,
                               double nx, double ny, 
                               int j, int left_side) {
    
    //double flux_x_l[2], flux_x_r[2];
    //double flux_y_l[2], flux_y_r[2];
    //double lambda;
    //int n;

    // calculate the left and right fluxes
    //eval_flux(U_left , flux_x_l, flux_y_l, V, t, j, left_side);
    //eval_flux(U_right, flux_x_r, flux_y_r, V, t, j, left_side);

    //lambda = eval_lambda(U_left, U_right, V, t, nx, ny, j, left_side);

    // calculate the riemann problem at this integration point
    //for (n = 0; n < N; n++) {
    //    F_n[n] = 0.5 * ((flux_x_l[n] + flux_x_r[n]) * nx + (flux_y_l[n] + flux_y_r[n]) * ny 
    //                + lambda * (U_left[n] - U_right[n]));
    //}
    //
    riemann_solver_upwind(F_n, U_left, U_right, V, t, nx, ny, j, left_side);
}

__kernel void check_physical(__global double *C_global) { 
    // do nothing
}

//*
 //* source term
 //
void source_term(double *S, double *U, double *V, double t, int j) {
    int n;
    for (n = 0; n < M; n++) {
        S[n] = 0;
    }
}


//***********************
// *
// * CFL CONDITION
// *
// ***********************/
//* global lambda evaluation
// *
// * 
//
/*
__kernel void eval_global_lambda(__global double *C,  __global double *lambda, 
                                 __global double *V1x,__global double *V1y,
                                 __global double *V2x,__global double *V2y,
                                 __global double *V3x,__global double *V3y,
                                 __global double *d_r,         double t) {
    int idx = get_global_id(0);
    if (idx < num_elem) { 

        double A[2];
        double X,Y;
        double V[6];

        V[0] = V1x[idx];
        V[1] = V1y[idx];
        V[2] = V2x[idx];
        V[3] = V2y[idx];
        V[4] = V3x[idx];
        V[5] = V3y[idx];

        // get velocity at the center integration point in the cell
        get_coordinates_2d(&X,&Y, V, n_quad / 2);
        get_velocity(A, X, Y, t);

        // speed of the wave
        lambda[idx] = d_r[idx]/sqrt(A[0]*A[0] + A[1]*A[1]);
    }
}
*/

//New Condition from Thesis
__kernel void eval_global_lambda(__global double *C,  __global double *lambda, 
                                 __global double *V1x,__global double *V1y,
                                 __global double *V2x,__global double *V2y,
                                 __global double *V3x,__global double *V3y,
                                 __global double *r,         double t) {
    int idx = get_global_id(0);
    if (idx < num_elem) { 

        double A[2];
        double X,Y;
        double V[6];

        V[0] = V1x[idx];
        V[1] = V1y[idx];
        V[2] = V2x[idx];
        V[3] = V2y[idx];
        V[4] = V3x[idx];
        V[5] = V3y[idx];

        // get velocity at the center integration point in the cell
        get_coordinates_2d(&X,&Y, V, n_quad / 2);
        get_velocity(A, X, Y, t);

        if (sign(A[0]*(V[3]-V[1]) - A[1]*(V[2]-V[0])) == sign(A[0]*(V[1]-V[5]) - A[1]*(V[0]-V[4])) || A[0]*(V[3]-V[1]) - A[1]*(V[2]-V[0]) == 0) {
            lambda[idx] = ((V[2]-V[0])*(V[5]-V[1])-(V[4]-V[0])*(V[3]-V[1]))/fabs(A[0]*(V[5]-V[3]) - A[1]*(V[4]-V[2]));
        } else if (sign(A[0]*(V[1]-V[5]) - A[1]*(V[0]-V[4])) == sign(A[0]*(V[5]-V[3]) - A[1]*(V[4]-V[2])) || A[0]*(V[1]-V[5]) - A[1]*(V[0]-V[4]) == 0){
            lambda[idx] = ((V[2]-V[0])*(V[5]-V[1])-(V[4]-V[0])*(V[3]-V[1]))/fabs(A[0]*(V[3]-V[1]) - A[1]*(V[2]-V[0]));
        } else if (sign(A[0]*(V[5]-V[3]) - A[1]*(V[4]-V[2])) == sign(A[0]*(V[3]-V[1]) - A[1]*(V[2]-V[0])) || A[0]*(V[5]-V[3]) - A[1]*(V[4]-V[2]) == 0){
            lambda[idx] = ((V[2]-V[0])*(V[5]-V[1])-(V[4]-V[0])*(V[3]-V[1]))/fabs(A[0]*(V[1]-V[5]) - A[1]*(V[0]-V[4]));
        }
    }
}
