lc =0.1;
Point(1) = {-1,-1,0,lc};
Point(2) = {1,-1.,0,lc};
Point(3) = {1,1,0,lc};
Point(4) = {-1,1,0,lc};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};
//Line(5) = {2,4};

Line Loop(8) = {2,3,4,1};
/*Line Loop(7) = {1,5,4};
Line Loop(8) = {2,3,-5};*/
//Plane Surface(9) = {7};
Plane Surface(10) = {8};

Transfinite Line {1,2,3,4} = 51;
//number of points on each boundary

Transfinite Surface {10} = {1,2,3,4};
//Transfinite Surface {10} = {2,3,4};
/*if uncommennented will produce quads.*/

//Recombine Surface {10};
Physical Surface (100) = {10};
//Physical Line (55555) = {5};
Physical Line (55) = {2};
Physical Line (33) = {3};
Physical Line (77) = {4};






