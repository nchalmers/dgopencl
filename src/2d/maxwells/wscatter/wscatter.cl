
#define PI 3.141592653589793

/***********************
 *
 * INITIAL CONDITIONS
 *
 ***********************/

/* initial condition function
 *
 * returns the value of the intial condition at point x,y
 */

void U0(double *U, double x, double y) {

    U[0] = 0;
    U[1] = 0;
    U[2] = 0;
}

/***********************
*
* INFLOW CONDITIONS
*
************************/
double get_mu() {
    return 1.;
}
double get_eps() {
    return 1;
}
void U_inflow(double *U, double x, double y, double t) {
    double f;
    f = 1.;

    U[0] = 0.;
    U[1] = 0.;
    if (y < .15 && y > -.15 && t < f/2.) {
        U[2] = sinpi(2*f*t);
    } else {
        U[2] = 0.;
    }
}

/***********************
*
* OUTFLOW CONDITIONS
*
************************/

void U_outflow(double *U, double x, double y, double t) {
    double f = 1.;
    U[0] = 0;
    U[1] = 0;
    //if (x - 1. > t && x + 1. < t) {
        //U[2] = sinpi(2*f*t);
    //} else {
        U[2] = 0.;
    //}
}

/***********************
*
* REFLECTING CONDITIONS
*
************************/

void U_reflection(double *U_left, double *U_right, 
                             double x, double y, double t,
                             double nx, double ny) {
    double ddot;

    // set h to be the same
    U_right[2] = U_left[2];

    // and reflect the velocities
    ddot = U_left[0] * nx + U_left[1] * ny;

    U_right[1] = -U_left[1] + 2*ddot*nx;
    U_right[2] = -U_left[2] + 2*ddot*ny;
}

void U_exact(double *U, double x, double y, double t) {
}
