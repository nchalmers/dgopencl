#include "../../main.c"

/* wscatter.c
 *
 * maxwells equations
 *
 */

#define PI 3.14159


int limiter = NO_LIMITER;  
double CFL =1.;
int M = 3;

/***********************
 *
 * MAIN FUNCTION
 *
 ***********************/



int main(int argc, char *argv[]) {
    run_dgopencl(argc, argv,"maxwells.cl","wscatter/wscatter.cl");
}