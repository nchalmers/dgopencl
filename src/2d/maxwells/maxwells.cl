
/* maxwells.cl
 *
 * This file contains the relevant information for making a system to solve
 *
 * d_t [ H_x ] + d_x [    0    ] + d_y [ -E_z/mu  ] = 0
 * d_t [ H_y ] + d_x [ E_z/mu  ] + d_y [  0       ] = 0
 * d_t [ E_z ] + d_x [ H_y/eps ] + d_y [ -H_x/eps ] = 0
 *
 */


void get_coordinates_2d(double *x, double *y, double *V, int j);

void get_coordinates_1d(double *x, double *y, double *V, int j, int left_side);


/***********************
 *
 * INITIAL CONDITIONS
 *
 ***********************/

void evalU0(double *U, double *V, int i) {
    int j;
    double u0[3];
    double X,Y;

    U[0] = 0.;
    U[1] = 0.;
    U[2] = 0.;

    for (j = 0; j < n_quad; j++) {

        // get the actual point on the mesh
        get_coordinates_2d(&X, &Y, V, j);

        // evaluate U0 here
        U0(u0, X, Y);

        // evaluate U at the integration point
        U[0] += w[j] * u0[0] * basis[i * n_quad + j];
        U[1] += w[j] * u0[1] * basis[i * n_quad + j];
        U[2] += w[j] * u0[2] * basis[i * n_quad + j];
    }
}

double eval_c(double *U) {
    return 1./sqrt(get_eps() * get_mu());
}


__kernel void check_physical(__global double *C_global) { 
}

/***********************
 *
 * MAXWELLS FLUX
 *
 ***********************/
 /*
  * sets the flux for advection
  * U = (H_x, H_y, E_z)
 */
void eval_flux(double *U, double *flux_x, double *flux_y,
                          double *V, double t, int j, int left_side) {
    double mu, eps;

    mu  = get_mu();
    eps = get_eps();

    // flux_1 
    flux_x[0] = 0.;
    flux_x[1] = 1./mu * U[2];
    flux_x[2] = 1./eps * U[1];

    // flux_2
    flux_y[0] = -1./mu * U[2];
    flux_y[1] = 0.;
    flux_y[2] = -1./eps * U[0];

}

/***********************
 *
 * RIEMAN SOLVER
 *
 ***********************/
/* finds the max absolute value of the jacobian for F(u).
 */
double eval_lambda(double *U_left, double *U_right,
                              double *V,      double t,
                              double nx,      double ny,
                              int j, int left_side) {
                              
    double c_left, c_right;

    // get c for both sides
    c_left  = fabs(eval_c(U_left));
    c_right = fabs(eval_c(U_right));

    return (c_left > c_right) ? c_left : c_right;
}
/* upwinding riemann solver
 */
void riemann_solver(double *F_n, double *U_left, double *U_right,
                                      double *V, double t, 
                                      double nx, double ny, 
                                      int j, int left_side) {
 
    int n;

    double flux_x_l[3], flux_x_r[3];
    double flux_y_l[3], flux_y_r[3];

    // calculate the left and right fluxes
    eval_flux(U_left, flux_x_l, flux_y_l, V, t, j, left_side);
    eval_flux(U_right, flux_x_r, flux_y_r, V, t, j, left_side);

    double lambda = eval_lambda(U_left, U_right, V, t, nx, ny, j, left_side);

    // calculate the riemann problem at this integration point
    for (n = 0; n < M; n++) {
        F_n[n] = 0.5 * ((flux_x_l[n] + flux_x_r[n]) * nx + (flux_y_l[n] + flux_y_r[n]) * ny 
                    + lambda * (U_left[n] - U_right[n]));
    }
}

/***********************
 *
 * CFL CONDITION
 *
 ***********************/
/* global lambda evaluation
 *
 * computes the max eigenvalue 
 */
__kernel void eval_global_lambda(__global double *C,__global double *lambda, 
                                 __global double *V1x,__global double *V1y,
                                 __global double *V2x,__global double *V2y,
                                 __global double *V3x,__global double *V3y,
                                 __global double *r,  double t) {
    int idx = get_global_id(0);
    if (idx < num_elem) { 
        double U[3];

        U[0] = C[num_elem * NP * 0 + idx] * basis[0];
        U[1] = C[num_elem * NP * 1 + idx] * basis[0];
        U[2] = C[num_elem * NP * 2 + idx] * basis[0];

        // return the r value divided by the max eigenvalue
        lambda[idx] = r[idx]/eval_c(U);
    }
}

/*
 * source term
 */
void source_term(double *S, double *U, double *V, double t, int j) {
    int n;
    for (n = 0; n < M; n++) {
        S[n] = 0.;
    }
}