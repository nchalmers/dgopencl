
/* threepoint.cl
 *
 * This file contains the relevant information for making a system to solve the euler equations 
 * in a shifted coordinate system
 *
 * d_t [   rho   ] + d_x [     rho * u - rho*xi     ] + d_y [    rho * v - rho*eta       ] = 0
 * d_t [ rho * u ] + d_x [ rho * u^2 + p - rho*u*xi ] + d_y [   rho * u * v - rho*u*eta  ] = 0
 * d_t [ rho * v ] + d_x [  rho * u * v  - rho*v*xi ] + d_y [  rho * v^2 + p - rho*v*eta ] = 0
 * d_t [    E    ] + d_x [   u * ( E +  p ) -E*xi   ] + d_y [    v * ( E +  p ) - E*eta  ] = 0
 *
 */

void get_coordinates_2d(double *x, double *y, double *V, int j);

void get_coordinates_1d(double *x, double *y, double *V, int j, int left_side);

/***********************
 *
 * EULER DEVICE FUNCTIONS
 *
 ***********************/

void evalU0(double *U, double *V, int i) {
    int j;
    double u0[4];
    double X,Y;

    U[0] = 0.;
    U[1] = 0.;
    U[2] = 0.;
    U[3] = 0.;

    for (j = 0; j < n_quad; j++) {

        // get the 2d point on the mesh
        get_coordinates_2d(&X,&Y, V, j);

        // evaluate U0 here
        U0(u0, X, Y);

        // evaluate U at the integration point
        U[0] += w[j] * u0[0] * basis[i * n_quad + j];
        U[1] += w[j] * u0[1] * basis[i * n_quad + j];
        U[2] += w[j] * u0[2] * basis[i * n_quad + j];
        U[3] += w[j] * u0[3] * basis[i * n_quad + j];
    }
}

/* evaluate pressure
 *
 * evaluates the pressure for U
 */
double pressure(double *U) {

    return (GAMMA - 1.) * (U[3] - (U[1]*U[1] + U[2]*U[2]) / 2. / U[0]);
}

/* is physical
 *
 * returns true if the density and pressure at U make physical sense
 */
bool is_physical(double *U) {
    if (U[0] < 0) {
        return false;
    }

    if (pressure(U) < 0) {
        return false;
    }

    return true;
}

/* check physical
 *
 * if U isn't physical, replace the solution with the constant average value
 */
__kernel void check_physical(__global double *C_global) {
    int i,j,n,k,side;
    int idx = get_global_id(0);
    double C[M*NP];
    double U[M];

    if (idx < num_elem) {

        //Read in global coefficients
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                C[n*NP + i]  = C_global[num_elem * NP * n + i * num_elem + idx];
            }
        }

        for (j = 0; j < n_quad; j++) {

            // initialize to zero
            for (n = 0; n < M; n++) {
                U[n] = 0.;
            }

            // calculate at the integration point
            for (k = 0; k < NP; k++) {
                for (n = 0; n < M; n++) {
                    U[n] += C[n*NP + k] * basis[n_quad * k + j];
                }
            }

            // check to see if U is physical
            if (!is_physical(U)) {
                // set C[1] to C[NP] to zero
                for (i = 1; i < NP; i++) {
                    C_global[num_elem * NP * 0 + i * num_elem + idx] = 0.;
                    C_global[num_elem * NP * 1 + i * num_elem + idx] = 0.;
                    C_global[num_elem * NP * 2 + i * num_elem + idx] = 0.;
                    C_global[num_elem * NP * 3 + i * num_elem + idx] = 0.;

                    C[NP * 0 + i] = 0.;
                    C[NP * 1 + i] = 0.;
                    C[NP * 2 + i] = 0.;
                    C[NP * 3 + i] = 0.;
                }

                // rebuild the solution as simply the average value
                U[0] = C[NP * 0 + 0] * basis[0];
                U[1] = C[NP * 1 + 0] * basis[0];
                U[2] = C[NP * 2 + 0] * basis[0];
                U[3] = C[NP * 3 + 0] * basis[0];
                return;
            }
        }

        for (side =0;side<3;side++)
            for (j = 0; j < n_quad1d; j++) {
                // set U to 0
                for (n = 0; n < M; n++) {
                    U[n]  = 0.;
                }

                //evaluate U at the integration points
                for (i = 0; i < NP; i++) {
                    for (n = 0; n < M; n++) {
                        U[n] += C[n*NP + i] * basis_side[side * NP * n_quad1d + i * n_quad1d + j];
                    }
                }

                if (!is_physical(U)) {
                // set C[1] to C[NP] to zero
                for (i = 1; i < NP; i++) {
                    C_global[num_elem * NP * 0 + i * num_elem + idx] = 0.;
                    C_global[num_elem * NP * 1 + i * num_elem + idx] = 0.;
                    C_global[num_elem * NP * 2 + i * num_elem + idx] = 0.;
                    C_global[num_elem * NP * 3 + i * num_elem + idx] = 0.;

                    C[NP * 0 + i] = 0.;
                    C[NP * 1 + i] = 0.;
                    C[NP * 2 + i] = 0.;
                    C[NP * 3 + i] = 0.;
                }

                // rebuild the solution as simply the average value
                U[0] = C[NP * 0 + 0] * basis[0];
                U[1] = C[NP * 1 + 0] * basis[0];
                U[2] = C[NP * 2 + 0] * basis[0];
                U[3] = C[NP * 3 + 0] * basis[0];
                return;
            }
        }
        
    }
}

/* evaluate c
 *
 * evaulates the speed of sound c
 */
double eval_c(double *U) {
    double p = pressure(U);
    double rho = U[0];

    return sqrt(GAMMA * p / rho);
}    

/***********************
 *
 * EULER FLUX
 *
 ***********************/
/* takes the actual values of rho, u, v, and E and returns the flux 
 * x and y components. 
 * NOTE: this needs the ACTUAL values for u and v, NOT rho * u, rho * v.
 */
void eval_flux(double *U, double *flux_x, double *flux_y,
                          double *V, double t, int j, int left_side) {

    double rho, rhou, rhov, E;
    double xi, eta, p;
    double X,Y;

    // evaluate pressure
    p = pressure(U);
    
    // get variables
    rho  = U[0];
    rhou = U[1];
    rhov = U[2];
    E    = U[3];

    // get the grid points on the mesh
    if (left_side >= 0) {
        // in case we're in eval_surface
        get_coordinates_1d(&X,&Y, V, j, left_side);
    } else {
        // otherwise we're in eval_volume
        get_coordinates_2d(&X,&Y, V, j);
    }
    xi  = X;
    eta = Y;

    // flux_x 
    flux_x[0] = rhou - U[0] * xi;
    flux_x[1] = rhou * rhou / rho + p - U[1] * xi;
    flux_x[2] = rhou * rhov / rho - U[2] * xi;
    flux_x[3] = rhou * (E + p) / rho - U[3] * xi;

    // flux_y
    flux_y[0] = rhov - U[0] * eta;
    flux_y[1] = rhou * rhov / rho - U[1] * eta;
    flux_y[2] = rhov * rhov / rho + p - U[2] * eta;
    flux_y[3] = rhov * (E + p) / rho - U[3] * eta;
}

/***********************
 *
 * RIEMAN SOLVER
 *
 ***********************/
/* finds the max absolute value of the jacobian for F(u).
 *  |u - c|, |u|, |u + c|
 */
double eval_lambda(double *U_left, double *U_right,
                              double *V,      double t,
                              double nx,      double ny,
                              int j, int left_side) {
                              
    double left_max, right_max;
    double c_left, c_right;
    double X,Y;
    double xibar;

    // get c for both sides
    c_left  = eval_c(U_left);
    c_right = eval_c(U_right);

    // get xibar
    get_coordinates_1d(&X,&Y, V, j, left_side);
    xibar = nx * X + ny * Y;
    
    // if speed is positive, want s + c, else s - c
    if (xibar > 0.) {
        left_max = xibar + c_left;
    } else {
        left_max = -xibar + c_left;
    }

    // if speed is positive, want s + c, else s - c
    if (xibar > 0.) {
        right_max = xibar + c_right;
    } else {
        right_max = -xibar + c_right;
    }

    // return the max absolute value of | s +- c |
    if (fabs(left_max) > fabs(right_max)) {
        return fabs(left_max);
    } else { 
        return fabs(right_max);
    }
}

/* local lax-friedrichs riemann solver
 */
void riemann_solver(double *F_n, double *U_left, double *U_right,
                               double *V, double t,
                               double nx, double ny,
                               int j, int left_side) {
    int n;

    double flux_x_l[4], flux_x_r[4];
    double flux_y_l[4], flux_y_r[4];

    // calculate the left and right fluxes
    eval_flux(U_left, flux_x_l, flux_y_l, V, t, j, left_side);
    eval_flux(U_right, flux_x_r, flux_y_r, V, t, j, left_side);

    double lambda = eval_lambda(U_left, U_right, V, t, nx, ny, j, left_side);

    // local lax-friedrichs
    for (n = 0; n < M; n++) {
        F_n[n] = 0.5 * ((flux_x_l[n] + flux_x_r[n]) * nx + (flux_y_l[n] + flux_y_r[n]) * ny 
                    + lambda * (U_left[n] - U_right[n]));
    }
}

// returns the source term S
void source_term(double *S, double *U, double *V, double t, int j) {
    int n;

    // set the source term to 2U
    for (n = 0; n < M; n++) {
        S[n] = -2 * U[n];
    }
}

/***********************
 *
 * CFL CONDITION
 *
 ***********************/
/* global lambda evaluation
 *
 * computes the max eigenvalue of |u + c|, |u|, |u - c|.
 */
__kernel void eval_global_lambda(__global double *C,__global double *lambda, 
                                 __global double *V1x,__global double *V1y,
                                 __global double *V2x,__global double *V2y,
                                 __global double *V3x,__global double *V3y,
                                 __global double *r,  double t) {
    int idx = get_global_id(0);
    if (idx < num_elem) { 
        double c, xibar;
        double X,Y;
        double U[4];
        double V[6];

        // get location
        V[0] = V1x[idx];
        V[1] = V1y[idx];
        V[2] = V2x[idx];
        V[3] = V2y[idx];
        V[4] = V3x[idx];
        V[5] = V3y[idx];

        // get cell averages
        U[0] = C[num_elem * NP * 0 + idx] * basis[0];
        U[1] = C[num_elem * NP * 1 + idx] * basis[0];
        U[2] = C[num_elem * NP * 2 + idx] * basis[0];
        U[3] = C[num_elem * NP * 3 + idx] * basis[0];

        // evaluate c
        c = eval_c(U);

        // get xibar
        X = V[2] * 1./3 + V[4] * 1./3 + V[0] * 1./3;
        Y = V[3] * 1./3 + V[5] * 1./3 + V[1] * 1./3;

        xibar = sqrt(X*X + Y*Y);

        // return the max eigenvalue
        if (xibar > 0) {
            lambda[idx] = r[idx]/(xibar + c);
        } else {
            lambda[idx] = r[idx]/(-xibar + c);
        }
    }
}

/* evaluate pressure
 * 
 * evaluates pressure at the three vertex points for output
 * THREADS: num_elem
 */
__kernel void eval_pressure(__global double *C,__global double *Uv1,
                           __global double *Uv2,__global double *Uv3) {
    int idx = get_global_id(0);
    if (idx < num_elem) {
        int i, n;
        double U1[4], U2[4], U3[4];

        // calculate values at the integration points
        for (n = 0; n < M; n++) {
            U1[n] = 0.;
            U2[n] = 0.;
            U3[n] = 0.;
            for (i = 0; i < NP; i++) {
                U1[n] += C[num_elem * NP * n + i * num_elem + idx] * basis_vertex[i * 3 + 0];
                U3[n] += C[num_elem * NP * n + i * num_elem + idx] * basis_vertex[i * 3 + 1];
                U2[n] += C[num_elem * NP * n + i * num_elem + idx] * basis_vertex[i * 3 + 2];
            }
        }

        // store result
        Uv1[idx] = pressure(U1);
        Uv2[idx] = pressure(U2);
        Uv3[idx] = pressure(U3);
    }
}

/* evaluate mach number
 * 
 * evaluates mach number at the three vertex points for output
 * THREADS: num_elem
 */
__kernel void eval_mach(__global double *C,__global double *Uv1,
                        __global double *Uv2,__global double *Uv3) {
    int idx = get_global_id(0);
    if (idx < num_elem) {
        int i, n, vertex;
        double U[4];
        double c[3], vel[3];
        double u,v;

        // calculate values at the integration points
        for (vertex = 0; vertex < 3; vertex++) {
            for (n = 0; n < M; n++) {
                U[n] = 0.;
                for (i = 0; i < NP; i++) {
                    U[n] += C[num_elem * NP * n + i * num_elem + idx] * basis_vertex[i * 3 + vertex];
                }

                u = U[1]/U[0];
                v = U[2]/U[0];
                
                c[vertex] = eval_c(U);
                vel[vertex] = sqrt(u*u + v*v);
            }
        }


        // store result
        Uv1[idx] = vel[0] / c[0];
        Uv2[idx] = vel[1] / c[1];
        Uv3[idx] = vel[2] / c[2];
    }
}
