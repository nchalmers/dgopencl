lcmin = 0.05;
lcmax = 0.02;

Point(1) = {0,0,0};
Point(2) = {  1.9318516525781366,  0.5176380902050415, 0};
Point(3) = { 1.6730326074756159, 1.4835639164941097, 0};
Point(4) = { -0.2588190451025207,  0.9659258262890683, 0};

Point(5) = {1.075,0.28804538186345691,0};
Point(6) = {1.075,1.3233215622735399,0};

Point(7) = {1.045,0.4102,0};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

Line Loop(5) = {1,2,3,4};
Plane Surface(6) = {5};

Line(7) = {5,6};

Physical Surface (100) = {6};
Physical Line(10000) = {1};
Physical Line(30000) = {2,3,4};

Field[1] = Attractor;
Field[1].EdgesList = {7};
Field[1].NNodesByEdge = 1000;

Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = lcmin;
Field[2].LcMax = lcmax;
Field[2].DistMin = 0.1;
Field[2].DistMax = 0.4;

Field[3] = Attractor;
Field[3].NodesList = {7};

Field[4] = Threshold;
Field[4].IField = 3;
Field[4].LcMin = lcmin/50;
Field[4].LcMax = lcmax;
Field[4].DistMin = 0.01;
Field[4].DistMax = 0.7;

Field[7] = Min;
Field[7].FieldsList = {2,4};
Background Field = 7;


