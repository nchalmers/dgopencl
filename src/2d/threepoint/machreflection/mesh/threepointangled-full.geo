lcminShockLine  = 5.0e-3;
lcminLowerShock = 5.0e-3;
lcminPoint      = 1.0e-5;
lcmax = 1.5e-2;

Point(1) = {0,0,0};
Point(2) = {1.9318516525781366, 0.5176380902050415, 0};
Point(3) = {1.6730326074756159, 1.4835639164941097, 0};
Point(4) = {-0.2588190451025207, 0.9659258262890683, 0};

Point(5) = {1.075,0.28804538186345691,0};
Point(6) = {1.075,1.3233215622735399,0};

Point(7) = {1.075,0.40835,0};

Point(8) = {-1, 0, 0};
Point(9) = { -1,  0.9659258262890683, 0};

Point(10) = {1.1, 0.29474411167423498, 0};
Point(11) = {1.0875,0.2913947467688459,0};

Point(12) = {1.075, .4103, 0};

// two points for triple point refinement
Point(13) = {1.075, .4008, 0};
Point(14) = {1.075, .4108, 0};

// first box
Line(1) = {8,1};
Line(2) = {1,5};
Line(3) = {5,6};
Line(4) = {6,4};
Line(5) = {4,9};
Line(6) = {9,8};

// second box
Line(7) = {5,2};
Line(8) = {2,3};
Line(9) = {3,6};
Line(10) = {6,5};

// two extra lines
Line(11) = {10,7};
Line(12) = {11,7};

Line Loop(13) = {1,2,3,4,5,6};
Line Loop(14) = {7,8,9,10};
Plane Surface(15) = {13};
Plane Surface(16) = {14};

// new shock line
Line(17) = {6,12};

// new triple point line
Line(18) = {13,14};

// this combines the two surfaces
Extrude { Surface{16}; Layers{5, 0.01}; }

Physical Surface (100) = {15, 16};
Physical Line(10000) = {1,2,7};
Physical Line(30000) = {8,9,4,5,6};

// two bottom lines
Field[1] = Attractor;
Field[1].EdgesList = {11,12};
Field[1].NNodesByEdge = 100;

Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = lcminLowerShock;
Field[2].LcMax = lcmax;
Field[2].DistMin = 0.0100;
Field[2].DistMax = 0.15;

// shock line
Field[3] = Attractor;
Field[3].EdgesList = {17};
Field[3].NNodesByEdge = 600000;

Field[4] = Threshold;
Field[4].IField = 3;
Field[4].LcMin = lcminShockLine;
Field[4].LcMax = lcmax;
Field[4].DistMin = lcminShockLine*4;
Field[4].DistMax = lcminShockLine*8;

// triple point
Field[5] = Attractor;
Field[5].EdgesList = {18};
Field[5].NNodesByEdge = 10000;

Field[6] = Threshold;
Field[6].IField = 5;
Field[6].LcMin = lcminPoint;
Field[6].LcMax = lcmax;
Field[6].DistMin = 0.002;
Field[6].DistMax = 0.1250;

Field[7] = Min;
Field[7].FieldsList = {2,4,6};
Background Field = 7;
