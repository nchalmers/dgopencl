xf = 2;
yf = 1;
h1 = xf/12;
h2 = xf/120;
lcmin = 0.0002;
lcmax = 0.02;
x2 = 1.3429491924311225;
Point(1) = {0,0,0,h1};
Point(2) = {xf,0,0,h1};
Point(3) = {xf,yf,0,h1};
Point(4) = {0,yf,0,h1};

Point(5) = {1.0795,0,0,h1};
Point(6) = {x2,1,0,h1};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

Line Loop(5) = {1,2,3,4};
Plane Surface(6) = {5};

Line(7) = {5,6};

Physical Surface (100) = {6};
Physical Line(10000) = {1};
Physical Line(30000) = {2,3,4};

Field[1] = Attractor;
Field[1].NodesList = {5,6};
Field[1].NNodesByEdge = 1000;
Field[1].EdgesList = {7};

Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = lcmin;
Field[2].LcMax = lcmax;
Field[2].DistMin = 0.0003;
Field[2].DistMax = 0.7;

Field[7] = Min;
Field[7].FieldsList = {2};
Background Field = 7;


