#include "../../main.c"

/* machreflection.c
 *
 * Shifted euler equations to investigate the triple point paradox.
 */

#define PI 3.14159
#define GAMMA 1.4
#define MACH .98
#define ALPHA 5.

int limiter = LIMITER;  
double CFL =1.;
int M = 4;

/***********************
 *
 * MAIN FUNCTION
 *
 ***********************/



int main(int argc, char *argv[]) {
    run_dgopencl(argc, argv,"threepoint.cl","machreflection/machreflection.cl");
}
