
#define GAMMA 1.4
#define PI 3.14159265358979323846
#define MACH 1.075
#define X0 1.075
#define THETA 75.0

/***********************
 *
 * SHOCK CONDITIONS
 *
 ***********************/

void U_shock(double *U, double x, double y) {
    double p, s;

    // shock speed
    s = 0.12064;

    // pressure 
    p = 1.18156;

    U[0] = 1.57697;
    U[1] = s * U[0];
    U[2] = 0.;
    U[3] = 0.5 * U[0] * s * s + p / (GAMMA - 1.0);
}

/***********************
 *
 * INITIAL CONDITIONS
 *
 ***********************/

/* initial condition function
 *
 * returns the value of the intial condition at point x,y
 */
void U0(double *U, double x, double y) {
    
    if (x <= X0) {
        U_shock(U, x, y);
    } else {
        U[0] = GAMMA;
        U[1] = 0.;
        U[2] = 0.;
        U[3] = 1./ (GAMMA - 1.0);
    }
}

/***********************
*
* INFLOW CONDITIONS
*
************************/

void U_inflow(double *U, double x, double y, double t) {
    double shock_position;
    
    shock_position = X0;

    if (x <= shock_position) {
        U_shock(U, x, y);
    } else {
        U[0] = GAMMA;
        U[1] = 0.;
        U[2] = 0.;
        U[3] = 1./ (GAMMA - 1.0);
    }
}

/***********************
*
* OUTFLOW CONDITIONS
*
************************/

void U_outflow(double *U, double x, double y, double t) {
    U_inflow(U, x, y, t);
}

/***********************
*
* REFLECTING CONDITIONS
*
************************/
void U_reflection(double *U_left, double *U_right,
                             double x, double y, double t,
                             double nx, double ny) {

    double ddot;
    // set rho and E to be the same in the ghost cell
    U_right[0] = U_left[0];
    U_right[3] = U_left[3];

    // normal reflection
    ddot = U_left[1] * nx + U_left[2] * ny;
    U_right[1] = U_left[1] - 2*ddot*nx;
    U_right[2] = U_left[2] - 2*ddot*ny;

}

/***********************
 *
 * EXACT SOLUTION
 *
 ***********************/

void U_exact(double *U, double x, double y, double t) {
    // there is no exact solution
}
