/* main.c
 *
 * Contains the main functions to read the mesh mappings and run the GPU code
 * to solve the specific problem .
 *
 * Does the following
 * 1. gets user input
 * 2. reads mesh mappings
 * 3. allocates memory on the GPU
 * 4. Defines integration points and weights
 * 5. Precompute basis functions at those integration points
 * 6. performs other precomputations on GPU
 * 7. computes initial projection U_0 on GPU
 * 8. runs the time integrator function
 * 9. writes the result to file
 */
#include <CL/cl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

// limiter optoins
#define NO_LIMITER 0
#define LIMITER 1

#define MAX_SOURCE_SIZE (0x100000)

extern int M;
extern int limiter;
extern double CFL;


#include "quadrature.c"
#include "basis.c"
#include "init_OpenCL.c"
#include "time_integrator.c"



void write_U(cl_mem d_c, int num, double *V1x,double *V1y,
						double *V2x,double *V2y,
						double *V3x,double *V3y,
						int num_elem, int total_timesteps) {
    double *Uv1, *Uv2, *Uv3;
    cl_mem d_Uv1, d_Uv2, d_Uv3;
    int i, n;
    FILE *out_file;
    char out_filename[100];
    cl_uint status;

    // evaluate at the vertex points and copy over data
    Uv1 = (double *) malloc(num_elem * sizeof(double));
    Uv2 = (double *) malloc(num_elem * sizeof(double));
    Uv3 = (double *) malloc(num_elem * sizeof(double));

	d_Uv1 = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, num_elem  * sizeof(double),NULL, &status));
	d_Uv2 = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, num_elem  * sizeof(double),NULL, &status));
	d_Uv3 = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, num_elem  * sizeof(double),NULL, &status));
 
    // evaluate and write to file
    for (n = 0; n < M; n++) {
    
        eval_u(&d_c, &d_Uv1, &d_Uv2, &d_Uv3, &n);
        CL_FUNCTION(clFinish,(command_queue));
        
        CL_FUNCTION(clEnqueueReadBuffer,(command_queue, d_Uv1, CL_TRUE, 0, num_elem  * sizeof(double), (void *)Uv1, 0, NULL, NULL));
		CL_FUNCTION(clEnqueueReadBuffer,(command_queue, d_Uv2, CL_TRUE, 0, num_elem  * sizeof(double), (void *)Uv2, 0, NULL, NULL));
		CL_FUNCTION(clEnqueueReadBuffer,(command_queue, d_Uv3, CL_TRUE, 0, num_elem  * sizeof(double), (void *)Uv3, 0, NULL, NULL));
		CL_FUNCTION(clFinish,(command_queue));
        
        if (num == total_timesteps) {
            sprintf(out_filename, "output/U%d-final.pos", n);
        } else {
            sprintf(out_filename, "output/video/U%d-%d.pos", n, num);
        }
        out_file  = fopen(out_filename , "w");
        fprintf(out_file, "View \"U%i \" {\n", n);
        for (i = 0; i < num_elem; i++) {
            fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) "
            				  "{%.015lf,%.015lf,%.015lf};								\n", 
            				  	V1x[i], V1y[i], V2x[i], V2y[i], 
            				  	V3x[i], V3y[i], Uv1[i], Uv2[i], Uv3[i]);
        }
        fprintf(out_file,"};");
        fclose(out_file);
    }

    free(Uv1);
    free(Uv2);
    free(Uv3);
    
    CL_FUNCTION(clReleaseMemObject,(d_Uv1));
    CL_FUNCTION(clReleaseMemObject,(d_Uv2));
    CL_FUNCTION(clReleaseMemObject,(d_Uv3)); 
}

void display_U(cl_mem d_c, double *V1x,double *V1y,
						double *V2x,double *V2y,
						double *V3x,double *V3y,int *pid, 
						int num_elem, int total_timesteps) {
	int i;
	FILE *fp;

	if (*pid !=0) {
		kill(*pid, 9);
	}

    write_U(d_c, total_timesteps, V1x, V1y, V2x, V2y, V3x, V3y,num_elem,total_timesteps);
	system("gmsh -pid output/U0-final.pos > /tmp/gmsh.pid &");
	sleep(1);
	fp = fopen("/tmp/gmsh.pid", "r");
	fscanf(fp, "%d", pid);
	fclose(fp);
}

/* set quadrature 
 *
 * sets the 1d quadrature integration points and weights for the boundary integrals
 * and the 2d quadrature integration points and weights for the volume intergrals.
 */
 
void set_quadrature(int n,double **r1, double **r2, 
						  double **w, 
						  double **r_oned, double **w_oned,
						  int *n_quad, int *n_quad1d) {
    int i;
    /*
     * The sides are mapped to the canonical element, so we want the integration points
     * for the boundary integrals for sides s1, s2, and s3 as shown below:

     s (r2) |\
     ^      | \
     |      |  \
     |      |   \
     |   s3 |    \ s2
     |      |     \
     |      |      \
     |      |       \
     |      |________\
     |         s1
     |
     ------------------------> r (r1)

    *
    */
    switch (n) {
        case 0: *n_quad = 1;
                *n_quad1d = 1;
                break;
        case 1: *n_quad = 3;
                *n_quad1d = 2;
                break;
        case 2: *n_quad = 6;
                *n_quad1d = 3;
                break;
        case 3: *n_quad = 12 ;
                *n_quad1d = 4;
                break;
        case 4: *n_quad = 16;
                *n_quad1d = 5;
                break;
        case 5: *n_quad = 25;
                *n_quad1d = 6;
                break;
    }
    // allocate integration points
    *r1 = (double *)  malloc(*n_quad * sizeof(double));
    *r2 = (double *)  malloc(*n_quad * sizeof(double));
    *w  = (double *) malloc(*n_quad * sizeof(double));

    *r_oned = (double *) malloc(*n_quad1d * sizeof(double));
    *w_oned = (double *) malloc(*n_quad1d * sizeof(double));

	

    // set 2D quadrature rules
    for (i = 0; i < *n_quad; i++) {
        if (n > 0) {
            (*r1)[i] = quad_2d[2 * n - 1][3*i];
            (*r2)[i] = quad_2d[2 * n - 1][3*i+1];
            (*w) [i] = quad_2d[2 * n - 1][3*i+2] / 2.; //weights are 2 times too big for some reason
        } else {
            (*r1)[i] = quad_2d[0][3*i];
            (*r2)[i] = quad_2d[0][3*i+1];
            (*w) [i] = quad_2d[0][3*i+2] / 2.; //weights are 2 times too big for some reason
        }
        
    }

    // set 1D quadrature rules
    for (i = 0; i < *n_quad1d; i++) {
        (*r_oned)[i] = quad_1d[n][2*i];
        (*w_oned)[i] = quad_1d[n][2*i+1];
    }
}     

void read_mesh(char *mesh_filename, 
			  int *local_num_elem, int *local_num_sides,
              double **V1x, double **V1y,
              double **V2x, double **V2y,
              double **V3x, double **V3y,
              int **left_side_number, int **right_side_number,
              double **sides_x1, double **sides_y1,
              double **sides_x2, double **sides_y2,
              int **elem_s1,  int **elem_s2, int **elem_s3,
              int **left_elem, int **right_elem) {

    int i, items;
    char line[1000];
    FILE *mesh_file;
    
    
    // open the mesh to get local_num_elem for allocations
    mesh_file = fopen(mesh_filename, "r");
    if (!mesh_file) {
        printf("\nERROR: mesh file not found.\n");
        exit(1);
    }
    
    // stores the number of sides this element has.

    fgets(line, 1000, mesh_file);
    sscanf(line, "%i", local_num_elem);
    *V1x = (double *) malloc(*local_num_elem * sizeof(double));
    *V1y = (double *) malloc(*local_num_elem * sizeof(double));
    *V2x = (double *) malloc(*local_num_elem * sizeof(double));
    *V2y = (double *) malloc(*local_num_elem * sizeof(double));
    *V3x = (double *) malloc(*local_num_elem * sizeof(double));
    *V3y = (double *) malloc(*local_num_elem * sizeof(double));

    *elem_s1 = (int *) malloc(*local_num_elem * sizeof(int));
    *elem_s2 = (int *) malloc(*local_num_elem * sizeof(int));
    *elem_s3 = (int *) malloc(*local_num_elem * sizeof(int));

    // read the elements from the mesh
    for (i = 0; i < *local_num_elem; i++) {
        fgets(line, sizeof(line), mesh_file);
        items = sscanf(line, "%lf %lf %lf %lf %lf %lf %i %i %i", &(*V1x)[i], &(*V1y)[i], 
                                         &(*V2x)[i], &(*V2y)[i], 
                                         &(*V3x)[i], &(*V3y)[i], 
                                         &(*elem_s1)[i], &(*elem_s2)[i], &(*elem_s3)[i]);

        if (items != 9) {
            printf("error: not enough items (%i) while reading elements from mesh.\n", items);
            exit(0);
        }

    }

    fgets(line, 1000, mesh_file);
    sscanf(line, "%i", local_num_sides);
    
    *left_side_number  = (int *)   malloc(*local_num_sides * sizeof(int));
    *right_side_number = (int *)   malloc(*local_num_sides * sizeof(int));

    *sides_x1    = (double *) malloc(*local_num_sides * sizeof(double));
    *sides_x2    = (double *) malloc(*local_num_sides * sizeof(double));
    *sides_y1    = (double *) malloc(*local_num_sides * sizeof(double));
    *sides_y2    = (double *) malloc(*local_num_sides * sizeof(double)); 

    *left_elem   = (int *) malloc(*local_num_sides * sizeof(int));
    *right_elem  = (int *) malloc(*local_num_sides * sizeof(int));

    // read the edges from the mesh
    for (i = 0; i < *local_num_sides; i++) {
        fgets(line, sizeof(line), mesh_file);
        items = sscanf(line, "%lf %lf %lf %lf %i %i %i %i", &(*sides_x1)[i], &(*sides_y1)[i],
                                            &(*sides_x2)[i], &(*sides_y2)[i],
                                            &(*left_elem)[i], &(*right_elem)[i],
                                            &(*left_side_number)[i],
                                            &(*right_side_number)[i]);

        if (items != 8) {
            printf("error: not enough items (%i) while reading edges from mesh.\n", items);
            exit(0);
        }
    }
    
    // close the file
    fclose(mesh_file);
    
}

void usage_error() {
    printf("\nUsage: dgcuda [OPTIONS] [MESH] \n");
    printf(" Options: [-n] Order of polynomial approximation.\n");
    printf("          [-t] Number of timesteps.\n");
    printf("          [-T] End time.\n");
    printf("          [-v] Verbose.\n");
    printf("          [-V] Print out every N timesteps.\n");
    printf("          [-b] Benchmark.\n");
    printf("          [-cpu] Force use of CPU.\n");
    printf("          [-h] Display this message.\n");
}

int get_input(int argc, char *argv[], int *n, 
			  double *endtime, int *total_timesteps,
			  int *verbose, int *benchmark,
    		  int *video, int *cpu_switch,
    		  char **mesh_filename) {

    int i;

    *endtime = -1;
    *total_timesteps = -1;
    *verbose         = 0;
    *benchmark       = 0;
    *video           = 0;
    *cpu_switch		= 0;
    
    // read command line input
    if (argc < 5) {
        usage_error();
        return 1;
    }
    for (i = 0; i < argc; i++) {
        // order of polynomial
        if (strcmp(argv[i], "-n") == 0) {
            if (i + 1 < argc) {
                *n = atoi(argv[i+1]);
                if (*n < 0 || *n > 5) {
                    usage_error();
                    return 1;
                }
            } else {
                usage_error();
                return 1;
            }
        }
        // number of total_timesteps
        if (strcmp(argv[i], "-t") == 0) {
            if (i + 1 < argc) {
                *total_timesteps = atoi(argv[i+1]);
                if (*total_timesteps < 0) {
                    usage_error();
                    return 1;
                }
            } else {
                usage_error();
                return 1;
            }
        }
        if (strcmp(argv[i], "-T") == 0) {
            if (i + 1 < argc) {
                *endtime = atof(argv[i+1]);
                if (*endtime < 0) {
                    usage_error();
                    return 1;
                }
            } else {
                usage_error();
                return 1;
            }
        }
        if (strcmp(argv[i], "-V") == 0) {
            if (i + 1 < argc) {
                *video = atof(argv[i+1]);
                if (*video < 0) {
                    usage_error();
                    return 1;
                }
            } else {
                usage_error();
                return 1;
            }
        }
        if (strcmp(argv[i], "-b") == 0) {
            *benchmark = 1;
        }
        if (strcmp(argv[i], "-v") == 0) {
            *verbose = 1;
        }
        if (strcmp(argv[i], "-cpu") == 0) {
            *cpu_switch = 1;
        }
        if (strcmp(argv[i], "-h") == 0) {
                usage_error();
                return 1;
        }
    } 
    
    // sanity check on limiter
    if (limiter && *n != 1) {
        printf("Error: limiter only enabled for p = 1\n");
        return 1;
    }

    // last argument is filename
    *mesh_filename = argv[argc - 1];

    return 0;
}


int run_dgopencl(int argc, char *argv[], char *flux_CL_filename, char *ansatz_CL_filename) {
	//Problem data
	int n, n_p;
	int total_timesteps, pid;
	int verbose, video, benchmark, cpu_switch;
	double endtime, t;
	char *mesh_filename;

    //Local Mesh data
    int num_elem, num_sides;
    double *V1x, *V1y, *V2x, *V2y, *V3x, *V3y;
    double *sides_x1, *sides_x2;
    double *sides_y1, *sides_y2;
    int *left_elem, *right_elem;
    int *elem_s1, *elem_s2, *elem_s3;
    int *left_side_number, *right_side_number;

    //Local quadrature data
    int n_quad, n_quad1d;
    double *r1, *r2, *w;
	double *r_oned, *w_oned;
	double *basis, *basis_grad_x, *basis_grad_y; 
	double *basis_side, *basis_vertex; 

    //Local min cell height
    double *min_height, min_h;

    //Benchmark variables
	clock_t start, end;
    double elapsed;

	int i;

	//GPU memory buffers
	cl_mem d_c;

	cl_mem d_J, d_h, d_V1x, d_V1y, d_V2x, d_V2y, d_V3x, d_V3y;
	cl_mem d_s_V1x, d_s_V1y, d_s_V2x, d_s_V2y, d_s_length;
	cl_mem d_elem_s1, d_elem_s2, d_elem_s3; 
	cl_mem d_left_side_number, d_right_side_number, d_left_elem, d_right_elem; 
	cl_mem d_Nx, d_Ny, d_xr, d_yr, d_xs, d_ys; 


    // get input 
    if (get_input(argc, argv, &n, &endtime, &total_timesteps, &verbose, 
    			  &benchmark, &video, &cpu_switch, &mesh_filename)) {
        return 1;
    }

    // set the order of the approximation
    n_p = (n + 1) * (n + 2) / 2;

	// read in the mesh and make all the mappings
    read_mesh(mesh_filename, &num_elem, &num_sides,
    					 &V1x, &V1y, &V2x, &V2y, &V3x, &V3y,
                         &left_side_number, &right_side_number,
                         &sides_x1, &sides_y1, 
                         &sides_x2, &sides_y2, 
                         &elem_s1, &elem_s2, &elem_s3,
                         &left_elem, &right_elem);
    
    // get the correct quadrature rules for this scheme
    set_quadrature(n, &r1, &r2, &w, &r_oned, &w_oned,
    					&n_quad, &n_quad1d); 
    
    // evaluate the basis functions at those points
    preval_basis(n_p, n_quad, n_quad1d, r1, r2, w, r_oned,
    				&basis, &basis_grad_x, &basis_grad_y, 
    				&basis_side, &basis_vertex); 
    
    //Initialize and create OpenCL kernels                             
    init_OpenCL(flux_CL_filename, ansatz_CL_filename, &cpu_switch,
    			n_p, num_elem, num_sides, 
    			n_quad, n_quad1d, basis,
    			basis_grad_x, basis_grad_y,
    			basis_side, basis_vertex,
    			w, r1, r2, r_oned, w_oned);

    //Copy mesh data to the GPU
    create_buffers(n, n_p, num_elem, num_sides,
    		  V1x, V1y, V2x, V2y, V3x, V3y, 
              left_side_number, right_side_number,
              sides_x1, sides_y1,
              sides_x2, sides_y2,
              elem_s1, elem_s2, elem_s3,
              left_elem, right_elem,
              &d_c, &d_J, &d_h, &d_V1x, &d_V1y, &d_V2x, &d_V2y, &d_V3x, &d_V3y,
			  &d_s_V1x, &d_s_V1y, &d_s_V2x, &d_s_V2y, &d_s_length,
		      &d_elem_s1, &d_elem_s2, &d_elem_s3, 
			  &d_left_side_number, &d_right_side_number, &d_left_elem, &d_right_elem,
			  &d_Nx, &d_Ny, &d_xr, &d_yr, &d_xs, &d_ys);
            
    // pre computations
    preval_min_height(d_h,d_V1x,d_V1y,d_V2x,d_V2y,d_V3x,d_V3y);

    min_height = (double *) malloc(num_elem * sizeof(double));
    CL_FUNCTION(clEnqueueReadBuffer,(command_queue, d_h, CL_TRUE, 0, 
    							num_elem  * sizeof(double), (void *)min_height, 0, NULL, NULL));
    min_h = min_height[0];
    for (i = 1; i < num_elem; i++) {
        min_h = (min_height[i] < min_h) ? min_height[i] : min_h;
    }
    free(min_height);

    preval_jacobian(d_J,d_V1x,d_V1y,d_V2x,d_V2y,d_V3x,d_V3y); 
    
    preval_side_length(d_s_length,d_s_V1x,d_s_V1y,d_s_V2x,d_s_V2y);
                                                      
    preval_normals(d_Nx,d_Ny,d_s_V1x,d_s_V1y,d_s_V2x,d_s_V2y);
    
	CL_FUNCTION(clFinish,(command_queue));
	/*
    preval_normals_direction(d_Nx,d_Ny,d_V1x,d_V1y,d_V2x,d_V2y,d_V3x,d_V3y,
                              d_left_elem,d_left_side_number);
    */
    preval_partials(d_xr,d_yr,d_xs,d_ys,d_V1x,d_V1y,d_V2x,d_V2y,d_V3x,d_V3y);
   
   	CL_FUNCTION(clFinish,(command_queue));

   	//Clear unneeded precomputation variables
   	CL_FUNCTION(clReleaseMemObject,(d_s_V1x));
    CL_FUNCTION(clReleaseMemObject,(d_s_V2x));
    CL_FUNCTION(clReleaseMemObject,(d_s_V1y));
    CL_FUNCTION(clReleaseMemObject,(d_s_V2y));


    // initial conditions
    initial_conditions(d_c,d_J,d_V1x,d_V1y,d_V2x,d_V2y,d_V3x,d_V3y);
    if (limiter) {
    	limit_c(&d_c);	
    	check_physical(&d_c);
    }
    CL_FUNCTION(clFinish,(command_queue));  
   	
    
    printf(" ? %i degree polynomial interpolation (n_p = %i)\n", n, n_p);
    printf(" ? %i precomputed basis points\n", n_quad * n_p);
    printf(" ? %i elements\n", num_elem);
    printf(" ? %i sides\n", num_sides);
    printf(" ? min height = %.015lf\n", min_h);

    if (endtime == -1) {
        printf(" ? total_timesteps = %i\n", total_timesteps);
    } else if (endtime != -1) {
        printf(" ? endtime = %lf\n", endtime);
    }

    if (benchmark) {
        start = clock();
    }
    
    //Time step
    t = time_integrate(n,n_p,num_elem,num_sides,
    				   endtime,total_timesteps, 
    				   video,verbose,&pid,d_c,
    				   V1x, V1y, V2x, V2y, V3x, V3y);
    
    if (benchmark) {
        end = clock();
        elapsed = ((double)(end - start)) / CLOCKS_PER_SEC;
        printf("Runtime: %lf seconds\n", elapsed);
    }
    
    CL_FUNCTION(clReleaseMemObject,(d_s_length));
    
    CL_FUNCTION(clReleaseMemObject,(d_h));
    
    CL_FUNCTION(clReleaseMemObject,(d_xr));
    CL_FUNCTION(clReleaseMemObject,(d_yr));
    CL_FUNCTION(clReleaseMemObject,(d_xs));
    CL_FUNCTION(clReleaseMemObject,(d_ys));

    CL_FUNCTION(clReleaseMemObject,(d_left_side_number));
    CL_FUNCTION(clReleaseMemObject,(d_right_side_number));

    CL_FUNCTION(clReleaseMemObject,(d_Nx));
    CL_FUNCTION(clReleaseMemObject,(d_Ny));

    CL_FUNCTION(clReleaseMemObject,(d_J));
    CL_FUNCTION(clReleaseMemObject,(d_right_elem));
    CL_FUNCTION(clReleaseMemObject,(d_left_elem));
    CL_FUNCTION(clReleaseMemObject,(d_elem_s1));
    CL_FUNCTION(clReleaseMemObject,(d_elem_s2));
    CL_FUNCTION(clReleaseMemObject,(d_elem_s3));

    CL_FUNCTION(clReleaseMemObject,(d_V1x));
    CL_FUNCTION(clReleaseMemObject,(d_V1y));
    CL_FUNCTION(clReleaseMemObject,(d_V2x));
    CL_FUNCTION(clReleaseMemObject,(d_V2y));
    CL_FUNCTION(clReleaseMemObject,(d_V3x));
    CL_FUNCTION(clReleaseMemObject,(d_V3y));


	// evaluate and write U to file
    write_U(d_c, total_timesteps, V1x, V1y, V2x, V2y, V3x, V3y,num_elem,total_timesteps);      

           
    // free everything else
    CL_FUNCTION(clReleaseMemObject,(d_c));

    CL_FUNCTION(clReleaseKernel,(kernel_preval_jacobian)); 
    CL_FUNCTION(clReleaseKernel,(kernel_preval_side_length));
    CL_FUNCTION(clReleaseKernel,(kernel_preval_min_height));
    CL_FUNCTION(clReleaseKernel,(kernel_preval_normals));
    CL_FUNCTION(clReleaseKernel,(kernel_preval_normals_direction));
    CL_FUNCTION(clReleaseKernel,(kernel_preval_partials));
	CL_FUNCTION(clReleaseKernel,(kernel_initial_conditions));
	CL_FUNCTION(clReleaseKernel,(kernel_eval_u));
	CL_FUNCTION(clReleaseKernel,(kernel_limit_c));
	CL_FUNCTION(clReleaseKernel,(kernel_eval_global_lambda));
	CL_FUNCTION(clReleaseKernel,(kernel_eval_surface));
	CL_FUNCTION(clReleaseKernel,(kernel_eval_volume));
	CL_FUNCTION(clReleaseKernel,(kernel_eval_rhs));
	CL_FUNCTION(clReleaseKernel,(kernel_rk_add));

	CL_FUNCTION(clReleaseProgram,(program));
	CL_FUNCTION(clReleaseCommandQueue,(command_queue));
	CL_FUNCTION(clReleaseContext,(context));

    free(V1x);
    free(V1y);
    free(V2x);
    free(V2y);
    free(V3x);
    free(V3y);
	
    return 0;
}
