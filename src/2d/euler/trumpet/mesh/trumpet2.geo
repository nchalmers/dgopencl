
//meshing along trumpet boundar
charLen1= 0.001;

//meshing of outer box
charLen2= 0.02;



//This mesh is an alteration of March25mesh.geo/March25mesh.msh because the dimensions of the tube were 1.2cm too big 
//The mouthpiece diameter also changed to 11.6cm for the first 20.5cm of the tubing
//Refine by splitting.

a=0.09; //Jan30mesh1.msh (order magnitude too small) has a=0.5, Jan30mesh2.msh has a=0.09
c=0.01;

w=0.0006; //0.6cm subtracted off each side of wall so diagnometer is 1.2cm smaller
w2= 0.0006*2; //more realistic boundary


//Bell points
Point(1)  = {148.00*c,6.80*c-w,0,charLen1}; //-.1cm on each side
Point(2)  = {147.91*c,6.69*c-w,0,charLen1};
Point(3)  = {147.48*c,6.21*c-w,0,charLen1};
Point(4)  = {147.04*c,5.74*c-w,0,charLen1};
Point(5)  = {146.73*c,5.44*c-w,0,charLen1};
Point(6)  = {146.40*c,5.12*c-w,0,charLen1};
Point(7)  = {146.08*c,4.86*c-w,0,charLen1};
Point(8)  = {145.80*c,4.64*c-w,0,charLen1};
Point(9)  = {145.22*c,4.16*c-w,0,charLen1};
Point(10) = {144.77*c,3.89*c-w,0,charLen1};
Point(11) = {144.25*c,3.57*c-w,0,charLen1};
Point(12) = {143.82*c,3.30*c-w,0,charLen1};
Point(13) = {142.72*c,2.89*c-w,0,charLen1};
Point(14) = {142.13*c,2.69*c-w,0,charLen1};
Point(15) = {141.24*c,2.45*c-w,0,charLen1};
Point(16) = {140.34*c,2.24*c-w,0,charLen1};
Point(17) = {139.61*c,2.07*c-w,0,charLen1};
Point(18) = {138.98*c,1.96*c-w,0,charLen1};
Point(19) = {137.36*c,1.79*c-w,0,charLen1};
Point(20) = {136.41*c,1.71*c-w,0,charLen1};
Point(21) = {135.62*c,1.64*c-w,0,charLen1};
Point(22) = {134.54*c,1.56*c-w,0,charLen1};
Point(23) = {133.76*c,1.51*c-w,0,charLen1};
Point(24) = {132.54*c,1.42*c-w,0,charLen1};
Point(25) = {131.03*c,1.34*c-w,0,charLen1};
Point(26) = {128.11*c,1.22*c-w,0,charLen1};
Point(27) = {126.26*c,1.13*c-w,0,charLen1};
Point(28) = {124.62*c,1.07*c-w,0,charLen1};
Point(29) = {122.00*c,0.975*c-w,0,charLen1};
Point(30) = {116.00*c,0.850*c-w,0,charLen1};
Point(31) = {109.50*c,0.750*c-w,0,charLen1};
Point(32) = {102.00*c,0.70*c-w,0,charLen1};

Point(33) = {0,0.70*c-w,0,charLen1};

Point(34) = {0,0,0,charLen1};

//box points
Point(101) = {180.00*c,0,0,charLen2};
Point(102) = {180.00*c,20*c,0,charLen2};
Point(103) = {148.00*c,20*c,0,charLen2};


//lines
Line(1) = {101,102};
Line(2) = {102,103};
Line(3) = {103,1};
Spline(4) = {1 ... 32};
Line(5) = {32, 33};
Line(6) = {33,34};

//reflect around x axis
Rotate {{1, 0, 0}, {0, 0, 0}, Pi} {
  Duplicata { Line{1, 2, 3, 4, 5, 6}; }
}

Line Loop(13) = {1, 2, 3, 4, 5, 6, -12, -11, -10, -9, -8, -7};
Plane Surface(14) = {13};

//interior mesh
Physical Surface(20001) = {14};

//inflow edge
Physical Line(30000) = {6, 12};

//reflecting boundary
Physical Line(10000) = {5, 11, 4, 10};

//outflow boundary
Physical Line(20000) = {3, 2, 1, 7, 8, 9};

