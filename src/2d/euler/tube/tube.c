#include "../../main.c"

/* tube.c
 *
 * flow inside tube.
 *
 */

int limiter = NO_LIMITER;  // use a limiter or not
double CFL = 1.;
int M = 4;



/***********************
 *
 * MAIN FUNCTION
 *
 ***********************/

int main(int argc, char *argv[]) {
    run_dgopencl(argc, argv, "euler.cl","tube/tube.cl");
}