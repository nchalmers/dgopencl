#include "../../main.c"

/* doublemach.c
 *
 * Use the Euler equations to solve the double mach reflection.
 *
 * Create a shock and run it into a thin wedge with angle THETA at mach MACH.
 *  
 */


int limiter = LIMITER;  
double CFL = 1.;
int M = 4;


int main(int argc, char *argv[]) {
    run_dgopencl(argc, argv, "euler.cl","doublemach/doublemach.cl");
}
