#include "../../main.c"

/* vortex.c
 *
 * 
 */

#define PI 3.14159
#define GAMMA 1.4
#define MACH .98
#define ALPHA 5.

int limiter = NO_LIMITER;  
double CFL = 1.;
int M = 4;

/***********************
 *
 * MAIN FUNCTION
 *
 ***********************/



int main(int argc, char *argv[]) {
    run_dgopencl(argc, argv,"euler.cl","vortex/vortex.cl");
}
