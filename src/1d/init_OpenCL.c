
size_t n_threads, global_n_threads_elem;
cl_context context;
cl_command_queue command_queue;
cl_program program;
cl_kernel kernel_initial_conditions, kernel_eval_u, kernel_limit_c, kernel_eval_global_lambda_h;
cl_kernel kernel_eval_rhs, kernel_rk_add;

cl_uint status;


#define CL_FUNCTION(NAME, ARGLIST) status = NAME ARGLIST; CL_CHECK_ERROR(status, #NAME); 

#define CL_OBJECT(NAME, ARGLIST) NAME ARGLIST; CL_CHECK_ERROR(status, #NAME); 

#define CL_CHECK_ERROR(STATUS, NAME) \
  if ( (STATUS) != CL_SUCCESS) { \
    printf("ERROR: %s: %s\n", NAME, CLErrString(STATUS) ); \
    exit(-1); } 

static const char * CLErrString(cl_int status) {
   static struct { cl_int code; const char *msg; } error_table[] = {
        { CL_SUCCESS, "success" ,},
        { CL_DEVICE_NOT_FOUND, "device not found",},
        { CL_DEVICE_NOT_AVAILABLE, "device not available",},
        { CL_MEM_OBJECT_ALLOCATION_FAILURE, "mem object allocation failure",},
        { CL_OUT_OF_RESOURCES, "out of resources",},
        { CL_OUT_OF_HOST_MEMORY, "out of host memory",},
        { CL_PROFILING_INFO_NOT_AVAILABLE, "profiling info not available",},
        { CL_MEM_COPY_OVERLAP, "mem copy overlap",},
        { CL_IMAGE_FORMAT_MISMATCH, "image format mismatch",},
        { CL_IMAGE_FORMAT_NOT_SUPPORTED, "image format not supported",},
        { CL_BUILD_PROGRAM_FAILURE, "build program failure",},
        { CL_MAP_FAILURE, "map failure",},
        { CL_INVALID_VALUE, "invalid value",},
        { CL_INVALID_DEVICE_TYPE, "invalid device type",},
        { CL_INVALID_PLATFORM, "invalid platform",},
        { CL_INVALID_DEVICE, "invalid device",},
        { CL_INVALID_CONTEXT, "invalid context",},
        { CL_INVALID_QUEUE_PROPERTIES, "invalid queue properties",},
        { CL_INVALID_COMMAND_QUEUE, "invalid command queue",},
        { CL_INVALID_HOST_PTR, "invalid host ptr",},
        { CL_INVALID_MEM_OBJECT, "invalid mem object",},
        { CL_INVALID_IMAGE_FORMAT_DESCRIPTOR, "invalid image format descriptor",},
        { CL_INVALID_IMAGE_SIZE, "invalid image size",},
        { CL_INVALID_SAMPLER, "invalid sampler",},
        { CL_INVALID_BINARY, "invalid binary",},
        { CL_INVALID_BUILD_OPTIONS, "invalid build options",},
        { CL_INVALID_PROGRAM, "invalid program",},
        { CL_INVALID_PROGRAM_EXECUTABLE, "invalid program executable",},
        { CL_INVALID_KERNEL_NAME, "invalid kernel name",},
        { CL_INVALID_KERNEL_DEFINITION, "invalid kernel definition",},
        { CL_INVALID_KERNEL, "invalid kernel",},
        { CL_INVALID_ARG_INDEX, "invalid arg index",},
        { CL_INVALID_ARG_VALUE, "invalid arg value",},
        { CL_INVALID_ARG_SIZE, "invalid arg size",},
        { CL_INVALID_KERNEL_ARGS, "invalid kernel args",},
        { CL_INVALID_WORK_DIMENSION, "invalid work dimension",},
        { CL_INVALID_WORK_GROUP_SIZE, "invalid work group size",},
        { CL_INVALID_WORK_ITEM_SIZE, "invalid work item size",},
        { CL_INVALID_GLOBAL_OFFSET, "invalid global offset",},
        { CL_INVALID_EVENT_WAIT_LIST, "invalid event wait list",},
        { CL_INVALID_EVENT, "invalid event",},
        { CL_INVALID_OPERATION, "invalid operation",},
        { CL_INVALID_GL_OBJECT, "invalid gl object",},
        { CL_INVALID_BUFFER_SIZE, "invalid buffer size",},
        { CL_INVALID_MIP_LEVEL, "invalid mip level",},
        { 0, NULL },
   };
   static char unknown[25];
   int ii;

   for (ii = 0; error_table[ii].msg != NULL; ii++) {
      if (error_table[ii].code == status) {
         return error_table[ii].msg;
      }
   }

   snprintf(unknown, sizeof unknown, "unknown error %d", status);
   return unknown;
}

void initial_conditions(cl_mem d_c, cl_mem d_X, cl_mem d_h){    
    CL_FUNCTION(clSetKernelArg,(kernel_initial_conditions, 0, sizeof(cl_mem), (void *)&d_c));
    CL_FUNCTION(clSetKernelArg,(kernel_initial_conditions, 1, sizeof(cl_mem), (void *)&d_X));
    CL_FUNCTION(clSetKernelArg,(kernel_initial_conditions, 2, sizeof(cl_mem), (void *)&d_h));
    
    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_initial_conditions, 1, NULL, 
                                    &global_n_threads_elem, &n_threads, 0, NULL, NULL));

}

void eval_u(cl_mem *d_c, cl_mem *d_Uv1, cl_mem *d_Uv2, int *n){    
    CL_FUNCTION(clSetKernelArg,(kernel_eval_u, 0, sizeof(cl_mem), (void *)d_c));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_u, 1, sizeof(cl_mem), (void *)d_Uv1));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_u, 2, sizeof(cl_mem), (void *)d_Uv2));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_u, 3, sizeof(int), (void *)n));

    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_eval_u, 1, NULL, 
                                    &global_n_threads_elem, &n_threads, 0, NULL, NULL));
}

void limit_c(cl_mem *d_c){    
    CL_FUNCTION(clSetKernelArg,(kernel_limit_c, 0, sizeof(cl_mem), (void *)d_c));
    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_limit_c, 1, NULL, 
                                    &global_n_threads_elem, &n_threads, 0, NULL, NULL));
}

void eval_global_lambda_h(){
    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_eval_global_lambda_h, 1, NULL, 
                                    &global_n_threads_elem, &n_threads, 0, NULL, NULL));
}

void eval_rhs(cl_mem *d_c, double t, double dt){    
    CL_FUNCTION(clSetKernelArg,(kernel_eval_rhs,  1, sizeof(cl_mem), (void *)d_c)); 
    CL_FUNCTION(clSetKernelArg,(kernel_eval_rhs,  3, sizeof(double), (void *)&t)); 
    CL_FUNCTION(clSetKernelArg,(kernel_eval_rhs,  4, sizeof(double), (void *)&dt));
    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_eval_rhs, 1, NULL, 
                                    &global_n_threads_elem, &n_threads, 0, NULL, NULL));
}

void rk_add(cl_mem *d_k, cl_mem *d_c, cl_mem *d_rhs, double a){    
    CL_FUNCTION(clSetKernelArg,(kernel_rk_add, 0, sizeof(cl_mem), (void *)d_k));
    CL_FUNCTION(clSetKernelArg,(kernel_rk_add, 1, sizeof(cl_mem), (void *)d_c));
    CL_FUNCTION(clSetKernelArg,(kernel_rk_add, 2, sizeof(cl_mem), (void *)d_rhs));
    CL_FUNCTION(clSetKernelArg,(kernel_rk_add, 3, sizeof(double), (void *)&a));
    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_rk_add, 1, NULL, 
                                    &global_n_threads_elem, &n_threads, 0, NULL, NULL));

}


void arraytostring(char *string, char *type, double *data, int size) {

	int i;
	char temp[100];
	
	strcpy(string,type);
	sprintf(temp,"[] = {%.025f", data[0]);
	strcat(string,temp); 
	
	for (i = 1; i < size; i++) {
		sprintf(temp,",%.025f", data[i]);
		strcat(string,temp);
	}

	sprintf(temp,"}; \n");
	strcat(string,temp);
}

void init_OpenCL(char *flux_CL_filename, char *ansatz_CL_filename, int *cpu_switch,
                  int n_p, int num_elem, int n_quad,
                  double *basis, double *basis_x, double *basis_endpoints,
                  double *r, double *w) {
	// Load the kernel source code into the array source_str
    FILE *fp;
    char *source_basis, *source_constants, *source_ansatz, *source_flux, *source_kernels;
    char *source; 
    size_t source_size;
 
    
    cl_platform_id platform_id = NULL;
    cl_device_id device_id = NULL;   
    cl_uint ret_num_platforms, numDevices;
    
    // Get platform and device information
    CL_FUNCTION(clGetPlatformIDs,(1, &platform_id, &ret_num_platforms));
    if (*cpu_switch) {
        printf("Choosing CPU as default device.\n");
        CL_FUNCTION(clGetDeviceIDs,(platform_id, CL_DEVICE_TYPE_CPU, 1, &device_id, &numDevices));
    } else {
        status = clGetDeviceIDs( platform_id, CL_DEVICE_TYPE_GPU, 1, &device_id, &numDevices);   
        if (status == CL_DEVICE_NOT_FOUND){ //no GPU available.
            printf("No GPU device available.\n");
            printf("Choosing CPU as default device.\n");
            *cpu_switch = 1;
            CL_FUNCTION(clGetDeviceIDs,(platform_id, CL_DEVICE_TYPE_CPU, 1, &device_id, &numDevices));
        }
        CL_CHECK_ERROR(status, "clGetDeviceIDs");
    }
    
    
    // Create an OpenCL context
    context = CL_OBJECT(clCreateContext,(NULL, 1, &device_id, NULL, NULL, &status));
    // Create a command queue
    command_queue = CL_OBJECT(clCreateCommandQueueWithProperties,(context, device_id, 0, &status));
  
    source_constants = (char*)malloc(MAX_SOURCE_SIZE);
    source_basis = (char*)malloc(MAX_SOURCE_SIZE);
	
    //Adds constant declarations to begining of kernel source file. 
	//This way they are constant at compile time. 
	sprintf(source_constants, "#define M %d    							  					\n"
							  "#define NP %d  												\n"
					   		  "#define num_elem %d									\n"
					   	 	  "#define n_quad %d										\n"
					   		,M, n_p, num_elem, n_quad);
	
	arraytostring(source_basis, "__constant double basis", basis, n_quad*n_p);
	strcat(source_constants, source_basis); 
	arraytostring(source_basis, "__constant double basis_x", basis_x, n_quad*n_p);
	strcat(source_constants, source_basis); 
	arraytostring(source_basis, "__constant double basis_endpoints", basis_endpoints, 2* n_p);
	strcat(source_constants, source_basis); 
	arraytostring(source_basis, "__constant double w", w, n_quad);
	strcat(source_constants, source_basis); 
	arraytostring(source_basis, "__constant double r", r, n_quad);
	strcat(source_constants, source_basis); 


	//Read in Initial and boundary condition cl file
	source_ansatz = (char*)malloc(MAX_SOURCE_SIZE*sizeof(char));
	fp = fopen(ansatz_CL_filename, "r");
    if (!fp) {
        printf("Failed to load kernel %s.\n", ansatz_CL_filename);
        exit(1);
    }
    fread(source_ansatz, 1, MAX_SOURCE_SIZE, fp);
    fclose(fp);
    
    //Read in Flux cl file
    source_flux = (char*)malloc(MAX_SOURCE_SIZE*sizeof(char));
    fp = fopen(flux_CL_filename, "r");
    if (!fp) {
        printf("Failed to load kernel %s.\n", flux_CL_filename);
        exit(1);
    }
    fread(source_flux, 1, MAX_SOURCE_SIZE, fp);
    
   
    //Read in kernels cl file
    source_kernels = (char*)malloc(MAX_SOURCE_SIZE*sizeof(char));
    fp = fopen("../conserv_kernels.cl", "r");
    if (!fp) {
        printf("Failed to load kernel conserv_kernels.cl.\n");
        exit(1);
    }
    fread(source_kernels, 1, MAX_SOURCE_SIZE, fp);
    fclose(fp);

		
	source = strcat(source_constants,source_ansatz);
	source = strcat(source, source_flux);
	source = strcat(source, source_kernels);
	
	source_size = strlen(source);
	
		
	// Create a program from the kernel source
    program = CL_OBJECT(clCreateProgramWithSource,(context, 1, (const char **)&source, &source_size, &status));
    
    // Build the program
    status = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    //Output build log (Useful if the build fails)
    char *log;
    log = (char*)malloc(MAX_SOURCE_SIZE*sizeof(char));   
    clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, MAX_SOURCE_SIZE*sizeof(char),log,NULL);
    //status = clGetProgramInfo (program, CL_PROGRAM_SOURCE, MAX_SOURCE_SIZE*sizeof(char),log,NULL);
    printf("%s \n",log);
    CL_CHECK_ERROR(status, "clBuildProgram");
    free(log);

    // Create the OpenCL kernels
	kernel_initial_conditions = CL_OBJECT(clCreateKernel,(program, "initial_conditions", &status));
	kernel_eval_u = CL_OBJECT(clCreateKernel,(program, "eval_u", &status));
	kernel_limit_c = CL_OBJECT(clCreateKernel,(program, "limit_c", &status));
	kernel_eval_global_lambda_h = CL_OBJECT(clCreateKernel,(program, "eval_global_lambda_h", &status));
	kernel_eval_rhs = CL_OBJECT(clCreateKernel,(program, "eval_rhs", &status));
	kernel_rk_add = CL_OBJECT(clCreateKernel,(program, "rk_add", &status));
    
    if (cpu_switch){
        n_threads = 32;
    } else {
        CL_FUNCTION(clGetDeviceInfo,(device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &n_threads, NULL));
    }
    global_n_threads_elem = ((num_elem  / n_threads) + ((num_elem  % n_threads) ? 1 : 0))*n_threads;
  
    //clean up
    free(source_basis);
    free(source_constants);
    free(source_kernels);
    free(source_flux);
    free(source_ansatz);  

    //free local quadrature variables
    free(basis);
    free(basis_x);
    free(basis_endpoints);
    free(w);
    free(r);
}

void create_buffers(int n, int n_p, int num_elem,
                    double *X, double *h,
                    cl_mem *d_c, cl_mem *d_X, cl_mem *d_h) {


    double total_memory = (3*num_elem+1)*sizeof(double) +
                   M*num_elem*n_p*2*sizeof(double);

    //RK temp storage
    total_memory += (n+1)*M*num_elem*n_p*sizeof(double);

    if (total_memory < 1e3) {
        printf("Total memory required: %lf B\n", total_memory);
    } else if (total_memory >= 1e3 && total_memory < 1e6) {
        printf("Total memory required: %lf KB\n", total_memory * 1e-3);
    } else if (total_memory >= 1e6 && total_memory < 1e9) {
        printf("Total memory required: %lf MB\n", total_memory * 1e-6);
    } else {
        printf("Total memory required: %lf GB\n", total_memory * 1e-9);
    }


    *d_c = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, M * num_elem * n_p * sizeof(double),NULL, &status));
    *d_h = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_elem * sizeof(double),h, &status));
    *d_X = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, (num_elem+1)* sizeof(double),X, &status));
    
    
    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h, 0, sizeof(cl_mem), (void *)d_c));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h, 1, sizeof(cl_mem), (void *)d_h));  
    CL_FUNCTION(clSetKernelArg,(kernel_eval_rhs,  2, sizeof(cl_mem), (void *)d_h));
}