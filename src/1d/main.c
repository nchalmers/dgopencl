/* main.c
 *
 * Contains the main functions to read the mesh mappings and run the GPU code
 * to solve the specific problem .
 *
 * Does the following
 * 1. get user input
 * 2. generate the mesh
 * 3. allocate memory on the GPU
 * 4. get integration points and weights
 * 5. call basis.c to precompute basis functions at those integration points
 * 6. compute initial projection U_0 on GPU
 * 7. run the time integrator function
 * 8. write the result to file
 */
#include <CL/cl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

// limiter optoins
#define NO_LIMITER 0
#define LIMITER 1

#define MAX_SOURCE_SIZE (0x100000)

extern int M;
extern int limiter;
extern double CFL;


#include "quadrature.c"
#include "basis.c"
#include "init_OpenCL.c"
#include "time_integrator.c"


void set_mesh(int num_elem, double **X, double **h) {
	int i;
	
	double a = -1.;
	double b =  1.;
	
	//Uniform mesh
	double dx;
	
	dx = (b-a)/((double) num_elem);
	
	*X = (double *) malloc((num_elem + 1) * sizeof(double));
	*h = (double *) malloc((num_elem) * sizeof(double));
	
	(*X)[0] = a;
	for (i=0; i < num_elem; i++){
		(*X)[i+1] = a + (i+1)*dx;
		(*h)[i] = dx;
	}
}


void write_U(cl_mem d_c, double *X, int num, int total_timesteps, int num_elem) {
    double *Uv1, *Uv2;
    cl_mem d_Uv1, d_Uv2;
    int i, n;
    FILE *out_file;
    char out_filename[100];
    

    // evaluate at the vertex points and copy over data
    Uv1 = (double *) malloc(num_elem * sizeof(double));
    Uv2 = (double *) malloc(num_elem * sizeof(double));

	d_Uv1 = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, num_elem  * sizeof(double),NULL, &status));
	d_Uv2 = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, num_elem  * sizeof(double),NULL, &status));
	
    // evaluate and write to file
    for (n = 0; n < M; n++) {
    
        eval_u(&d_c, &d_Uv1, &d_Uv2, &n);
        
        CL_FUNCTION(clFinish,(command_queue));                                        
        CL_FUNCTION(clEnqueueReadBuffer,(command_queue, d_Uv1, CL_TRUE, 0, 
       									num_elem  * sizeof(double), (void *)Uv1, 0, NULL, NULL));
		CL_FUNCTION(clEnqueueReadBuffer,(command_queue, d_Uv2, CL_TRUE, 0, 
										num_elem  * sizeof(double), (void *)Uv2, 0, NULL, NULL));
		CL_FUNCTION(clFinish,(command_queue));
        if (num == total_timesteps) {
            sprintf(out_filename, "output/U%d-final.pos", n);
        } else {
            sprintf(out_filename, "output/video/U%d-%d.pos", n, num);
        }
        out_file  = fopen(out_filename , "w");
        fprintf(out_file, "#x U \n");
        for (i = 0; i < num_elem; i++) {
            fprintf(out_file, "%lf %lf\n", X[i],   Uv1[i]);
            fprintf(out_file, "%lf %lf\n", X[i+1], Uv2[i]);
        }
        fclose(out_file);
    }

    free(Uv1);
    free(Uv2);
    
    CL_FUNCTION(clReleaseMemObject,(d_Uv1));
    CL_FUNCTION(clReleaseMemObject,(d_Uv2)); 
}


/* set quadrature 
 *
 * sets the 1d quadrature integration points and weights 
 */
 
void set_quadrature(int n, int *n_quad, double **r, double **w) {
    int i;
    /*
     * The elements are mapped to the canonical element, [-1,1], so we want the integration points
     */
    
    *n_quad = n+1;
    
    // allocate integration points
    *r = (double *)  malloc(*n_quad * sizeof(double));
    *w  = (double *) malloc(*n_quad * sizeof(double));

    // set 1D quadrature rules
    for (i = 0; i < *n_quad; i++) {
        (*r)[i] = quad_1d[n][2*i];
        (*w)[i] = quad_1d[n][2*i+1];
    }
}     

void free_gpu(int stage) {
    switch (stage) {
        // free precomputed stuff
        case 1:      

            break;
        case 2:
            
            break;
    }
}

void usage_error() {
    printf("\nUsage: dgcuda [OPTIONS] [MESH] \n");
    printf(" Options: [-n] Order of polynomial approximation.\n");
    printf("          [-N] Number of Elements.\n");
    printf("          [-t] Number of timesteps.\n");
    printf("          [-T] End time.\n");
    printf("          [-v] Verbose.\n");
    printf("          [-V] Print out every N timesteps.\n");
    printf("          [-b] Benchmark.\n");
    printf("          [-cpu] Force use of CPU.\n");
    printf("          [-h] Display this message.\n");
}

int get_input(int argc, char *argv[], int *n, int *num_elem,
              double *endtime, int *total_timesteps,
              int *verbose, int *benchmark,
              int *video, int *cpu_switch) {

    int i;

    *endtime = -1;
    *total_timesteps = -1;
    *verbose         = 0;
    *benchmark       = 0;
    *video           = 0;
    *cpu_switch     = 0;
    
    // read command line input
    if (argc < 5) {
        usage_error();
        return 1;
    }
    for (i = 0; i < argc; i++) {
        // order of polynomial
        if (strcmp(argv[i], "-n") == 0) {
            if (i + 1 < argc) {
                *n = atoi(argv[i+1]);
                if (*n < 0 || *n > 5) {
                    usage_error();
                    return 1;
                }
            } else {
                usage_error();
                return 1;
            }
        }
        
        if (strcmp(argv[i], "-N") == 0) {
            if (i + 1 < argc) {
                *num_elem = atoi(argv[i+1]);
                if (*num_elem < 1) {
                    usage_error();
                    return 1;
                }
            } else {
                usage_error();
                return 1;
            }
        }
        
        // number of total_timesteps
        if (strcmp(argv[i], "-t") == 0) {
            if (i + 1 < argc) {
                *total_timesteps = atoi(argv[i+1]);
                if (*total_timesteps < 0) {
                    usage_error();
                    return 1;
                }
            } else {
                usage_error();
                return 1;
            }
        }
        if (strcmp(argv[i], "-T") == 0) {
            if (i + 1 < argc) {
                *endtime = atof(argv[i+1]);
                if (*endtime < 0) {
                    usage_error();
                    return 1;
                }
            } else {
                usage_error();
                return 1;
            }
        }
        if (strcmp(argv[i], "-V") == 0) {
            if (i + 1 < argc) {
                *video = atof(argv[i+1]);
                if (*video < 0) {
                    usage_error();
                    return 1;
                }
            } else {
                usage_error();
                return 1;
            }
        }      
        if (strcmp(argv[i], "-b") == 0) {
            *benchmark = 1;
        }
        if (strcmp(argv[i], "-v") == 0) {
            *verbose = 1;
        }
        if (strcmp(argv[i], "-cpu") == 0) {
            *cpu_switch = 1;
        }
        if (strcmp(argv[i], "-h") == 0) {
                usage_error();
                return 1;
        }
    } 

    return 0;
}


int run_dgopencl(int argc, char *argv[], char *flux_CL_filename, char *ansatz_CL_filename) {
    //Problem data
    int n, n_p; 
    int total_timesteps, runs;
    int verbose, video, benchmark, cpu_switch;
    double  endtime, t;

    //Mesh data
    int num_elem;
    double *X, *h;
    double min_h;

    //Quadrature data
    int n_quad;
    double *r, *w;
    double *basis, *basis_x, *basis_endpoints; 

    //Benchmark variables
    clock_t start, end;
    double elapsed;


	int i;

    //GPU Buffers
    cl_mem d_c;
    cl_mem d_X, d_h;
    
    // get input 
    endtime = -1;
    if (get_input(argc, argv, &n, &num_elem, &endtime, &total_timesteps,
              &verbose, &benchmark, &video, &cpu_switch)) {
        return 1;
    }
	
    n_p = n + 1;

	//Create mesh
	set_mesh(num_elem, &X, &h);

    // get the correct quadrature rules for this scheme
    set_quadrature(n, &n_quad, &r, &w); 
   
    // evaluate the basis functions at those points
    preval_basis(n_p, n_quad, r, w, &basis, &basis_x, &basis_endpoints); 
   
    //Initialize and create OpenCL kernels                             
    init_OpenCL(flux_CL_filename, ansatz_CL_filename, &cpu_switch,
                n_p, num_elem, n_quad, basis, basis_x, basis_endpoints, r, w);

    //Copy mesh data to the GPU
    create_buffers(n, n_p, num_elem, X, h, &d_c, &d_X, &d_h);
           
    // find the min h   
    min_h = h[0];
    for (i = 1; i < num_elem; i++) {
        min_h = (h[i] < min_h) ? h[i] : min_h;
    }
    free(h);

    // initial conditions
    initial_conditions(d_c,d_X,d_h);
    if (limiter) {
    	limit_c(&d_c);	
    }
    CL_FUNCTION(clFinish,(command_queue));  
   
    
    printf(" ? %i degree polynomial interpolation (n_p = %i)\n", n, n_p);
    printf(" ? %i precomputed basis points\n", n_quad * n_p);
    printf(" ? %i elements\n", num_elem);
    printf(" ? min h = %.015lf\n", min_h);

    if (endtime == -1) {
        printf(" ? total_timesteps = %i\n", total_timesteps);
    } else if (endtime != -1) {
        printf(" ? endtime = %lf\n", endtime);
    }

    if (benchmark) {
        start = clock();
    }
    
    //Time step
    t = time_integrate(n, n_p, num_elem, endtime, total_timesteps,
                        video, verbose, d_c, X);
    
    if (benchmark) {
        end = clock();
        elapsed = ((double)(end - start)) / CLOCKS_PER_SEC;
        printf("Runtime: %lf seconds\n", elapsed);
    }

    CL_FUNCTION(clReleaseMemObject,(d_X));
    CL_FUNCTION(clReleaseMemObject,(d_h)); 

	// evaluate and write U to file
    write_U(d_c, X, total_timesteps, total_timesteps, num_elem);      
            
    //free everything        
    CL_FUNCTION(clReleaseMemObject,(d_c));
    CL_FUNCTION(clReleaseKernel,(kernel_initial_conditions));
    CL_FUNCTION(clReleaseKernel,(kernel_eval_rhs));
    CL_FUNCTION(clReleaseKernel,(kernel_limit_c));
    CL_FUNCTION(clReleaseKernel,(kernel_rk_add));
    CL_FUNCTION(clReleaseKernel,(kernel_eval_u));
    CL_FUNCTION(clReleaseKernel,(kernel_eval_global_lambda_h));
    
    CL_FUNCTION(clReleaseProgram,(program));
    CL_FUNCTION(clReleaseCommandQueue,(command_queue));
    CL_FUNCTION(clReleaseContext,(context));

    //free(X);

    return 0;
}
