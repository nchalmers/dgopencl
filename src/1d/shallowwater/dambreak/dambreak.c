#include "../../main.c"

/* dambreak.c
 *
 * 1d shallow water equations with dam break initial condition
 *
 */

#define PI 3.14159265358979323

int limiter = NO_LIMITER;  // no limiter
double CFL = 1;
int M = 2;


/***********************
 *
 * MAIN FUNCTION
 *
 ***********************/

int main(int argc, char *argv[]) {
    run_dgopencl(argc, argv,"shallowwater.cl","dambreak/dambreak.cl");
}
