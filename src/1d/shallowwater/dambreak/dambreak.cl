
#define PI 3.14159265358979323
#define G 9.8


/***********************
 *
 * INITIAL CONDITIONS
 *
 ***********************/

/* initial condition function
 *
 * returns the value of the intial condition at point x,y
 */

void U0(double *U, double x) {

    if ( x <= 0 ){
        U[0] = 4;
    } else {
        U[0] = 1;
    }

	U[1] = 0.;
}


/***********************
*
* BOUNDARY CONDITIONS
*
************************/

/*
* By default the boundary conditions pass in the left and right boudnary values of the solution 
* at the end on the interval. 
*/

void eval_boundary_left(double *U_minus, double *U_plus, double t){
	//Assign U_minus

	//Do nothing = Periodic 

	//Free flow
	U_minus[0] = 4;
	U_minus[1] = 0;
}

void eval_boundary_right(double *U_minus, double *U_plus, double t){
	//Assign U_plus

	//Do nothing = Periodic

	//Free flow
	U_plus[0] = 1;
	U_plus[1] = 0;
 
}

/***********************
*
* EXACT SOLUTION
*
************************/
 
void U_exact(double *U, double x, double t) {
    
}