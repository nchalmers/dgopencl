/* euler.cl
 *
 * This file contains the relevant information for making a system to solve
 *
 * d_t [   rho   ] + d_x [     rho * u    ] = 0
 * d_t [ rho * u ] + d_x [ rho * u^2 + p  ] = 0
 * d_t [    E    ] + d_x [ u * ( E +  p ) ] = 0
 *
 */

void get_coordinates_1d(double *x, double *V, int j);

/***********************
 *
 * EULER DEVICE FUNCTIONS
 *
 ***********************/
/*
void evalU0(double *U, double *V, int i) {
    int j;
    double u0[4];
    double X,Y;

    U[0] = 0.;
    U[1] = 0.;
    U[2] = 0.;
    U[3] = 0.;

    for (j = 0; j < n_quad; j++) {

        // get the 2d point on the mesh
        get_coordinates_2d(&X, &Y, V, j);

        // evaluate U0 here
        U0(u0, X, Y);

        // evaluate U at the integration point
        U[0] += w[j] * u0[0] * basis[i * n_quad + j];
        U[1] += w[j] * u0[1] * basis[i * n_quad + j];
        U[2] += w[j] * u0[2] * basis[i * n_quad + j];
        U[3] += w[j] * u0[3] * basis[i * n_quad + j];
    }
}
*/



/* evaluate pressure
 *
 * evaluates the pressure for U
 */
double pressure(double *U) {

    double rho, rhou, E;
    rho  = U[0];
    rhou = U[1];
    E    = U[2];
    
    return (get_GAMMA() - 1.) * (E - (rhou*rhou) / 2. / rho);
}

/* evaluate c
 *
 * evaulates the speed of sound c
 */
double eval_c(double *U) {
    double p = pressure(U);
    double rho = U[0];

    return sqrt(get_GAMMA() * p / rho);
}    

/***********************
 *
 * EULER FLUX
 *
 ***********************/
/* takes the actual values of rho, u, v, and E and returns the flux 
 * x and y components. 
 * NOTE: this needs the ACTUAL values for u and v, NOT rho * u, rho * v.
 */
void eval_flux(double *flux, double *U) {

    // evaluate pressure
    double rho, rhou, E;
    double p = pressure(U);
    rho  = U[0];
    rhou = U[1];
    E    = U[2];

    // flux
    flux[0] = rhou;
    flux[1] = rhou * rhou / rho + p;
    flux[2] = rhou * (E + p) / rho;
}

/***********************
 *
 * RIEMAN SOLVER
 *
 ***********************/
/* finds the max absolute value of the jacobian for F(u).
 *  |u - c|, |u|, |u + c|
 */
double eval_lambda(double *U_left, double *U_right) {
                              
    double c_left, c_right;
    double u_left, u_right;
    double left_max, right_max;

    // get c for both sides
    c_left  = eval_c(U_left);
    c_right = eval_c(U_right);

    // find the speeds on each side
    u_left  = U_left[1] / U_left[0];
    u_right = U_right[1] / U_right[0];
    
    // if speed is positive, want u + c, else u - c
    if (u_left > 0.) {
        left_max = u_left + c_left;
    } else {
        left_max = -u_left + c_left;
    }

    // if speed is positive, want u + c, else u - c
    if (u_right > 0.) {
        right_max = u_right + c_right;
    } else {
        right_max = -u_right + c_right;
    }

    // return the max absolute value of | u +- c |
    if (fabs(left_max) > fabs(right_max)) {
        return fabs(left_max);
    } else { 
        return fabs(right_max);
    }
}

/* 
 * local lax-friedrichs riemann solver
 */
void riemann_solver(double *F_n, double *U_left, double *U_right) {
    int n;

    double flux_l[3], flux_r[3];

    // calculate the left and right fluxes
    eval_flux(flux_l, U_left);
    eval_flux(flux_r, U_right);

    double lambda = eval_lambda(U_left, U_right);

    // calculate the riemann problem at this integration point
    for (n = 0; n < M; n++) {
        F_n[n] = 0.5 * (flux_l[n] + flux_r[n]  + lambda * (U_left[n] - U_right[n]));
    }
}

/***********************
 *
 * CFL CONDITION
 *
 ***********************/
/* global lambda evaluation
 *
 * computes the max eigenvalue of |u + c|, |u|, |u - c|.
 */
__kernel void eval_global_lambda_h(__global double *C,__global double *h, __global double *lambda_h) {
    int idx = get_global_id(0);
    if (idx < num_elem) { 
        double c, s;

        double U[3];
        // get cell averages
        U[0] = C[num_elem * NP * 0 + idx] * basis[0];
        U[1] = C[num_elem * NP * 1 + idx] * basis[0];
        U[2] = C[num_elem * NP * 2 + idx] * basis[0];

        // evaluate c
        c = eval_c(U);

        // speed of the wave
        s = U[1]/U[0];

        // return the max eigenvalue
        if (s > 0) {
            lambda_h[idx] = h[idx]/(s + c);
        } else {
            lambda_h[idx] = h[idx]/(-s + c);
        }
    }
}

/* evaluate pressure
 * 
 * evaluates pressure at the three vertex points for output
 * THREADS: num_elem
 */
 /*
__kernel void eval_pressure(__global double *C,__global double *Uv1,
						   __global double *Uv2,__global double *Uv3) {
    int idx = get_global_id(0);
    if (idx < num_elem) {
        int i, n;
        double U1[4], U2[4], U3[4];

        // calculate values at the integration points
        for (n = 0; n < M; n++) {
            U1[n] = 0.;
            U2[n] = 0.;
            U3[n] = 0.;
            for (i = 0; i < n_p; i++) {
                U1[n] += C[num_elem * n_p * n + i * num_elem + idx] * basis_vertex[i * 3 + 0];
                U3[n] += C[num_elem * n_p * n + i * num_elem + idx] * basis_vertex[i * 3 + 1];
                U2[n] += C[num_elem * n_p * n + i * num_elem + idx] * basis_vertex[i * 3 + 2];
            }
        }

        // store result
        Uv1[idx] = pressure(U1);
        Uv2[idx] = pressure(U2);
        Uv3[idx] = pressure(U3);
    }
}
*/

