#include "../../main.c"

/* smooth.c
 *
 * 1d euler equations with smooth initial condition
 *
 */

#define PI 3.14159265358979323

int limiter = NO_LIMITER;  // no limiter
double CFL = 1;
int M = 3;


/***********************
 *
 * MAIN FUNCTION
 *
 ***********************/

int main(int argc, char *argv[]) {
    run_dgopencl(argc, argv,"euler.cl","smooth/smooth.cl");
}
