
#define PI 3.14159265358979323
#define GAMMA 1.4


double get_GAMMA() {
    return GAMMA;
}
/***********************
 *
 * INITIAL CONDITIONS
 *
 ***********************/

/* initial condition function
 *
 * returns the value of the intial condition at point x,y
 */

void U0(double *U, double x) {

    double p, s;
    double alpha = 0.05;


    if ((-0.1<=x) && (x <= 0.1)){
        p = 1+alpha*(1+cos(10*PI*x));
    } else {
        p = 1;
    }
	
    s = 0;

	U[0] = get_GAMMA();
	U[1] = U[0]*s;
	U[2] = p/(get_GAMMA()-1) + 0.5*U[0]*(s*s);

}


/***********************
*
* BOUNDARY CONDITIONS
*
************************/

/*
* By default the boundary conditions pass in the left and right boudnary values of the solution 
* at the end on the interval. 
*/

void eval_boundary_left(double *U_minus, double *U_plus, double t){
	//Assign U_minus

	//Do nothing = Periodic 

	//Free flow
	U_minus[0] = get_GAMMA();
	U_minus[1] = 0;
	U_minus[2] = 1/(get_GAMMA()-1);
 
}

void eval_boundary_right(double *U_minus, double *U_plus, double t){
	//Assign U_plus

	//Do nothing = Periodic

	//Free flow
	U_plus[0] = get_GAMMA();
	U_plus[1] = 0;
	U_plus[2] = 1/(get_GAMMA()-1);
 
}

/***********************
*
* EXACT SOLUTION
*
************************/
 
void U_exact(double *U, double x, double t) {
    
}