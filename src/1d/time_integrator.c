/* time_integrator.c
 *
 * time integration functions.
 */


double RK1_a[] = {0.};
double RK1_b[] = {1.};
double RK1_c[] = {0.};

double RK2_a[] = {0.,0.5};
double RK2_b[] = {0.,1.};
double RK2_c[] = {0.,0.5};

double RK4_a[] = {0.,0.5, 0.5, 1.0};
double RK4_b[] = {1./6., 1./3., 1./3., 1./6.};
double RK4_c[] = {0., 0.5, 0.5, 1.};

void write_U(cl_mem d_c, double *X, int num, int total_timesteps, int num_elem);


void ButcherTableau(int n, int n_p, int num_elem, int *num_stages, double **a, double **b, double **c, cl_mem **d_k){
	int i;
	
	switch (n) {
        case 0: //Forward Euler
            *num_stages = 1;
            *a = (double *)malloc((*num_stages)*sizeof(double));
            *b = (double *)malloc(*num_stages*sizeof(double));
            *c = (double *)malloc(*num_stages*sizeof(double));
            *d_k = (cl_mem *)malloc(*num_stages*sizeof(cl_mem));
            
            for (i=0;i<*num_stages;i++){
                (*a)[i] = RK1_a[i];
                (*b)[i] = RK1_b[i];
                (*c)[i] = RK1_c[i];
                (*d_k)[i] = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, M * num_elem * n_p *sizeof(double),NULL, &status));
            }
            break;
        case 1: //RK2
            *num_stages = 2;
            *a = (double *)malloc((*num_stages)*sizeof(double));
            *b = (double *)malloc(*num_stages*sizeof(double));
            *c = (double *)malloc(*num_stages*sizeof(double));
            *d_k = (cl_mem *)malloc(*num_stages*sizeof(cl_mem));
            
            for (i=0;i<*num_stages;i++){
                (*a)[i] = RK2_a[i];
                (*b)[i] = RK2_b[i];
                (*c)[i] = RK2_c[i];
                (*d_k)[i] = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, M * num_elem * n_p *sizeof(double),NULL, &status));
            }
            break;
        default: //RK4
            *num_stages = 4;
            *a = (double *)malloc(*num_stages*sizeof(double));
            *b = (double *)malloc(*num_stages*sizeof(double));
            *c = (double *)malloc(*num_stages*sizeof(double));
            *d_k = (cl_mem *)malloc(*num_stages*sizeof(cl_mem));
            
            for (i=0;i<*num_stages;i++){
                (*a)[i] = RK4_a[i];
                (*b)[i] = RK4_b[i];
                (*c)[i] = RK4_c[i];
                (*d_k)[i] = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, M * num_elem * n_p *sizeof(double),NULL, &status));
            } 
    } 
}


/* time integrate 
 *
 * uses runge-kutta time integration to time-step.
 * returns the final time this runs to.
 */
double time_integrate(int n, int n_p, int num_elem,
                    double endtime, int total_timesteps,
                    int video, int verbose,
                    cl_mem d_c, double *X) {
    int i, vidnum = 0;
    double dt, t = 0.0;
    int timestep = 0;
	
	//RK Butcher tableau
	int m,l, num_stages;
	double *a, *b, *c; 
	
	double *lambda_h = (double *) malloc(num_elem * sizeof(double));
    double min_lambda_h;
    
    cl_mem *d_k;
    cl_mem d_rhs, d_lambda_h;

	ButcherTableau(n, n_p, num_elem, &num_stages,&a,&b,&c,&d_k);
      
    d_lambda_h = CL_OBJECT(clCreateBuffer,(context,CL_MEM_READ_WRITE,num_elem* sizeof(double),NULL, &status));
    d_rhs      = CL_OBJECT(clCreateBuffer,(context,CL_MEM_READ_WRITE,M * n_p * num_elem * sizeof(double),NULL, &status));

    CL_FUNCTION(clSetKernelArg,(kernel_eval_rhs, 0, sizeof(cl_mem), (void *) &d_rhs));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h, 2, sizeof(cl_mem), (void *) &d_lambda_h));

    printf("Computing...\n");
    
    while (t < endtime || (timestep < total_timesteps && total_timesteps != -1)) {
        
        // compute all the lambda values over each cell
        eval_global_lambda_h();

        // grab all the lambdas off the GPU and find the min one
        CL_FUNCTION(clEnqueueReadBuffer,(command_queue, d_lambda_h, CL_TRUE, 0, 
        						num_elem  * sizeof(double), (void *)lambda_h, 0, NULL, NULL));
        min_lambda_h = lambda_h[0];
        for (i = 0; i < num_elem; i++) {
            min_lambda_h = (lambda_h[i] < min_lambda_h) ? lambda_h[i] : min_lambda_h;
        }

        timestep++;

        //cfl condition
        dt = CFL * min_lambda_h /  (2. * n + 1.);

        // panic
        if (isnan(dt)) {
            printf("Error: dt is NaN. Dumping...\n");
            return t;
        }

        // get next timestep
        if (t + dt > endtime && total_timesteps == -1) {
            dt = endtime - t;
        }


		CL_FUNCTION(clEnqueueCopyBuffer,(command_queue,d_c,d_k[0],0,0,M * num_elem * n_p * sizeof(double),0,NULL,NULL)); 
		//RK Time Stepping
		for (m=0;m<num_stages;m++){
			eval_rhs(&d_k[m], t+c[m]*dt, dt);
			CL_FUNCTION(clEnqueueBarrierWithWaitList,(command_queue,0,NULL,NULL));

			if (m != num_stages-1) {
				rk_add(&d_k[m+1], &d_c, &d_rhs, a[m+1]);  
				CL_FUNCTION(clEnqueueBarrierWithWaitList,(command_queue,0,NULL,NULL));
				if (limiter) {
					limit_c(&d_k[m+1]);
					CL_FUNCTION(clEnqueueBarrierWithWaitList,(command_queue,0,NULL,NULL));
				}
			} else {//Final Stage
				CL_FUNCTION(clEnqueueCopyBuffer,(command_queue,d_rhs,d_k[num_stages-1],0,0,M * num_elem * n_p * sizeof(double),0,NULL,NULL)); 
                for (l=0;l<num_stages;l++){
					rk_add(&d_c, &d_c, &d_k[l], b[l]);   
					CL_FUNCTION(clEnqueueBarrierWithWaitList,(command_queue,0,NULL,NULL));
				}
				
				if (limiter) {
					limit_c(&d_c);
					CL_FUNCTION(clEnqueueBarrierWithWaitList,(command_queue,0,NULL,NULL));
				}    
			} 
            CL_FUNCTION(clEnqueueBarrierWithWaitList,(command_queue,0,NULL,NULL));               	 
		}
        CL_FUNCTION(clFinish,(command_queue));

        t += dt;
        if (verbose == 1) {
            printf("(%i) t = %lf, dt = %lf, min lambda_h = %lf\n", timestep, t, dt, min_lambda_h);
        } else {
            printf("\r(%i) t = %lf", timestep, t);
        }
	}


    printf("\n");

    //clean up
    free(lambda_h);
    free(a);
    free(b);
    free(c);

    CL_FUNCTION(clReleaseMemObject,(d_lambda_h));
    for (m=0;m<num_stages;m++){
        CL_FUNCTION(clReleaseMemObject,(d_k[m]));
    }
    free(d_k);

    return t;
}
