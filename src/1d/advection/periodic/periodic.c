#include "../../main.c"

/* periodic.c
 *
 * simple advection with periodic boundary conditions
 *
 */

#define PI 3.14159265358979323

int limiter = NO_LIMITER;  // no limiter
double CFL = 1;
int M = 1;


/***********************
 *
 * MAIN FUNCTION
 *
 ***********************/

int main(int argc, char *argv[]) {
    run_dgopencl(argc, argv,"advection.cl","periodic/periodic.cl");
}
