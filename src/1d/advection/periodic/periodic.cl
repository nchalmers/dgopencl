
#define PI 3.14159265358979323

/***********************
 *
 * INITIAL CONDITIONS
 *
 ***********************/

/* initial condition function
 *
 * returns the value of the intial condition at point x,y
 */

void U0(double *U, double x) {
    U[0] = sin(PI*x);
}


/***********************
*
* BOUNDARY CONDITIONS
*
************************/

/*
* By default the boundary conditions pass in the left and right boudnary values of the solution 
* at the end on the interval. 
*/

void eval_boundary_left(double *U_minus, double *U_plus, double t){
	//Assign U_minus

	//Do nothing = Periodic 

	//U_minus[0] = sin(PI*(-1-t));

}

void eval_boundary_right(double *U_minus, double *U_plus, double t){
	//Assign U_plus

	//Do nothing = Periodic

	//U_plus[0] = sin(PI*(1-t));
}

/***********************
*
* EXACT SOLUTION
*
************************/
double get_velocity();
 
void U_exact(double *U, double x, double y, double t) {
    U0(U, x - get_velocity()*t);
}