
// * advection.cl
// *
// * This file contains the relevant information for making a system to solve
// *
// * d_t [ u ] + d_x [ au ]  = 0
// *

double get_velocity() {

    return 1.;

}

void get_coordinates_1d(double *x, double *V, int j);

//***********************
// *
// * ADVECTION FLUX
// *
// ***********************/
 //
 //  sets the flux for advection
 //
void eval_flux(double *F, double *U) {
    F[0] = get_velocity() * U[0];
}

//***********************
// *
// * RIEMAN SOLVER
// *
// ***********************/
/* finds the max absolute value of the jacobian for F(u).
 */

//Upwind Flux
void riemann_solver_upwind(double *F_n, double *U_left, double *U_right) {
    double flux_l, flux_r;

    // calculate the left and right fluxes
    eval_flux(&flux_l, U_left);
    eval_flux(&flux_r, U_right);

    F_n[0] = 0.5*(flux_r + flux_l) - 0.5*fabs(get_velocity())*(U_right[0] - U_left[0]);

}
 
void riemann_solver(double *F_n, double *U_left, double *U_right) {
    
    riemann_solver_upwind(F_n, U_left, U_right);

}

//***********************
// *
// * CFL CONDITION
// *
// ***********************/
//* global lambda evaluation
// *
// * computes the max eigenvalue.
//
__kernel void eval_global_lambda_h(__global double *c, __global double *h, __global double *lambda_h) {
    int idx = get_global_id(0);
    
    if (idx < num_elem) { 
        lambda_h[idx] = h[idx]/fabs(get_velocity());
    }
}
