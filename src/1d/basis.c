/* basis.cu
 *
 * Stores the coefficients for the basis functions as
 *
 * phi_j = c[0][j] 
 *       + c[1][j] * x + c[2][j] * y 
 *       + c[3][j] * x^2 + c[4][j] * x*y + c[5][j] * y^2
 *       + c[6][j] * x^3 + c[7][j] * x^2 * y + c[8][j] * x * y^2 + c[9][j] * y^3 
 *       + ...
 *
 * and so forth.
 *
 * This file is called to precompute the basis functions $phi_j$ at 
 *   1. one-dimensional integration point along the interval [-1, 1] mapped to each side of the canonical triangle
 *   2. two-dimensional integration points inside the canonical  triangle [(0,0), (1,0), (0,1)]
 *   3. verticies of the canonical triangle
 *   4. gradients evaluated at the two-dimensional integration points
 */
 
void checkOpenCLError(cl_int status, const char *message);
 
const int size = 11;
const static double basis_coeff[11][11] = {
 { 	     1., 		0., 		0., 		0., 		  0., 		   0., 			 0., 		   0., 			  0., 		   0., 			0.},
 {	     0., 		1., 		0., 		0., 		  0., 		   0., 			 0., 		   0., 			  0., 		   0., 			0.},
 {   -1./2., 		0., 	 3./2., 		0., 		  0., 		   0., 			 0., 		   0., 			  0., 		   0., 			0.},
 {       0., 	-3./2., 		0., 	 5./2., 		  0., 		   0., 			 0., 		   0., 			  0., 		   0., 			0.},
 {    3./8., 		0.,    -30./8., 		0., 	  35./8., 		   0., 			 0., 		   0., 			  0., 		   0., 			0.},
 {       0., 	15./8., 		0.,    -70./8., 		  0., 	   63./8., 			 0., 		   0., 			  0., 		   0., 			0.},
 {  -5./16., 		0.,   105./16., 		0., 	-315/16., 		   0., 	   231./16., 		   0., 			  0., 		   0., 			0.},
 {       0.,  -35./16., 		0.,   315./16., 		  0., 	-693./16., 		 	 0., 	 429./17., 			  0., 		   0., 			0.},
 { 35./128., 		0., -1260./128, 		0.,   6930./128., 		   0., -12012./128., 		   0.,    6435./128., 		   0., 		 	0.},
 {       0., 315./128., 	    0., -4620./128, 	      0., 18018./128., 		  	 0., -25740./128., 		      0., 12155./128., 		 	0.},
 {-63./256., 		0.,  3465./256, 		0., -30030./256., 		   0.,  90090./256., 	       0., -109395./256., 		   0., 46189./256.}
};

double phi(double r, int n) {
    int j; 
    double sum = 0.;

    double R[size];
    
    for (j = 0; j <= n; j++) {
        R[j] = pow(r, j);
    }
    
    for (j = 0; j <= n; j++) {
        sum += basis_coeff[n][j] * R[j];
    }

    return sum;
}

double phi_x(double r, int n) {
    int j;
    double sum = 0.;

    double R[size];

	for (j = 1; j <= n; j++) {
		R[j] = j * pow(r, j - 1);
	}
       
    for (j = 1; j <= n; j++) {
        sum += basis_coeff[n][j] * R[j];
    }

    return sum;
}

void preval_basis(int n_p, int n_quad, double *r,double *w, double **basis,
				double **basis_x,	double **basis_endpoints) {
    				
    *basis           = (double *) malloc(n_quad * n_p * sizeof(double));
    *basis_x         = (double *) malloc(n_quad * n_p * sizeof(double)); 
    *basis_endpoints = (double *) malloc(2 * n_p * sizeof(double));

    int i, j;

    for (i = 0; i < n_p; i++) {
        // precompute the values at the vertex points
        (*basis_endpoints)[i * 2 + 0] = phi(-1., i);
        (*basis_endpoints)[i * 2 + 1] = phi( 1., i);
            

        //precompute the quadrature nodes on the elements for the basis & gradients
        for (j = 0; j < n_quad; j++) {
            (*basis)[i * n_quad + j] = phi(r[j], i);
            (*basis_x)[i * n_quad + j] = w[j] * phi_x(r[j], i);
        }
    }    
}
