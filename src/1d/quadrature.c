////////////////////////////////////////
// 1D Quadrature Rules
////////////////////////////////////////

// order goes (r1, w1, r2, w2, ...)
double quad_1d_degree1[] = {0.0000000000000000e+00, 2.000000000000000e+00};
double quad_1d_degree2[] = {-5.773502691896257e-01,1.,
                             5.773502691896257e-01, 1.};

double quad_1d_degree3[] = {-7.745966692414834e-01, 5.555555555555552e-01, 
                           0.000000000000000e0, 8.888888888888888e-01,  
                           7.745966692414834e-01, 5.555555555555552e-01};

double quad_1d_degree4[] = {-8.611363115940526e-01,3.478548451374537e-01, 
                            -3.399810435848563e-01, 6.521451548625464e-01,  
                            3.399810435848563e-01, 6.521451548625464e-01, 
                            8.611363115940526e-01, 3.478548451374537e-01};
//double quad_1d_degree4[] = {-sqrt((3.+2.*sqrt(6./5))/7.), (18.-sqrt(30.))/36.,
                           //-sqrt((3.-2.*sqrt(6./5))/7.), (18.+sqrt(30.))/36.,
                           //sqrt((3.-2.*sqrt(6./5))/7.), (18.+sqrt(30.))/36.,
                           //sqrt((3.+2.*sqrt(6./5))/7.), (18.-sqrt(30.))/36.};

double quad_1d_degree5[] = {-9.061798459386640e-01,2.369268850561890e-01, 
                            -5.384693101056831e-01,  4.786286704993665e-01, 
                            0.000000000000000e+00, 5.688888888888889e-01, 
                            5.384693101056831e-01, 4.786286704993665e-01, 
                            9.061798459386640e-01,2.369268850561890e-01};
//double quad_1d_degree5[] = {-sqrt(5.+2.*sqrt(10./7))/3., (322.-13.*sqrt(70.))/900.,
                           //-sqrt(5.-2.*sqrt(10./7))/3., (322.+13.*sqrt(70.))/900.,
                           //0., 128./225,
                           //sqrt(5.-2.*sqrt(10./7))/3., (322.+13.*sqrt(70.))/900.,
                           //sqrt(5.+2.*sqrt(10./7))/3., (322.-13.*sqrt(70.))/900.};
double quad_1d_degree6[] = {-9.324695142031521e-01,1.713244923791705e-01, 
                            -6.612093864662646e-01, 3.607615730481386e-01, 
                            -2.386191860831969e-01,4.679139345726913e-01, 
                            2.386191860831969e-01, 4.679139345726913e-01, 
                            6.612093864662646e-01, 3.607615730481386e-01, 
                            9.324695142031521e-01, 1.713244923791705e-01};
//double quad_1d_degree6[] = {-0.93246951, 0.17132449,
                           //-0.66120939, 0.36076157,
                           //-0.23861918, 0.46791393,
                           //0.23861918, 0.46791393,
                           //0.66120939, 0.36076157,
                           //0.93246951, 0.17132449};
double quad_1d_degree7[] = {-9.491079123427585e-01,1.294849661688697e-01, 
                            -7.415311855993945e-01, 2.797053914892767e-01, 
                            -4.058451513773972e-01, 3.818300505051190e-01, 
                            0.000000000000000e+00, 4.179591836734694e-01, 
                            4.058451513773972e-01, 3.818300505051190e-01, 
                            7.415311855993945e-01, 2.797053914892767e-01, 
                            9.491079123427585e-01, 1.294849661688697e-01};
//double quad_1d_degree7[] = {-0.94910791, 0.12948497,
                           //-0.74153119, 0.27970539,
                           //-0.40584515, 0.38183005,
                            //0., 0.41795918,
                           //0.40584515, 0.38183005,
                           //0.74153119, 0.27970539,
                           //0.94910791, 0.12948497};
double quad_1d_degree8[] = {-9.602898564975363e-01,1.012285362903768e-01, 
                            -7.966664774136268e-01, 2.223810344533745e-01, 
                            -5.255324099163290e-01,3.137066458778874e-01, 
                            -1.834346424956498e-01, 3.626837833783620e-01, 
                            1.834346424956498e-01, 3.626837833783620e-01, 
                            5.255324099163290e-01, 3.137066458778874e-01, 
                            7.966664774136268e-01, 2.223810344533745e-01, 
                            9.602898564975363e-01, 1.012285362903768e-01};
//double quad_1d_degree8[] = {-0.18343464, 0.36268378,
                           //-0.96028986, 0.10122854,
                           //-0.79666648, 0.22238103,
                           //-0.52553241, 0.31370665,
                           //0.18343464, 0.36268378,
                           //0.52553241, 0.31370665,
                           //0.79666648, 0.22238103,
                           //0.96028986, 0.10122854};

double *quad_1d[] = {quad_1d_degree1, quad_1d_degree2, quad_1d_degree3,
                    quad_1d_degree4, quad_1d_degree5, quad_1d_degree6,
                    quad_1d_degree7, quad_1d_degree8};
