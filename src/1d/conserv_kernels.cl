/* conserv_kernels.cl
 *
 * Contains the GPU variables and kernels to solve hyperbolic conservation laws in two-dimensions.
 * Stores all mesh variables and the following kernels
 * 
 * 1. Initial projection
 * 2. rhs calculation 
 * 3. limiter
 * 4. evaluate u 
 *
 */

//gets the grid cooridinates at the j'th integration point for 1d 
void get_coordinates_1d(double *X, double *V, int j) {

    // x = 0.5*(x_j+1 + x_j) + 0.5*(x_j+1 - x_j) * \xi_m
    *X = 0.5*(V[1]+V[0]) + 0.5*(V[1]-V[0]) * r[j]; 
}

double L2_projection(double *V, int i, int n) {
    int j;
    double X;

    double C = 0.;
	double U[M];


	//Numerically integrate U0 times the i'th basis function
    for (j = 0; j < n_quad; j++) {
        // get the 2d point on the mesh
        get_coordinates_1d(&X, V, j);
		
		U0(U,X);
		
        // evaluate U at the integration point
        C += w[j] * U[n] * basis[i * n_quad + j];
    }
    return C;
}


__kernel void initial_conditions(__global double *c,__global double *X, __global double *h) {
    int idx = get_global_id(0);
    int i, n;
    double V[2];

	if (idx < num_elem) {
        //Boundaries of Cell
        V[0] = X[idx];
        V[1] = X[idx+1];

        for (i = 0; i < NP; i++) {
             for (n = 0; n < M; n++) {
                 c[num_elem * NP * n + i * num_elem + idx] = (2*i+1)*L2_projection(V, i, n)/2;
             }
        } 
    }
}

/***********************
 *
 * MAIN FUNCTIONS
 *
 ***********************/

/* limiter
 *
 * Moment limiter
 *
 * THREADS: num_elem
 */

double minmod(double a, double b, double c) {

    if ((sign(a) == sign(b)) && (sign(b) == sign(c))) {

        return sign(a)*min(min(fabs(a),fabs(b)),fabs(c));
    } else {
        return 0.;
    }

}

//TODO: This should be changed to characteristic limiting for euler, shallow water, etc. 
__kernel void limit_c(__global double *c) {

    int idx = get_global_id(0);

    if (idx < num_elem) { 
        double C[NP], C_m[NP], C_p[NP], C_bar[NP];
        int n, i;
        int idx_m, idx_p;

        // get element neighbor indexes
        idx_m = (idx == 0) ? idx : idx - 1;
        idx_p = (idx == num_elem -1) ? idx : idx + 1;
        

        for (n = 0; n < M; n++) {

            //Read coefficients into registers
            for (i=0; i < NP; i++) {
                C[i]  = c[num_elem * NP * n + i * num_elem + idx];
                C_m[i]  = c[num_elem * NP * n + i * num_elem + idx_m];
                C_p[i]  = c[num_elem * NP * n + i * num_elem + idx_p];
                C_bar[i] = C[i];
            }
            
            //Moment Limiter
            for(i = NP; i>0; i--) {
                C_bar[i] = minmod(C_p[i-1] - C[i-1], C[i], C[i-1] - C_m[i-1]);

                if (C_bar[i] == C[i]){ //C[i] Wasn't limited
                    break;
                }
            }

            //Write back to global
            for (i=0; i < NP; i++) {
                c[num_elem * NP * n + i * num_elem + idx] = C_bar[i];
            }
        }
    }
}


/***********************
 * ASSEMBLE RHS FUNCTIONS
 ***********************/

/* volume integrals
 *
 * evaluates the volume integral
 * 
 */
void eval_volume_integral(double *F_int, double *C) {

    int j, n, k;

    double U[M];
    double F[M];

    // set to 0
    for (k = 0; k < NP; k++) {
        for (n = 0; n < M; n++) {
            F_int[NP * n + k] = 0.;
        }
    }


    // for each integration point
    for (j = 0; j < n_quad; j++) {

        // initialize to zero
        for (n = 0; n < M; n++) {
            U[n] = 0.;
        }
        

        // calculate at the integration point
        for (k = 0; k < NP; k++) {
            for (n = 0; n < M; n++) {
                U[n] += C[n*NP + k] * basis[n_quad * k + j];
            }
        }

        // evaluate the flux
        eval_flux(F, U);

        for (k = 0; k < NP; k++) {
            for (n = 0; n < M; n++) {
                // add the sum
                F_int[NP * n + k] += F[n] * basis_x[n_quad * k + j];
            }
        }

    } 
}


/* right hand side
 *
 * computes the sum of the quadrature and the riemann flux for the 
 * coefficients for each element
 * THREADS: num_elem
 */


__kernel void eval_rhs(__global double *rhs,__global double *c, __global double *h, double t, double dt) {
    int idx = get_global_id(0);
    int k, n;
    
    if (idx < num_elem) {
    	double C[M * NP]; 
	    double U_minusone[M], U_plusone[M]; //States on the left and right sides of the current element.
    	double U_left[M], U_right[M]; //States at left and right enpoints of the current cell.
    	double F_plus[M], F_minus[M]; //Riemann fluxes through the interfaces
    	double F_int[M * NP]; //Flux integral
    	
    	//Copy cell coefficients into registers
    	for (k=0; k < NP; k++) {
    		for (n=0; n < M; n++) {
    			C[n * NP + k]  = c[num_elem * NP * n + k * num_elem + idx];
    		}
        }
        
        // Evaluate the left and right endpoint values of the current cell.
    	for (n = 0; n < M; n++) {
            U_left[n]  = 0.;
            U_right[n] = 0.;
            U_minusone[n] = 0.;
            U_plusone[n]  = 0.;

            for (k = 0; k < NP; k++) {
                U_left[n]  += C[NP * n + k] * basis_endpoints[k * 2 + 0];
                U_right[n] += C[NP * n + k] * basis_endpoints[k * 2 + 1];
            }
        }

    	// Evaluate the left and right states at the boundaries
    	if (idx == 0) { //left boundary element
            for (n = 0; n < M; n++) {
                for (k = 0; k < NP; k++) {
                    U_minusone[n] += c[num_elem * NP * n + k * num_elem + num_elem - 1] * basis_endpoints[k * 2 + 1];
                    U_plusone[n]  += c[num_elem * NP * n + k * num_elem +      idx + 1] * basis_endpoints[k * 2 + 0];
                }
            }
    		eval_boundary_left(U_minusone, U_left, t); 
    	} else if (idx == num_elem - 1) { //right boundary element
            for (n = 0; n < M; n++) {
                for (k = 0; k < NP; k++) {
                    U_minusone[n] += c[num_elem * NP * n + k * num_elem + idx - 1] * basis_endpoints[k * 2 + 1];
                    U_plusone[n]  += c[num_elem * NP * n + k * num_elem +       0] * basis_endpoints[k * 2 + 0];
                }
            }
    		eval_boundary_right(U_right, U_plusone, t);
    	} else {
    		for (n = 0; n < M; n++) {
                for (k = 0; k < NP; k++) {
                    U_minusone[n] += c[num_elem * NP * n + k * num_elem + idx - 1] * basis_endpoints[k * 2 + 1];
                    U_plusone[n]  += c[num_elem * NP * n + k * num_elem + idx + 1] * basis_endpoints[k * 2 + 0];
                }
            }
        }

        //Call the riemann solver to evaluate the interface fluxes. 
        riemann_solver(F_plus,  U_right,    U_plusone);
        riemann_solver(F_minus, U_minusone, U_left );   		        
        
    	//Calculate the flux integral
    	eval_volume_integral(F_int, C);

        //mem_fence(CLK_GLOBAL_MEM_FENCE);

    	//Add everything together.
    	for (n = 0; n < M; n++) {
	    	for (k = 0; k < NP; k++) {
	    		rhs[num_elem * NP * n + k * num_elem + idx] = -(2*k+1)*dt * (F_plus[n] - basis_endpoints[k * 2 + 0] * F_minus[n] - F_int[NP * n + k])/h[idx];
            }   
        }
    }
}



/***********************
 * RK4 
 ***********************/

/* tempstorage for RK
 * 
 * I need to store u + alpha * k_i into some temporary variable called kstar
 */
__kernel void rk_add(__global double *k,__global double *c,__global double *rhs, double a) {
    int idx = get_global_id(0);
    int i, n;

    if (idx < num_elem) {

        for (n = 0; n < M; n++) {
            for (i = 0; i < NP; i++) {
                k[num_elem * NP * n + i * num_elem + idx] = c[num_elem * NP * n + i * num_elem + idx] + a * rhs[num_elem * NP * n + i * num_elem + idx];
            }
        }
    }
}


/* evaluate u
 * 
 * evaluates rho and E at the three vertex points for output
 * THREADS: num_elem
 */
__kernel void eval_u(__global double *C, __global double *Uv1,
                     __global double *Uv2, int n) {
    int idx = get_global_id(0);
    int i;
    double uv1, uv2;
        
    if (idx < num_elem) {
        // calculate values at the integration points
        uv1 = 0.;
        uv2 = 0.;

        for (i = 0; i < NP; i++) {
            uv1 += C[num_elem * NP * n + i * num_elem + idx] * basis_endpoints[i * 2 + 0];
            uv2 += C[num_elem * NP * n + i * num_elem + idx] * basis_endpoints[i * 2 + 1];
        }

        // store result
        Uv1[idx] = uv1;
        Uv2[idx] = uv2;
    }
}


/* plot exact
 * 
 * evaluates rho and E at the three vertex points for output
 * THREADS: num_elem
 *//*
 void plot_exact(double *C, 
                           double *V1x, double *V1y,
                           double *V2x, double *V2y,
                           double *V3x, double *V3y,
                           double *Uv1, double *Uv2, double *Uv3, 
                           double t, int n) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) {
        double U[4];

        // calculate values at the integration points
        U_exact(U, V1x[idx], V1y[idx], t);
        Uv1[idx] = U[n];
        U_exact(U, V2x[idx], V2y[idx], t);
        Uv2[idx] = U[n];
        U_exact(U, V3x[idx], V3y[idx], t);
        Uv3[idx] = U[n];
    }
}*/



