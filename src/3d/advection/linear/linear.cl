
#define PI 3.14159265358979323

/***********************
 *
 * INITIAL CONDITIONS
 *
 ***********************/

/* initial condition function
 *
 * returns the value of the intial condition at point x,y
 */

void U0(double *U, double x, double y, double z) {
    /*
    if (x < 0.2 && x > 0. && y < 0.2 && y > 0.) {
        U[0] = 1.;
    } else if(x < 0.5 && x > 0.3 && y < 0.2 && y > 0.)  {
        U[0] = 10*min(min(min(x-0.3,0.5-x),y),0.2-y);
    } else if(x < 0.2 && x > 0. && y < 0.5 && y > 0.3) {
        U[0] = sqrt(max(1-100*((x-0.1)*(x-0.1)+(y-0.4)*(y-0.4)),0.));
    } else if(x < 0.5 && x > 0.3 && y < 0.5 && y > 0.3) {
        U[0] = exp(-500*((x-0.4)*(x-0.4)+(y-0.4)*(y-0.4)));
    }
    else {
        U[0] = 0.;
    }
    */
    double sigma = 15.0;

    U[0] = exp(-sigma*((x-0.5)*(x-0.5) + (y-0.5)*(y-0.5) + (z-0.5)*(z-0.5)));
}

/***********************
*
* INFLOW CONDITIONS
*
************************/
void get_velocity(double *A, double x, double y, double z,double t) {
    A[0] = 1.;
    A[1] = 1.;
    A[2] = 1.;
}

void U_inflow(double *U, double x, double y, double z, double t) {
    U[0] = 0.;
}

/***********************
*
* OUTFLOW CONDITIONS
*
************************/

void U_outflow(double *U, double x, double y, double z, double t) {
    // there are no outflow boundaries in this problem 
}

/***********************
*
* REFLECTING CONDITIONS
*
************************/

void U_reflection(double *U_left, double *U_right, 
                             double x, double y, double z, double t,
                             double nx, double ny,  double nz) {
    // there are no reflecting boundaries in this problem
}

/***********************
*
* EXACT SOLUTION
*
************************/

void U_exact(double *U, double x, double y, double z, double t) {
    double A[3];
    get_velocity(A, x, y, z, t);
    U0(U, x - A[0]*t, y - A[1]*t, z - A[2]*t);
}


