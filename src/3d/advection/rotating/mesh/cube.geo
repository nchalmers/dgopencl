lc =0.03;
Point(1) = {0,0,0,lc};
Point(2) = {1,0,0,lc};
Point(3) = {0,1,0,lc};
Point(4) = {0,0,1,lc};
Point(5) = {1,1,1,lc};
Point(6) = {1,0,1,lc};
Point(7) = {1,1,0,lc};
Point(8) = {0,1,1,lc};

Line(1) = {1, 4};
Line(2) = {4, 8};
Line(3) = {8, 3};
Line(4) = {3, 1};
Line(5) = {3, 7};
Line(6) = {7, 2};
Line(7) = {2, 1};
Line(8) = {8, 5};
Line(9) = {5, 7};
Line(10) = {4, 6};
Line(11) = {6, 5};
Line(12) = {6, 2};

Line Loop(13) = {2, 8, -11, -10};
Plane Surface(14) = {13};
Line Loop(15) = {9, 6, -12, 11};
Plane Surface(16) = {15};
Line Loop(17) = {6, 7, -4, 5};
Plane Surface(18) = {17};
Line Loop(19) = {4, 1, 2, 3};
Plane Surface(20) = {19};
Line Loop(21) = {3, 5, -9, -8};
Plane Surface(22) = {21};
Line Loop(23) = {7, 1, 10, 12};
Plane Surface(24) = {23};

Surface Loop(25) = {14, 20, 18, 16, 22, 24};
Volume(26) = {25};

Physical Surface(30000) = {16, 14, 20, 18, 22, 24};

Physical Volume (100) = {26};
