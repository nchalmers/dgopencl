#define PI 3.14159265358979323

//***********************
// *
// * INITIAL CONDITIONS
// *
//***********************/

// initial condition function
// returns the value of the intial condition at point x,y
//
 

void U0(double *U, const double x, const double y, const double z) {
    double x0, y0, z0 ,r;

    //if (x < .4 && x > .2 && y < .1 && y > -.1) {
        //U[0] = 1;
    //} else {
        //U[0] = 0;
    //}
        
    x0 = 0.2;
    y0 = 0.5;
    z0 = 0.5;
    r  = 0.15;
    U[0] = 5*exp(-(pow(x - x0,2) + pow(y - y0,2) + pow(z - z0,2))/(2*r*r));
                  
    
}

//***********************
//*
//* INFLOW CONDITIONS
//*
//************************/
void get_velocity(double *A, double x, double y, double z, double t) {
    A[0] = -2*PI*y;
    A[1] =  2*PI*x;
    A[1] =  0;
}

void U_inflow(double *U, double x, double y, double z, double t) {

    //U[0] = 0;

    double x0, y0, z0, r;
    x0 = 0.2;
    y0 = 0.5;
    z0 = 0.5;
    r  = 0.15;
    U[0] = 5*exp(-(pow(x*cospi(2*t) + y*sinpi(2*t) - x0,2) + 
                   pow(-x*sinpi(2*t) + y*cospi(2*t) - y0,2) + pow(z - z0,2))/(2*r*r));
}

//***********************
//*
//* OUTFLOW CONDITIONS
//*
//************************/

void U_outflow(double *U, double x, double y, double z, double t) {
    // there are no outflow boundaries in this problem 
}

//***********************
//*
//* REFLECTING CONDITIONS
//*
//************************/

void U_reflection(double *U_left, double *U_right, 
                             double x, double y, double z, double t,
                             double nx, double ny, double nz) {
    // there are no reflecting boundaries in this problem
}

//***********************
//*
//* EXACT SOLUTION
//*
//************************/

void U_exact(double *U, double x, double y, double z, double t) {

    double x0, y0, z0, r;
    x0 = 0.2;
    y0 = 0.5;
    z0 = 0.5;
    r  = 0.15;
    U[0] = 5*exp(-(pow(x*cospi(2*t) + y*sinpi(2*t) - x0,2) + 
                   pow(-x*sinpi(2*t) + y*cospi(2*t) - y0,2) + pow(z - z0,2))/(2*r*r));
    
    //U0(U, x, y);
}
