#include "../../main.c"

/* rotatinghill.c
 *
 * simple flow with exact boundary conditions
 *
 */

#define PI 3.14159265358979323

int limiter = NO_LIMITER;  // no limiter
double CFL = 1.;
int M = 1;


/***********************
 *
 * MAIN FUNCTION
 *
 ***********************/




int main(int argc, char *argv[]) {

    run_dgopencl(argc, argv,"advection.cl","rotating/rotatinghill.cl");
}
