/* conserv_kernels.cl
 *
 * Contains the GPU variables and kernels to solve hyperbolic conservation laws in two-dimensions.
 * Stores all mesh variables and the following kernels
 * 
 * 1. precompuations
 * 2. surface integral 
 * 3. volume integral 
 * 4. limiter
 * 5. evaluate u 
 *
 */


/***********************
 *
 * PRECOMPUTING
 *
 ***********************/

/* jacobian computing
 *
 * precomputes the jacobian determinant for each element.
 * THREADS: num_elem
 */
__kernel void preval_jacobian(__global double *J, 
                              __global double *V1x,__global double *V1y,__global double *V1z,
                              __global double *V2x,__global double *V2y,__global double *V2z,
                              __global double *V3x,__global double *V3y,__global double *V3z,
                              __global double *V4x,__global double *V4y,__global double *V4z) {
    int idx = get_global_id(0);

    if (idx < num_elem) {
        double xr, xs, xt, yr, ys, yt, zr, zs, zt;

        // read vertex points
        xr = V2x[idx]-V1x[idx];
        xs = V3x[idx]-V1x[idx];
        xt = V4x[idx]-V1x[idx];
        yr = V2y[idx]-V1y[idx];
        ys = V3y[idx]-V1y[idx];
        yt = V4y[idx]-V1y[idx];
        zr = V2z[idx]-V1z[idx];
        zs = V3z[idx]-V1z[idx];
        zt = V4z[idx]-V1z[idx];

        // calculate jacobian determinant
        J[idx] = xr * (ys * zt - yt * zs) - xs * (yr * zt - yt * zr) + xt * ( yr * zs -ys * zr);
    }
}

/* side length computer
 *
 * precomputes the length of each side.
 * THREADS: num_sides
 */ 
__kernel void preval_side_area(__global double *s_area, 
                             __global double *s_V1x,__global double *s_V1y,__global double *s_V1z,  
                             __global double *s_V2x,__global double *s_V2y,__global double *s_V2z,
                             __global double *s_V3x,__global double *s_V3y,__global double *s_V3z) {
    int idx = get_global_id(0);

    double u[3], v[3];

    if (idx < num_sides) {

        u[0] = s_V2x[idx]-s_V1x[idx];
        u[1] = s_V2y[idx]-s_V1y[idx];
        u[2] = s_V2z[idx]-s_V1z[idx];

        v[0] = s_V3x[idx]-s_V1x[idx];
        v[1] = s_V3y[idx]-s_V1y[idx];
        v[2] = s_V3z[idx]-s_V1z[idx];

        // compute and store the area of the side
        s_area[idx] = 0.5*sqrt(pow(u[1]*v[2]-u[2]*v[1],2)+pow(u[2]*v[0]-u[0]*v[2],2)+pow(u[0]*v[1]-u[1]*v[0],2));
    }
}

/* minimum height computing
 *
 * computes the smallest height of each cell
 * 
 */

__kernel void preval_min_height(__global double *h,__global double *J,__global double *s_area,
                                __global int *elem_s1,__global int *elem_s2,
                                __global int *elem_s3,__global int *elem_s4) {
    int idx = get_global_id(0);

    double a, b, c, d, k;
    int s1_idx, s2_idx, s3_idx, s4_idx;

    if (idx < num_elem) {
        //The Jacobian is 6 times the volume
        k = J[idx]/6.;

        s1_idx = elem_s1[idx];
        s2_idx = elem_s2[idx];
        s3_idx = elem_s3[idx];
        s4_idx = elem_s4[idx];

        //area of each side
        a = s_area[s1_idx];
        b = s_area[s2_idx];
        c = s_area[s3_idx];
        d = s_area[s4_idx];

        //Smallest height
        h[idx] = min(min(min(3*k/a,3*k/b),3*k/c),3*k/d);
    }
}

/* evaluate normal vectors
 *
 * computes the normal vectors for each element along each side.
 * THREADS: num_sides
 *
 */
__kernel void preval_normals(__global double *Nx,__global double *Ny,__global double *Nz, 
                         __global double *s_V1x,__global double *s_V1y,__global double *s_V1z, 
                         __global double *s_V2x,__global double *s_V2y,__global double *s_V2z,
                         __global double *s_V3x,__global double *s_V3y,__global double *s_V3z) {

    int idx = get_global_id(0);

    if (idx < num_sides) {
        double x, y, z, l;

        x =  (s_V2y[idx] - s_V1y[idx]) * (s_V3z[idx] - s_V1z[idx]) - (s_V2z[idx] - s_V1z[idx]) * (s_V3y[idx] - s_V1y[idx]);
        y = -(s_V2x[idx] - s_V1x[idx]) * (s_V3z[idx] - s_V1z[idx]) + (s_V2z[idx] - s_V1z[idx]) * (s_V3x[idx] - s_V1x[idx]);
        z =  (s_V2x[idx] - s_V1x[idx]) * (s_V3y[idx] - s_V1y[idx]) - (s_V2y[idx] - s_V1y[idx]) * (s_V3x[idx] - s_V1x[idx]);
    
        // normalize
        l = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));

        // store the result
        Nx[idx] =  -x / l;
        Ny[idx] =  -y / l;
        Nz[idx] =  -z / l;
    }
}

__kernel void preval_normals_direction(__global double *Nx,__global double *Ny,__global double *Nz, 
                                    __global double *V1x,__global double *V1y,__global double *V1z,
                                    __global double *V2x,__global double *V2y,__global double *V2z,
                                    __global double *V3x,__global double *V3y,__global double *V3z,
                                    __global double *V4x,__global double *V4y,__global double *V4z,
                                    __global int *left_elem,__global int *left_side_number) {

//REMOVED
}

__kernel void preval_partials(__global double *V1x,__global double *V1y,__global double *V1z,
                              __global double *V2x,__global double *V2y,__global double *V2z,
                              __global double *V3x,__global double *V3y,__global double *V3z,
                              __global double *V4x,__global double *V4y,__global double *V4z,
                              __global double *xr, __global double *yr, __global double *zr,
                              __global double *xs, __global double *ys, __global double *zs,
                              __global double *xt, __global double *yt, __global double *zt) {
    int idx = get_global_id(0);
    if (idx < num_elem) {
        // evaulate the jacobians of the element mappings for the chain rule
        xr[idx] = V2x[idx] - V1x[idx];
        yr[idx] = V2y[idx] - V1y[idx];
        zr[idx] = V2z[idx] - V1z[idx];
        xs[idx] = V3x[idx] - V1x[idx];
        ys[idx] = V3y[idx] - V1y[idx];
        zs[idx] = V3z[idx] - V1z[idx];
        xt[idx] = V4x[idx] - V1x[idx];
        yt[idx] = V4y[idx] - V1y[idx];
        zt[idx] = V4z[idx] - V1z[idx];
    }
}


/* initial conditions
 *
 * computes the coefficients for the initial conditions
 * THREADS: num_elem
 */
double L2_projection(double *V, int i, int n) {
    int j;
    double X,Y,Z;

    double C = 0.;
	double U[M];
	//Numerically integrate U0 times the i'th basis function

    for (j = 0; j < n_quad3d; j++) {
        // get the 2d point on the mesh
        get_coordinates_3d(&X, &Y, &Z, V, j);
		
		U0(U,X,Y,Z);
		
        // evaluate U at the integration point
        C += w3[j] * U[n] * basis[i * n_quad3d + j];
    }
    return C;
}

 
__kernel void initial_conditions(__global double *c,  __global double *J,
                                 __global double *V1x,__global double *V1y,__global double *V1z,
                                 __global double *V2x,__global double *V2y,__global double *V2z,
                                 __global double *V3x,__global double *V3y,__global double *V3z,
                                 __global double *V4x,__global double *V4y,__global double *V4z) {
    int idx = get_global_id(0);
    int i, n;
    double V[12];

	if (idx < num_elem) {
        V[0]  = V1x[idx];
        V[1]  = V1y[idx];
        V[2]  = V1z[idx];
        V[3]  = V2x[idx];
        V[4]  = V2y[idx];
        V[5]  = V2z[idx];
        V[6]  = V3x[idx];
        V[7]  = V3y[idx];
        V[8]  = V3z[idx];
        V[9]  = V4x[idx];
        V[10] = V4y[idx];
        V[11] = V4z[idx];

        for (i = 0; i < NP; i++) {
             for (n = 0; n < M; n++) {
                 c[num_elem * NP * n + i * num_elem + idx] = L2_projection(V, i, n);
             }
        } 
    }
}

//gets the grid coordinates at the j'th integration point for 3d 
 void get_coordinates_3d(double *x, double *y, double *z, double *V, int j) {
    *x = V[0] + (V[3] - V[0]) * r3[j] + (V[6] - V[0]) * s3[j] + (V[9] - V[0]) * t3[j];
    *y = V[1] + (V[4] - V[1]) * r3[j] + (V[7] - V[1]) * s3[j] + (V[10] - V[1]) * t3[j];
    *z = V[2] + (V[5] - V[2]) * r3[j] + (V[8] - V[2]) * s3[j] + (V[11] - V[2]) * t3[j];
}

 //gets the grid cooridinates at the j'th integration point for 2d 
 void get_coordinates_2d(double *x, double *y, double *z, double *V, int j, int left_side) {

    double r3_eval, s3_eval, t3_eval;

    // we need the mapping back to the grid space
    switch (left_side) {
        case 0: //face 0
            r3_eval = r2[j];
            s3_eval = s2[j];
            t3_eval = 0.;
            break;
        case 1: //face 1
            r3_eval = 1.-r2[j]-s2[j];
            s3_eval = 0.;
            t3_eval = s2[j];
            break;
        case 2: //face 2
            r3_eval = 0.;
            s3_eval = 1.-r2[j]-s2[j];
            t3_eval = r2[j];
            break;
        case 3: //face 3
            r3_eval = s2[j];
            s3_eval = r2[j];
            t3_eval = 1.-r2[j]-s2[j];
            break;
    }

    *x = V[0] + (V[3] - V[0]) * r3_eval + (V[6] - V[0]) * s3_eval + (V[9] - V[0]) * t3_eval;
    *y = V[1] + (V[4] - V[1]) * r3_eval + (V[7] - V[1]) * s3_eval + (V[10] - V[1]) * t3_eval;
    *z = V[2] + (V[5] - V[2]) * r3_eval + (V[8] - V[2]) * s3_eval + (V[11] - V[2]) * t3_eval;
}
 

/***********************
 *
 * BOUNDARY CONDITIONS
 *
 ***********************/
/* Put the boundary conditions for the problem in here.
*/
void inflow_boundary(double *U_left, double *U_right,
                                double *V, double nx, double ny, double nz,
                                int j, int left_side, double t) {

    double X, Y, Z;

    // get x, y, z coordinates
    get_coordinates_2d(&X, &Y, &Z, V, j, left_side);

    U_inflow(U_right, X, Y, Z, t);
}

void outflow_boundary(double *U_left, double *U_right,
                                 double *V, double nx, double ny, double nz,
                                 int j, int left_side, double t) {
    double X, Y, Z;
    

    // get x, y, z coordinates
    get_coordinates_2d(&X, &Y, &Z, V, j, left_side);
    
    U_outflow(U_right, X, Y, Z, t);
}

void reflecting_boundary(double *U_left, double *U_right,
                            double *V, double nx, double ny,  double nz,
                            int j, int left_side, double t) {
    double X, Y, Z;

    // get x, y, z coordinates
    get_coordinates_2d(&X, &Y, &Z, V, j, left_side);
    
    U_reflection(U_left, U_right, X, Y, Z, t, nx, ny, nz);
}

/***********************
 *
 * MAIN FUNCTIONS
 *
 ***********************/

/* limiter
 *
 * the standard Barth-Jespersen limiter for p = 1 
 * TODO: The limiter for Euler should use the characteristic variables
 *
 * THREADS: num_elem
 */
__kernel void limit_c(__global double *C,
                      __global int *elem_s1,__global int *elem_s2,
                      __global int *elem_s3,__global int *elem_s4,
                      __global int *left_side_idx,__global int *right_side_idx) {

    int idx = get_global_id(0);
    if (idx < num_elem) { 
        double U_i, U_c, Umin, Umax, alpha, min_alpha;
        int s1, s2, s3, s4;
        int n, i, j, side;
        int neighbor_idx[4];
        // get element neighbors
        s1 = elem_s1[idx];
        s2 = elem_s2[idx];
        s3 = elem_s3[idx];
        s4 = elem_s4[idx];

        // get element neighbor indexes
        neighbor_idx[0] = (left_side_idx[s1] == idx) ? right_side_idx[s1] : left_side_idx[s1];
        neighbor_idx[1] = (left_side_idx[s2] == idx) ? right_side_idx[s2] : left_side_idx[s2];
        neighbor_idx[2] = (left_side_idx[s3] == idx) ? right_side_idx[s3] : left_side_idx[s3];
        neighbor_idx[3] = (left_side_idx[s4] == idx) ? right_side_idx[s4] : left_side_idx[s4];
        
        // make sure we aren't on a boundary element
        for (i = 0; i < 4; i++) {
            neighbor_idx[i] = (neighbor_idx[i] < 0) ? idx : neighbor_idx[i];
        }

        for (n = 0; n < M; n++) {
            // set initial stuff
            U_c = C[num_elem*NP*n + idx] * basis[0];
            Umin = U_c;
            Umax = U_c;

            // get delmin and delmax
            for (i = 0; i < 4; i++) {
                U_i = C[num_elem*NP*n + neighbor_idx[i]] * basis[0];
                
                Umin = (U_i < Umin) ? U_i : Umin;
                Umax = (U_i > Umax) ? U_i : Umax;
            }

            min_alpha = 1.;

			// Gauss points
            for (j = 0; j < n_quad2d; j++){
                for (side = 0; side < 4; side++) {
            	
                    // evaluate U
                    U_i = 0.;
                    for (i = 0; i < NP; i++) {
                        U_i += C[num_elem*NP*n + i*num_elem + idx] 
                             * basis_side[side * n_quad2d *NP + i * n_quad2d + j];
                    }
    				                
                    // pick the correct min_phi
                    if (U_i > U_c) {
                        alpha = min(1., (U_c-Umin)/(U_i - U_c));
                    } else if (U_i < U_c) {
                        alpha = min(1., (U_c-Umax)/(U_i - U_c));
                    } else {
                    	alpha = 1.;
                    }
      
                    // find min_phi
                    min_alpha = (alpha < min_alpha) ? alpha : min_alpha; 
                }
            }

            if (min_alpha < 0) {
                min_alpha = 0.;
            }

            // limit the slope
            C[NP * num_elem * n + 1 * num_elem + idx] *= min_alpha;
            C[NP * num_elem * n + 2 * num_elem + idx] *= min_alpha;
            C[NP * num_elem * n + 3 * num_elem + idx] *= min_alpha;
        }
    }
}


void eval_boundary(double *U_left, double *U_right, 
                              double *V, double nx, double ny, double nz,
                              int j, int left_side, double t, int right_idx) {
    switch (right_idx) {
        // reflecting 
        case -1: 
            reflecting_boundary(U_left, U_right,
                V, nx, ny, nz,
                j, left_side, t);
            break;
        // outflow 
        case -2: 
            outflow_boundary(U_left, U_right,
                V, nx, ny, nz,
                j, left_side, t);
            break;
        // inflow 
        case -3: 
            inflow_boundary(U_left, U_right,
                V, nx, ny, nz,
                j, left_side, t);
            break;
    }
}

/* left & right evaluator
 * 
 * calculate U_left and U_right at the integration point,
 * using boundary conditions when necessary.
 */
 void eval_left_right(double *C_left, double *C_right, 
                             double *U_left, double *U_right,
                             double nx, double ny, double nz,
                             double *V, int j, 
                             int left_side, int right_side,
                             int left_idx, int right_idx,
                             int r_shift, double t) { 

    int i, n;

    // set U to 0
    for (n = 0; n < M; n++) {
        U_left[n]  = 0.;
        U_right[n] = 0.;
    }

    //evaluate U at the integration points
    for (i = 0; i < NP; i++) {
        for (n = 0; n < M; n++) {
            U_left[n] += C_left[n*NP + i] * 
                         basis_side[left_side * NP * n_quad2d + i * n_quad2d + j];
        }
    }

    // boundaries are sorted to avoid warp divergence
    if (right_idx < 0) {
        eval_boundary(U_left, U_right, V, nx, ny, nz, j, left_side, t, right_idx);
    } else {
        // evaluate the right side at the integration point
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                U_right[n] += C_right[n*NP + i] * 
                              basis_side[right_side * NP * n_quad2d + i * n_quad2d + r_shift];
            }
        }
    }
}

/* surface integral evaluation
 *
 * evaluate all the riemann problems for each element.
 * THREADS: num_sides
 */
__kernel void eval_surface(__global double *C, 
                  __global double *rhs_surface_left,__global double *rhs_surface_right, 
                  __global double *side_area,
                  __global double *V1x,__global double *V1y,__global double *V1z,
                  __global double *V2x,__global double *V2y,__global double *V2z,
                  __global double *V3x,__global double *V3y,__global double *V3z,
                  __global double *V4x,__global double *V4y,__global double *V4z,
                  __global int *left_elem,__global int *right_elem,
                  __global int *left_side_number,__global int *right_side_number,
                  __global int *rotation, __global double *Nx,
                  __global double *Ny,__global double *Nz, double t) {
    int idx = get_global_id(0);

    if (idx < num_sides) {
        int i, j, n;
        int left_idx, right_idx, left_side, right_side, rot, r_shift;
        double A, nx, ny,nz;
        double C_left[M * NP], C_right[M * NP];
        double U_left[M], U_right[M];
        double F_n[M];
        double V[12];

		double surface_left[M*NP], surface_right[M*NP];

        
        
        /*
        double F_left[M], F_right[M];
        double alpha[10];

        alpha[0]=1.;
        
        alpha[1]=0.33;
        alpha[2]=0.33;

        alpha[3]=1.;
        alpha[4]=1.;
        alpha[5]=1.;

        alpha[6]=1.;
        alpha[7]=1.;
        alpha[8]=1.;
        alpha[9]=1.;
        */

		/*
		TODO: Idea for reducing the overflowing registers.
		- Compute UL and UR and F_n at one integration point and add it to the total.
		- Maybe put those computations in separate function? It's hard to know whether that will free the register memory. 
		- Loop over each integration point
		*/

        // read edge data
        A = side_area[idx];
        nx  = Nx[idx];
        ny  = Ny[idx];
        nz  = Nz[idx];
        left_idx   = left_elem[idx];
        right_idx  = right_elem[idx];
        left_side  = left_side_number[idx];
        right_side = right_side_number[idx];
        rot = rotation[idx];

        // get verticies
        V[0]  = V1x[left_idx];
        V[1]  = V1y[left_idx];
        V[2]  = V1z[left_idx];
        V[3]  = V2x[left_idx];
        V[4]  = V2y[left_idx];
        V[5]  = V2z[left_idx];
        V[6]  = V3x[left_idx];
        V[7]  = V3y[left_idx];
        V[8]  = V3z[left_idx];
        V[9]  = V4x[left_idx];
        V[10] = V4y[left_idx];
        V[11] = V4z[left_idx];

        // read coefficients
        if (right_idx > -1) {
            for (n = 0; n < M; n++) {
                for (i = 0; i < NP; i++) {
                    C_left[n*NP + i]  = C[num_elem * NP * n + i * num_elem + left_idx];
                    C_right[n*NP + i] = C[num_elem * NP * n + i * num_elem + right_idx];
                }
            }
        } else { //Element on boundary
            for (n = 0; n < M; n++) {
                for (i = 0; i < NP; i++) {
                    C_left[n*NP + i]  = C[num_elem * NP * n + i * num_elem + left_idx];
                }
            }
        }

		// set RHS to 0
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                surface_left[NP * n + i]  = 0.;
                surface_right[NP * n + i] = 0.;
            }
        }

        // at each integration point
        for (j = 0; j < n_quad2d; j++) {

            r_shift = rotation_shift[rot*n_quad2d + j];

            // calculate the left and right values along the surface
            eval_left_right(C_left, C_right,
                            U_left, U_right,
                            nx, ny,nz,
                            V, j, left_side, right_side,
                            left_idx, right_idx, r_shift, t);

            // compute F_n(U_left, U_right)
            riemann_solver(F_n, U_left, U_right, V, t, nx,ny,nz, j, left_side);

            /*
            // compute F_left and F_right)
            riemann_solver(F_left, U_left, U_left, V, t, nx, ny, j, left_side);
            // compute F_n(U_left, U_right)
            riemann_solver(F_right, U_right, U_right, V, t, nx, ny, j, left_side);
            */

            // multiply across by phi_i at this integration point
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    //surface_left[NP * n + i]  += -len / 2 * (w_oned[j] * (alpha[i]*F_n[n]+(1.-alpha[i])*F_left[n]) * basis_side[left_side  * NP * n_quad2d + i * n_quad2d + j]);
                    //surface_right[NP * n + i] +=  len / 2 * (w_oned[j] * (alpha[i]*F_n[n]+(1.-alpha[i])*F_right[n]) * basis_side[right_side * NP * n_quad2d + i * n_quad2d + n_quad2d - 1 - j]);
                    surface_left[NP * n + i]  += -2 * A * (w2[j]       * F_n[n] * basis_side[left_side  * NP * n_quad2d + i * n_quad2d + j]);
                    surface_right[NP * n + i] +=  2 * A * (w2[r_shift] * F_n[n] * basis_side[right_side * NP * n_quad2d + i * n_quad2d + r_shift]);
                }
            } 
        } 
        
        
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                rhs_surface_left[num_sides * NP * n + i * num_sides + idx]  = surface_left[NP*n+i];
                rhs_surface_right[num_sides * NP * n + i * num_sides + idx] = surface_right[NP*n+i];
            }
        }
    } 
}

/* volume integrals
 *
 * evaluates and adds the volume integral to the rhs vector
 * THREADS: num_elem
 */
__kernel void eval_volume(__global double *C,__global double *rhs_volume, 
                          __global double *Xr,__global double *Yr,__global double *Zr,
                          __global double *Xs,__global double *Ys,__global double *Zs,
                          __global double *Xt,__global double *Yt,__global double *Zt,
                          __global double *V1x,__global double *V1y,__global double *V1z,
                          __global double *V2x,__global double *V2y,__global double *V2z,
                          __global double *V3x,__global double *V3y,__global double *V3z,
                          __global double *V4x,__global double *V4y,__global double *V4z,
                          __global double *J, double t) {
    int idx = get_global_id(0);

    if (idx < num_elem) {
        double V[12];
        //double S[M];
        double C_left[M * NP];
        int i, j, k, n;
        double U[M];
        double flux_x[M], flux_y[M],flux_z[M];
        double xr, yr, zr, xs, ys, zs, xt, yt, zt;
        //double detJ;

		double volume[M * NP];

		/*
		TODO: Idea for reducing the overflowing registers.
		- Compute F_n at each integration point and store. (This is NxN_quad, not NxNP) (Marginal difference)
		- Maybe put those computations in separate function? It's hard to know whether that will free the register memory. 

		- Loop over each integration point
		
		*/

        // read coefficients
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                C_left[n*NP + i]  = C[num_elem * NP * n + i * num_elem + idx];
            }
        }

        // get element data
        xr = Xr[idx];
        yr = Yr[idx];
        zr = Zr[idx];
        xs = Xs[idx];
        ys = Ys[idx];
        zs = Zs[idx];
        xt = Xt[idx];
        yt = Yt[idx];
        zt = Zt[idx];

        // get jacobian determinant
        //detJ = J[idx];
        
        // get verticies
        V[0]  = V1x[idx];
        V[1]  = V1y[idx];
        V[2]  = V1z[idx];
        V[3]  = V2x[idx];
        V[4]  = V2y[idx];
        V[5]  = V2z[idx];
        V[6]  = V3x[idx];
        V[7]  = V3y[idx];
        V[8]  = V3z[idx];
        V[9]  = V4x[idx];
        V[10] = V4y[idx];
        V[11] = V4z[idx];

        // set to 0
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                volume[NP * n + i] = 0.;
            }
        }

        // for each integration point
        for (j = 0; j < n_quad3d; j++) {

            // initialize to zero
            for (n = 0; n < M; n++) {
                U[n] = 0.;
            }

            // calculate at the integration point
            for (k = 0; k < NP; k++) {
                for (n = 0; n < M; n++) {
                    U[n] += C_left[n*NP + k] * basis[n_quad3d * k + j];
                }
            }

            // evaluate the flux
            eval_flux(U, flux_x, flux_y, flux_z, V, t, j, -1);

            // get the source term
            //source_term(S, U, V, t, j);

            // multiply across by phi_i
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    // add the sum
                    //     [fx fy] * [y_s, -y_r; -x_s, x_r] * [phi_x phi_y]
                    volume[NP * n + i] += 
                              flux_x[n] * (  basis_grad_x[n_quad3d * i + j] * ( ys * zt - yt * zs)
                                           + basis_grad_y[n_quad3d * i + j] * (-yr * zt + yt * zr)
                                           + basis_grad_z[n_quad3d * i + j] * ( yr * zs - ys * zr) )
                            + flux_y[n] * (  basis_grad_x[n_quad3d * i + j] * (-xs * zt + xt * zs)
                                           + basis_grad_y[n_quad3d * i + j] * ( xr * zt - xt * zr)
                                           + basis_grad_z[n_quad3d * i + j] * (-xr * zs + xs * zr) )
                            + flux_z[n] * (  basis_grad_x[n_quad3d * i + j] * ( xs * yt - ys * xt)
                                           + basis_grad_y[n_quad3d * i + j] * (-xr * yt + xt * yr)
                                           + basis_grad_z[n_quad3d * i + j] * ( xr * ys - xs * yr) );

                    // add the source term S * phi_i
                    //volume[NP * n + i] += 
                    //        S[n] * basis[n_quad3d * i + j] * w[j] * detJ;
                }
            }
        }
        
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                rhs_volume[num_elem * NP * n + i * num_elem + idx] = volume[NP * n + i];
            }
        }       
    }
}

/***********************
 * ASSEMBLE RHS FUNCTIONS
 ***********************/

/* right hand side
 *
 * computes the sum of the quadrature and the riemann flux for the 
 * coefficients for each element
 * THREADS: num_elem
 */


__kernel void eval_rhs(__global double *k,__global double *rhs_volume,
					   __global double *rhs_surface_left,__global double *rhs_surface_right, 
                       __global int *elem_s1,__global int *elem_s2,
                       __global int *elem_s3,__global int *elem_s4,
                       __global int *left_elem,__global double *J, double dt) {
    int idx = get_global_id(0);
    double j;
    int i, s1_idx, s2_idx, s3_idx, s4_idx;
    int n;
    
    /*
	TODO: Idea for reducing the memory usage
	
	- Why do we even use rhs_volume? Isn't this redundant? Why not just use k?
	
	*/
    
    
    double K[M*NP];

    if (idx < num_elem) {

        // set to 0
        for (i = 0; i < NP; i++) { 
            for (n = 0; n < M; n++) {
                K[NP * n + i] = 0.;
            }
        }

        // read jacobian determinant
        j = J[idx];

        // get the indicies for the riemann contributions for this element
        s1_idx = elem_s1[idx];
        s2_idx = elem_s2[idx];
        s3_idx = elem_s3[idx];
        s4_idx = elem_s4[idx];
        

        // add volume integral
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                K[NP * n + i] += dt/j*rhs_volume[num_elem * NP * n + i * num_elem + idx];
            }
        }

        // for the first edge, add either left or right surface integral
        if (idx == left_elem[s1_idx]) {
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    K[NP * n + i] += dt/j*rhs_surface_left[num_sides*NP*n+i*num_sides + s1_idx];
                }
            }
        } else {
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    K[NP * n + i] += dt/j*rhs_surface_right[num_sides*NP*n+i*num_sides + s1_idx];
                }
            }
        }
        // for the second edge, add either left or right surface integral
        if (idx == left_elem[s2_idx]) {
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    K[NP * n + i] += dt/j*rhs_surface_left[num_sides*NP*n+i*num_sides + s2_idx];
                }
            }
        } else {
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    K[NP * n + i] += dt/j*rhs_surface_right[num_sides*NP*n+i*num_sides + s2_idx];
                }
            }
        }
       // for the third edge, add either left or right surface integral
        if (idx == left_elem[s3_idx]) {
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    K[NP * n + i] += dt/j*rhs_surface_left[num_sides*NP*n+i * num_sides + s3_idx];
                }
            }
        } else {
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    K[NP * n + i] += dt/j*rhs_surface_right[num_sides*NP*n+i * num_sides + s3_idx];
                }
            }
        }
        // for the forth edge, add either left or right surface integral
        if (idx == left_elem[s4_idx]) {
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    K[NP * n + i] += dt/j*rhs_surface_left[num_sides*NP*n+i * num_sides + s4_idx];
                }
            }
        } else {
            for (i = 0; i < NP; i++) {
                for (n = 0; n < M; n++) {
                    K[NP * n + i] += dt/j*rhs_surface_right[num_sides*NP*n+i * num_sides + s4_idx];
                }
            }
        }
     
        //Write to K
        for (i = 0; i < NP; i++) { 
            for (n = 0; n < M; n++) {
                k[num_elem * NP * n + i * num_elem + idx] = K[NP * n + i];
            }
        }
    }
}



/***********************
 * RK
 ***********************/

/* tempstorage for RK
 * 
 * I need to store u + alpha * k_i into some temporary variable called kstar
 */
__kernel void rk_add(__global double *kstar,__global double *c,__global double *k, double a) {
    int idx = get_global_id(0);
    int i, n;

    if (idx < num_elem) {
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                kstar[num_elem * NP * n + i * num_elem + idx] = c[num_elem * NP * n + i * num_elem + idx] + a * k[num_elem * NP * n + i * num_elem + idx];
            }
        }
    }
}


/* evaluate u
 * 
 * evaluates rho and E at the three vertex points for output
 * THREADS: num_elem
 */
__kernel void eval_u(__global double *C, __global double *Uv1,
                     __global double *Uv2, __global double *Uv3,__global double *Uv4, int n) {
    int idx = get_global_id(0);
    int i;
    double uv1, uv2, uv3, uv4;
        
    if (idx < num_elem) {
        // calculate values at the integration points
        uv1 = 0.;
        uv2 = 0.;
        uv3 = 0.;
        uv4 = 0.;
        for (i = 0; i < NP; i++) {
            uv1 += C[num_elem * NP * n + i * num_elem + idx] * basis_vertex[i * 4 + 0];
            uv2 += C[num_elem * NP * n + i * num_elem + idx] * basis_vertex[i * 4 + 1];
            uv3 += C[num_elem * NP * n + i * num_elem + idx] * basis_vertex[i * 4 + 2];
            uv4 += C[num_elem * NP * n + i * num_elem + idx] * basis_vertex[i * 4 + 3];
        }

        // store result
        Uv1[idx] = uv1;
        Uv2[idx] = uv2;
        Uv3[idx] = uv3;
        Uv4[idx] = uv4;
    }
}