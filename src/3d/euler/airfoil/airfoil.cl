#define PI 3.14159
#define GAMMA 1.4
#define MACH .98
#define ALPHA 5.

/***********************
 *
 * INITIAL CONDITIONS
 *
 ***********************/

/* initial condition function
 *
 * returns the value of the intial condition at point x,y
 */
void U0(double *U, double x, double y, double z) {
    U[0] = GAMMA;
    U[1] = U[0] * MACH * cospi(ALPHA / 180.);
    U[2] = U[0] * MACH * sinpi(ALPHA / 180.);
    U[3] = 0.;
    U[4] = 0.5 * U[0] * MACH * MACH + 1./ (GAMMA - 1.0);
}

/***********************
*
* INFLOW CONDITIONS
*
************************/

void U_inflow(double *U, double x, double y, double z, double t) {
    U0(U, x, y, z);
}

/***********************
*
* OUTFLOW CONDITIONS
*
************************/

void U_outflow(double *U, double x, double y, double z, double t) {
    U0(U, x, y, z);
}

/***********************
*
* REFLECTING CONDITIONS
*
************************/
void U_reflection(double *U_left, double *U_right,
                             double x, double y, double z, double t,
                             double nx, double ny, double nz) {

    //double dot;

    // set rho and E to be the same in the ghost cell
    //U_right[0] = U_left[0];
    //U_right[3] = U_left[3];

    
    // set rho and E to be the same in the ghost cell
    U_right[0] = U_left[0];
    U_right[4] = U_left[4];
    //double Nx, Ny, dot;
    double ddot;

    // normal reflection
    ddot = U_left[1] * nx + U_left[2] * ny + U_left[3] * nz;

    U_right[1] = U_left[1] - 2*ddot*nx;
    U_right[2] = U_left[2] - 2*ddot*ny;
    U_right[3] = U_left[3] - 2*ddot*nz;

}


/***********************
 *
 * EXACT SOLUTION
 *
 ***********************/
void U_exact(double *U, double x, double y, double z, double t) {
    // no exact solution
}

double get_GAMMA() {
    return GAMMA;
}

