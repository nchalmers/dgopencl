#include "../../main.c"

/* airfoil.c
 *
 * Flow around an airfoil.
 */

#define PI 3.14159
#define GAMMA 1.4
#define MACH .98
#define ALPHA 5.

int limiter = LIMITER;  
double CFL =1.;
int M = 5;

/***********************
 *
 * MAIN FUNCTION
 *
 ***********************/



int main(int argc, char *argv[]) {
    run_dgopencl(argc, argv,"euler.cl","airfoil/airfoil.cl");
}
