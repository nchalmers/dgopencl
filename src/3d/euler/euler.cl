/* euler.cl
 *
 * This file contains the relevant information for making a system to solve
 *
 * d_t [   rho   ] + d_x [     rho * u    ] + d_y [    rho * v     ] + d_z [    rho * w     ] = 0
 * d_t [ rho * u ] + d_x [ rho * u^2 + p  ] + d_y [  rho * v * u   ] + d_z [  rho * w * u   ] = 0
 * d_t [ rho * v ] + d_x [  rho * u * v   ] + d_y [  rho * v^2 + p ] + d_z [  rho * w * v   ] = 0
 * d_t [ rho * w ] + d_x [  rho * u * w   ] + d_y [  rho * v * w   ] + d_z [  rho * w^2 + p ] = 0
 * d_t [    E    ] + d_x [ u * ( E +  p ) ] + d_y [ v * ( E +  p ) ] + d_z [ w * ( E +  p ) ] = 0
 *
 */

void get_coordinates_3d(double *x, double *y, double *z, double *V, int j);

void get_coordinates_2d(double *x, double *y, double *z, double *V, int j, int left_side);

/***********************
 *
 * EULER DEVICE FUNCTIONS
 *
 ***********************/

/* evaluate pressure
 *
 * evaluates the pressure for U
 */
double pressure(double *U) {

    double rho, rhou, rhov, rhow, E;
    rho  = U[0];
    rhou = U[1];
    rhov = U[2];
    rhow = U[3];
    E    = U[4];
    
    return (get_GAMMA() - 1.) * (E - (rhou*rhou + rhov*rhov + rhow*rhow) / 2. / rho);
}

/* is physical
 *
 * returns true if the density and pressure at U make physical sense
 */
bool is_physical(double *U) {
    if (U[0] < 0) {
        return false;
    }

    if (pressure(U) < 0) {
        return false;
    }

    return true;
}

/* check physical
 *
 * if U isn't physical, replace the solution with the constant average value
 */
__kernel void check_physical(__global double *C_global) {
    int i,j,n,k,side;
    int idx = get_global_id(0);
    double C[M*NP];
    double U[M];

    if (idx < num_elem) {

        //Read in global coefficients
        for (i = 0; i < NP; i++) {
            for (n = 0; n < M; n++) {
                C[n*NP + i]  = C_global[num_elem * NP * n + i * num_elem + idx];
            }
        }

        for (j = 0; j < n_quad3d; j++) {

            // initialize to zero
            for (n = 0; n < M; n++) {
                U[n] = 0.;
            }

            // calculate at the integration point
            for (k = 0; k < NP; k++) {
                for (n = 0; n < M; n++) {
                    U[n] += C[n*NP + k] * basis[n_quad3d * k + j];
                }
            }

            // check to see if U is physical
            if (!is_physical(U)) {
                // set C[1] to C[NP] to zero
                for (i = 1; i < NP; i++) {
                    for (n = 0; n < M; n++) {
                        C_global[num_elem * NP * n + i * num_elem + idx] = 0.;

                        C[NP * n + i] = 0.;
                    }
                }

                // rebuild the solution as simply the average value
                for (n = 0; n < M; n++) {
                    U[n] = C[NP * n + 0] * basis[0];
                }
                return;
            }
        }

        for (side =0;side<4;side++) {
            for (j = 0; j < n_quad2d; j++) {
                // set U to 0
                for (n = 0; n < M; n++) {
                    U[n]  = 0.;
                }

                //evaluate U at the integration points
                for (i = 0; i < NP; i++) {
                    for (n = 0; n < M; n++) {
                        U[n] += C[n*NP + i] * basis_side[side * NP * n_quad2d + i * n_quad2d + j];
                    }
                }

                // check to see if U is physical
                if (!is_physical(U)) {
                    // set C[1] to C[NP] to zero
                    for (i = 1; i < NP; i++) {
                        for (n = 0; n < M; n++) {
                            C_global[num_elem * NP * n + i * num_elem + idx] = 0.;

                            C[NP * n + i] = 0.;
                        }
                    }

                    // rebuild the solution as simply the average value
                    for (n = 0; n < M; n++) {
                        U[n] = C[NP * n + 0] * basis[0];
                    }
                    return;
                }
            }
        }
    }
}

/* evaluate c
 *
 * evaulates the speed of sound c
 */
double eval_c(double *U) {
    double p = pressure(U);
    double rho = U[0];

    return sqrt(get_GAMMA() * p / rho);
}    

/***********************
 *
 * EULER FLUX
 *
 ***********************/
/* takes the actual values of rho, u, v, and E and returns the flux 
 * x and y components. 
 * NOTE: this needs the ACTUAL values for u and v, NOT rho * u, rho * v.
 */
void eval_flux(double *U, double *flux_x, double *flux_y, double *flux_z,
                          double *V, double t, int j, int left_side) {

    // evaluate pressure
    double rho, rhou, rhov, rhow, E;
    double p = pressure(U);
    rho  = U[0];
    rhou = U[1];
    rhov = U[2];
    rhow = U[3];
    E    = U[4];

    // flux_x 
    flux_x[0] = rhou;
    flux_x[1] = rhou * rhou / rho + p;
    flux_x[2] = rhou * rhov / rho;
    flux_x[3] = rhou * rhow / rho;
    flux_x[4] = rhou * (E + p) / rho;

    // flux_y
    flux_y[0] = rhov;
    flux_y[1] = rhou * rhov / rho;
    flux_y[2] = rhov * rhov / rho + p;
    flux_y[3] = rhow * rhov / rho;
    flux_y[4] = rhov * (E + p) / rho;

    // flux_z
    flux_z[0] = rhow;
    flux_z[1] = rhou * rhow / rho;
    flux_z[2] = rhov * rhow / rho;
    flux_z[3] = rhow * rhow / rho + p;
    flux_z[4] = rhow * (E + p) / rho;
}

/***********************
 *
 * RIEMAN SOLVER
 *
 ***********************/
/* finds the max absolute value of the jacobian for F(u).
 *  |u - c|, |u|, |u + c|
 */
double eval_lambda(double *U_left, double *U_right,
                              double *V, double t,
                              double nx, double ny, double nz,
                              int j, int left_side) {
                              
    double s_left, s_right;
    double c_left, c_right;
    double u_left, v_left, w_left;
    double u_right, v_right, w_right;
    double left_max, right_max;

    // get c for both sides
    c_left  = eval_c(U_left);
    c_right = eval_c(U_right);

    // find the speeds on each side
    u_left  = U_left[1] / U_left[0];
    v_left  = U_left[2] / U_left[0];
    w_left  = U_left[3] / U_left[0];
    u_right = U_right[1] / U_right[0];
    v_right = U_right[2] / U_right[0];
    w_right = U_right[3] / U_right[0];
    s_left  = nx * u_left  + ny * v_left + nz * w_left;
    s_right = nx * u_right + ny * v_right + nz * w_right; 
    
    // if speed is positive, want s + c, else s - c
    if (s_left > 0.) {
        left_max = s_left + c_left;
    } else {
        left_max = -s_left + c_left;
    }

    // if speed is positive, want s + c, else s - c
    if (s_right > 0.) {
        right_max = s_right + c_right;
    } else {
        right_max = -s_right + c_right;
    }

    // return the max absolute value of | s +- c |
    if (fabs(left_max) > fabs(right_max)) {
        return fabs(left_max);
    } else { 
        return fabs(right_max);
    }
}

/* local lax-friedrichs riemann solver
 */
void riemann_solver(double *F_n, double *U_left, double *U_right,
                               double *V, double t,
                               double nx, double ny, double nz,
                               int j, int left_side) {
    int n;

    double flux_x_l[5], flux_x_r[5];
    double flux_y_l[5], flux_y_r[5];
    double flux_z_l[5], flux_z_r[5];

    // calculate the left and right fluxes
    eval_flux(U_left, flux_x_l, flux_y_l, flux_z_l, V, t, j, left_side);
    eval_flux(U_right, flux_x_r, flux_y_r, flux_z_r, V, t, j, left_side);

    double lambda = eval_lambda(U_left, U_right, V, t, nx, ny, nz, j, left_side);

    // calculate the riemann problem at this integration point
    for (n = 0; n < M; n++) {
        F_n[n] = 0.5 * ((flux_x_l[n] + flux_x_r[n]) * nx + (flux_y_l[n] + flux_y_r[n]) * ny 
                    + (flux_z_l[n] + flux_z_r[n]) * ny + lambda * (U_left[n] - U_right[n]));
    }
}

/***********************
 *
 * CFL CONDITION
 *
 ***********************/
/* global lambda evaluation
 *
 * computes the max eigenvalue of |u + c|, |u|, |u - c|.
 */
__kernel void eval_global_lambda_h(__global double *C,  __global double *lambda,__global double *h, 
                                __global double *V1x,__global double *V1y,__global double *V1z,
                                __global double *V2x,__global double *V2y,__global double *V2z,
                                __global double *V3x,__global double *V3y,__global double *V3z,
                                __global double *V4x,__global double *V4y,__global double *V4z,
                                        double t) {
    int idx = get_global_id(0);
    if (idx < num_elem) { 
        double c, s;

        double U[5];
        // get cell averages
        U[0] = C[num_elem * NP * 0 + idx] * basis[0];
        U[1] = C[num_elem * NP * 1 + idx] * basis[0];
        U[2] = C[num_elem * NP * 2 + idx] * basis[0];
        U[3] = C[num_elem * NP * 3 + idx] * basis[0];
        U[4] = C[num_elem * NP * 4 + idx] * basis[0];

        // evaluate c
        c = eval_c(U);

        // speed of the wave
        s = sqrt(U[1]*U[1] + U[2]*U[2] + U[3]*U[3])/U[0];

        // return the h value divided by the max eigenvalue
        if (s > 0) {
            lambda[idx] = h[idx]/(s + c);
        } else {
            lambda[idx] = h[idx]/(-s + c);
        }
    }
}



/*
 * source term
 */
void source_term(double *S, double *U, double *V, double t, int j) {
    int n;
    for (n = 0; n < M; n++) {
        S[n] = 0.;
    }
}

/* evaluate pressure
 * 
 * evaluates pressure at the three vertex points for output
 * THREADS: num_elem
 */
__kernel void eval_pressure(__global double *C,__global double *Uv1,
						   __global double *Uv2,__global double *Uv3,__global double *Uv4) {
    int idx = get_global_id(0);
    if (idx < num_elem) {
        int i, n;
        double U1[5], U2[5], U3[5], U4[5];

        // calculate values at the integration points
        for (n = 0; n < M; n++) {
            U1[n] = 0.;
            U2[n] = 0.;
            U3[n] = 0.;
            U4[n] = 0.;
            for (i = 0; i < NP; i++) {
                U1[n] += C[num_elem * NP * n + i * num_elem + idx] * basis_vertex[i * 4 + 0];
                U2[n] += C[num_elem * NP * n + i * num_elem + idx] * basis_vertex[i * 4 + 1];
                U3[n] += C[num_elem * NP * n + i * num_elem + idx] * basis_vertex[i * 4 + 2];
                U4[n] += C[num_elem * NP * n + i * num_elem + idx] * basis_vertex[i * 4 + 3];
            }
        }

        // store result
        Uv1[idx] = pressure(U1);
        Uv2[idx] = pressure(U2);
        Uv3[idx] = pressure(U3);
        Uv4[idx] = pressure(U4);
    }
}


