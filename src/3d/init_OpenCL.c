
//OpenCL variables
size_t n_threads, global_n_threads_elem, global_n_threads_sides;
cl_context context;
cl_command_queue command_queue;
cl_program program;

cl_kernel kernel_preval_jacobian, kernel_preval_side_area, kernel_preval_min_height; 
cl_kernel kernel_preval_normals, kernel_preval_normals_direction, kernel_preval_partials;
cl_kernel kernel_initial_conditions, kernel_eval_u, kernel_limit_c, kernel_eval_global_lambda_h;
cl_kernel kernel_eval_surface, kernel_eval_volume, kernel_eval_rhs, kernel_rk_add, kernel_check_physical;

cl_uint status;

#define CL_FUNCTION(NAME, ARGLIST) status = NAME ARGLIST; CL_CHECK_ERROR(status, #NAME); 

#define CL_OBJECT(NAME, ARGLIST) NAME ARGLIST; CL_CHECK_ERROR(status, #NAME); 

#define CL_CHECK_ERROR(STATUS, NAME) \
  if ( (STATUS) != CL_SUCCESS) { \
    printf("ERROR: %s: %s\n", NAME, CLErrString(STATUS) ); \
    exit(-1); } 

static const char * CLErrString(cl_int status) {
   static struct { cl_int code; const char *msg; } error_table[] = {
        { CL_SUCCESS, "success" ,},
        { CL_DEVICE_NOT_FOUND, "device not found",},
        { CL_DEVICE_NOT_AVAILABLE, "device not available",},
        { CL_MEM_OBJECT_ALLOCATION_FAILURE, "mem object allocation failure",},
        { CL_OUT_OF_RESOURCES, "out of resources",},
        { CL_OUT_OF_HOST_MEMORY, "out of host memory",},
        { CL_PROFILING_INFO_NOT_AVAILABLE, "profiling info not available",},
        { CL_MEM_COPY_OVERLAP, "mem copy overlap",},
        { CL_IMAGE_FORMAT_MISMATCH, "image format mismatch",},
        { CL_IMAGE_FORMAT_NOT_SUPPORTED, "image format not supported",},
        { CL_BUILD_PROGRAM_FAILURE, "build program failure",},
        { CL_MAP_FAILURE, "map failure",},
        { CL_INVALID_VALUE, "invalid value",},
        { CL_INVALID_DEVICE_TYPE, "invalid device type",},
        { CL_INVALID_PLATFORM, "invalid platform",},
        { CL_INVALID_DEVICE, "invalid device",},
        { CL_INVALID_CONTEXT, "invalid context",},
        { CL_INVALID_QUEUE_PROPERTIES, "invalid queue properties",},
        { CL_INVALID_COMMAND_QUEUE, "invalid command queue",},
        { CL_INVALID_HOST_PTR, "invalid host ptr",},
        { CL_INVALID_MEM_OBJECT, "invalid mem object",},
        { CL_INVALID_IMAGE_FORMAT_DESCRIPTOR, "invalid image format descriptor",},
        { CL_INVALID_IMAGE_SIZE, "invalid image size",},
        { CL_INVALID_SAMPLER, "invalid sampler",},
        { CL_INVALID_BINARY, "invalid binary",},
        { CL_INVALID_BUILD_OPTIONS, "invalid build options",},
        { CL_INVALID_PROGRAM, "invalid program",},
        { CL_INVALID_PROGRAM_EXECUTABLE, "invalid program executable",},
        { CL_INVALID_KERNEL_NAME, "invalid kernel name",},
        { CL_INVALID_KERNEL_DEFINITION, "invalid kernel definition",},
        { CL_INVALID_KERNEL, "invalid kernel",},
        { CL_INVALID_ARG_INDEX, "invalid arg index",},
        { CL_INVALID_ARG_VALUE, "invalid arg value",},
        { CL_INVALID_ARG_SIZE, "invalid arg size",},
        { CL_INVALID_KERNEL_ARGS, "invalid kernel args",},
        { CL_INVALID_WORK_DIMENSION, "invalid work dimension",},
        { CL_INVALID_WORK_GROUP_SIZE, "invalid work group size",},
        { CL_INVALID_WORK_ITEM_SIZE, "invalid work item size",},
        { CL_INVALID_GLOBAL_OFFSET, "invalid global offset",},
        { CL_INVALID_EVENT_WAIT_LIST, "invalid event wait list",},
        { CL_INVALID_EVENT, "invalid event",},
        { CL_INVALID_OPERATION, "invalid operation",},
        { CL_INVALID_GL_OBJECT, "invalid gl object",},
        { CL_INVALID_BUFFER_SIZE, "invalid buffer size",},
        { CL_INVALID_MIP_LEVEL, "invalid mip level",},
        { 0, NULL },
   };
   static char unknown[25];
   int ii;

   for (ii = 0; error_table[ii].msg != NULL; ii++) {
      if (error_table[ii].code == status) {
         return error_table[ii].msg;
      }
   }

   snprintf(unknown, sizeof unknown, "unknown error %d", status);
   return unknown;
}

void preval_jacobian(cl_mem d_J,cl_mem d_V1x,cl_mem d_V1y,cl_mem d_V1z, 
                                  cl_mem d_V2x,cl_mem d_V2y,cl_mem d_V2z,
                                  cl_mem d_V3x,cl_mem d_V3y,cl_mem d_V3z,
                                  cl_mem d_V4x,cl_mem d_V4y,cl_mem d_V4z){    

    CL_FUNCTION(clSetKernelArg,(kernel_preval_jacobian,  0, sizeof(cl_mem), (void *)&d_J));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_jacobian,  1, sizeof(cl_mem), (void *)&d_V1x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_jacobian,  2, sizeof(cl_mem), (void *)&d_V1y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_jacobian,  3, sizeof(cl_mem), (void *)&d_V1z));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_jacobian,  4, sizeof(cl_mem), (void *)&d_V2x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_jacobian,  5, sizeof(cl_mem), (void *)&d_V2y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_jacobian,  6, sizeof(cl_mem), (void *)&d_V2z));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_jacobian,  7, sizeof(cl_mem), (void *)&d_V3x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_jacobian,  8, sizeof(cl_mem), (void *)&d_V3y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_jacobian,  9, sizeof(cl_mem), (void *)&d_V3z));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_jacobian, 10, sizeof(cl_mem), (void *)&d_V4x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_jacobian, 11, sizeof(cl_mem), (void *)&d_V4y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_jacobian, 12, sizeof(cl_mem), (void *)&d_V4z));

    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_preval_jacobian, 1, NULL, 
                                    &global_n_threads_elem, &n_threads, 0, NULL, NULL));
}

void preval_side_area(cl_mem d_s_area,cl_mem d_s_V1x,cl_mem d_s_V1y,cl_mem d_s_V1z,
                                      cl_mem d_s_V2x,cl_mem d_s_V2y,cl_mem d_s_V2z,
                                      cl_mem d_s_V3x,cl_mem d_s_V3y,cl_mem d_s_V3z){    

    CL_FUNCTION(clSetKernelArg,(kernel_preval_side_area, 0, sizeof(cl_mem), (void *)&d_s_area));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_side_area, 1, sizeof(cl_mem), (void *)&d_s_V1x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_side_area, 2, sizeof(cl_mem), (void *)&d_s_V1y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_side_area, 3, sizeof(cl_mem), (void *)&d_s_V1z));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_side_area, 4, sizeof(cl_mem), (void *)&d_s_V2x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_side_area, 5, sizeof(cl_mem), (void *)&d_s_V2y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_side_area, 6, sizeof(cl_mem), (void *)&d_s_V2z));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_side_area, 7, sizeof(cl_mem), (void *)&d_s_V3x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_side_area, 8, sizeof(cl_mem), (void *)&d_s_V3y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_side_area, 9, sizeof(cl_mem), (void *)&d_s_V3z));

    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_preval_side_area, 1, NULL, 
                                    &global_n_threads_sides, &n_threads, 0, NULL, NULL));
}

void preval_min_height(cl_mem d_h,cl_mem d_J,cl_mem d_s_area, 
                                  cl_mem d_elem_s1,cl_mem d_elem_s2,
                                  cl_mem d_elem_s3,cl_mem d_elem_s4){ 

    CL_FUNCTION(clSetKernelArg,(kernel_preval_min_height,  0, sizeof(cl_mem), (void *)&d_h));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_min_height,  1, sizeof(cl_mem), (void *)&d_J));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_min_height,  2, sizeof(cl_mem), (void *)&d_s_area));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_min_height,  3, sizeof(cl_mem), (void *)&d_elem_s1));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_min_height,  4, sizeof(cl_mem), (void *)&d_elem_s2));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_min_height,  5, sizeof(cl_mem), (void *)&d_elem_s3));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_min_height,  6, sizeof(cl_mem), (void *)&d_elem_s4));
    

    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_preval_min_height, 1, NULL, 
                                    &global_n_threads_elem, &n_threads, 0, NULL, NULL));
}

void preval_normals(cl_mem d_Nx,cl_mem d_Ny,cl_mem d_Nz,
                    cl_mem d_s_V1x,cl_mem d_s_V1y,cl_mem d_s_V1z,
                    cl_mem d_s_V2x,cl_mem d_s_V2y,cl_mem d_s_V2z,
                    cl_mem d_s_V3x,cl_mem d_s_V3y,cl_mem d_s_V3z){    

    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals,  0, sizeof(cl_mem), (void *)&d_Nx));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals,  1, sizeof(cl_mem), (void *)&d_Ny));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals,  2, sizeof(cl_mem), (void *)&d_Nz));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals,  3, sizeof(cl_mem), (void *)&d_s_V1x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals,  4, sizeof(cl_mem), (void *)&d_s_V1y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals,  5, sizeof(cl_mem), (void *)&d_s_V1z));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals,  6, sizeof(cl_mem), (void *)&d_s_V2x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals,  7, sizeof(cl_mem), (void *)&d_s_V2y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals,  8, sizeof(cl_mem), (void *)&d_s_V2z));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals,  9, sizeof(cl_mem), (void *)&d_s_V3x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals, 10, sizeof(cl_mem), (void *)&d_s_V3y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals, 11, sizeof(cl_mem), (void *)&d_s_V3z));

    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_preval_normals, 1, NULL, 
                                    &global_n_threads_sides, &n_threads, 0, NULL, NULL));
}

void preval_normals_direction(cl_mem d_Nx,cl_mem d_Ny,cl_mem d_Nz,
                              cl_mem d_V1x,cl_mem d_V1y,cl_mem d_V1z, 
                              cl_mem d_V2x,cl_mem d_V2y,cl_mem d_V2z,
                              cl_mem d_V3x,cl_mem d_V3y,cl_mem d_V3z,
                              cl_mem d_V4x,cl_mem d_V4y,cl_mem d_V4z,
                              cl_mem d_left_elem,cl_mem d_left_side_number){    

    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals_direction,  0, sizeof(cl_mem), (void *)&d_Nx));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals_direction,  1, sizeof(cl_mem), (void *)&d_Ny));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals_direction,  2, sizeof(cl_mem), (void *)&d_Nz));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals_direction,  3, sizeof(cl_mem), (void *)&d_V1x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals_direction,  4, sizeof(cl_mem), (void *)&d_V1y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals_direction,  5, sizeof(cl_mem), (void *)&d_V1z));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals_direction,  6, sizeof(cl_mem), (void *)&d_V2x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals_direction,  7, sizeof(cl_mem), (void *)&d_V2y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals_direction,  8, sizeof(cl_mem), (void *)&d_V2z));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals_direction,  9, sizeof(cl_mem), (void *)&d_V3x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals_direction, 10, sizeof(cl_mem), (void *)&d_V3y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals_direction, 11, sizeof(cl_mem), (void *)&d_V3z));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals_direction, 12, sizeof(cl_mem), (void *)&d_V4x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals_direction, 13, sizeof(cl_mem), (void *)&d_V4y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals_direction, 14, sizeof(cl_mem), (void *)&d_V4z));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals_direction, 15, sizeof(cl_mem), (void *)&d_left_elem));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_normals_direction, 16, sizeof(cl_mem), (void *)&d_left_side_number));

    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_preval_normals_direction, 1, NULL, 
                                    &global_n_threads_sides, &n_threads, 0, NULL, NULL));
}

void preval_partials(cl_mem d_xr,cl_mem d_yr,cl_mem d_zr,
                     cl_mem d_xs,cl_mem d_ys,cl_mem d_zs,
                     cl_mem d_xt,cl_mem d_yt,cl_mem d_zt,
                     cl_mem d_V1x,cl_mem d_V1y,cl_mem d_V1z, 
                     cl_mem d_V2x,cl_mem d_V2y,cl_mem d_V2z,
                     cl_mem d_V3x,cl_mem d_V3y,cl_mem d_V3z,
                     cl_mem d_V4x,cl_mem d_V4y,cl_mem d_V4z){    

    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials,  0, sizeof(cl_mem), (void *)&d_V1x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials,  1, sizeof(cl_mem), (void *)&d_V1y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials,  2, sizeof(cl_mem), (void *)&d_V1z));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials,  3, sizeof(cl_mem), (void *)&d_V2x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials,  4, sizeof(cl_mem), (void *)&d_V2y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials,  5, sizeof(cl_mem), (void *)&d_V2z));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials,  6, sizeof(cl_mem), (void *)&d_V3x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials,  7, sizeof(cl_mem), (void *)&d_V3y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials,  8, sizeof(cl_mem), (void *)&d_V3z));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials,  9, sizeof(cl_mem), (void *)&d_V4x));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials, 10, sizeof(cl_mem), (void *)&d_V4y));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials, 11, sizeof(cl_mem), (void *)&d_V4z));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials, 12, sizeof(cl_mem), (void *)&d_xr));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials, 13, sizeof(cl_mem), (void *)&d_yr));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials, 14, sizeof(cl_mem), (void *)&d_zr));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials, 15, sizeof(cl_mem), (void *)&d_xs));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials, 16, sizeof(cl_mem), (void *)&d_ys));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials, 17, sizeof(cl_mem), (void *)&d_zs);   ) 
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials, 18, sizeof(cl_mem), (void *)&d_xt));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials, 19, sizeof(cl_mem), (void *)&d_yt));
    CL_FUNCTION(clSetKernelArg,(kernel_preval_partials, 20, sizeof(cl_mem), (void *)&d_zt));
                     
    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_preval_partials, 1, NULL, 
                                    &global_n_threads_elem, &n_threads, 0, NULL, NULL));
}


void initial_conditions(cl_mem d_c,cl_mem d_J,
                        cl_mem d_V1x,cl_mem d_V1y,cl_mem d_V1z, 
                        cl_mem d_V2x,cl_mem d_V2y,cl_mem d_V2z,
                        cl_mem d_V3x,cl_mem d_V3y,cl_mem d_V3z,
                        cl_mem d_V4x,cl_mem d_V4y,cl_mem d_V4z){    

    CL_FUNCTION(clSetKernelArg,(kernel_initial_conditions,  0, sizeof(cl_mem), (void *)&d_c));
    CL_FUNCTION(clSetKernelArg,(kernel_initial_conditions,  1, sizeof(cl_mem), (void *)&d_J));
    CL_FUNCTION(clSetKernelArg,(kernel_initial_conditions,  2, sizeof(cl_mem), (void *)&d_V1x));
    CL_FUNCTION(clSetKernelArg,(kernel_initial_conditions,  3, sizeof(cl_mem), (void *)&d_V1y));
    CL_FUNCTION(clSetKernelArg,(kernel_initial_conditions,  4, sizeof(cl_mem), (void *)&d_V1z));
    CL_FUNCTION(clSetKernelArg,(kernel_initial_conditions,  5, sizeof(cl_mem), (void *)&d_V2x));
    CL_FUNCTION(clSetKernelArg,(kernel_initial_conditions,  6, sizeof(cl_mem), (void *)&d_V2y));
    CL_FUNCTION(clSetKernelArg,(kernel_initial_conditions,  7, sizeof(cl_mem), (void *)&d_V2z));
    CL_FUNCTION(clSetKernelArg,(kernel_initial_conditions,  8, sizeof(cl_mem), (void *)&d_V3x));
    CL_FUNCTION(clSetKernelArg,(kernel_initial_conditions,  9, sizeof(cl_mem), (void *)&d_V3y));
    CL_FUNCTION(clSetKernelArg,(kernel_initial_conditions, 10, sizeof(cl_mem), (void *)&d_V3z));
    CL_FUNCTION(clSetKernelArg,(kernel_initial_conditions, 11, sizeof(cl_mem), (void *)&d_V4x));
    CL_FUNCTION(clSetKernelArg,(kernel_initial_conditions, 12, sizeof(cl_mem), (void *)&d_V4y));
    CL_FUNCTION(clSetKernelArg,(kernel_initial_conditions, 13, sizeof(cl_mem), (void *)&d_V4z));

    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_initial_conditions, 1, NULL, 
                                    &global_n_threads_elem, &n_threads, 0, NULL, NULL));
}

void eval_u(cl_mem *d_c, cl_mem *d_Uv1, cl_mem *d_Uv2, cl_mem *d_Uv3, cl_mem *d_Uv4, int *n){

    CL_FUNCTION(clSetKernelArg,(kernel_eval_u, 0, sizeof(cl_mem), (void *)d_c));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_u, 1, sizeof(cl_mem), (void *)d_Uv1));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_u, 2, sizeof(cl_mem), (void *)d_Uv2));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_u, 3, sizeof(cl_mem), (void *)d_Uv3));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_u, 4, sizeof(cl_mem), (void *)d_Uv4));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_u, 5, sizeof(int), (void *)n));
    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_eval_u, 1, NULL, 
                                    &global_n_threads_elem, &n_threads, 0, NULL, NULL));
}

void limit_c(cl_mem *d_c){    

    CL_FUNCTION(clSetKernelArg,(kernel_limit_c, 0, sizeof(cl_mem), (void *)d_c));
    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_limit_c, 1, NULL, 
                                    &global_n_threads_elem, &n_threads, 0, NULL, NULL));
}

void check_physical(cl_mem *d_c){   

    CL_FUNCTION(clSetKernelArg,(kernel_check_physical, 0, sizeof(cl_mem), (void *)d_c));
    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_check_physical, 1, NULL, 
                                    &global_n_threads_elem, &n_threads, 0, NULL, NULL));
}

void eval_global_lambda_h(double t){

    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h, 15, sizeof(double), (void *)&t));
    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_eval_global_lambda_h, 1, NULL, 
                                    &global_n_threads_elem, &n_threads, 0, NULL, NULL));
}

void eval_surface(cl_mem *d_k, double t){    

    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface,  0, sizeof(cl_mem), (void *)d_k));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface, 24, sizeof(double), (void *)&t));
    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_eval_surface, 1, NULL, 
                                    &global_n_threads_sides, &n_threads, 0, NULL, NULL));
}

void eval_volume(cl_mem *d_k, double t){    
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume,  0, sizeof(cl_mem), (void *)d_k));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume, 24, sizeof(double), (void *)&t));
    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_eval_volume, 1, NULL, 
                                    &global_n_threads_sides, &n_threads, 0, NULL, NULL));
}

void eval_rhs(cl_mem *d_k, double dt){    

    CL_FUNCTION(clSetKernelArg,(kernel_eval_rhs,  0, sizeof(cl_mem), (void *)d_k); ) 
    CL_FUNCTION(clSetKernelArg,(kernel_eval_rhs, 10, sizeof(double), (void *)&dt));
    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_eval_rhs, 1, NULL, 
                                    &global_n_threads_elem, &n_threads, 0, NULL, NULL));
}

void rk_add(cl_mem *d_kstar, cl_mem *d_c,cl_mem *d_k, double a){    

    CL_FUNCTION(clSetKernelArg,(kernel_rk_add, 0, sizeof(cl_mem), (void *)d_kstar));
    CL_FUNCTION(clSetKernelArg,(kernel_rk_add, 1, sizeof(cl_mem), (void *)d_c));
    CL_FUNCTION(clSetKernelArg,(kernel_rk_add, 2, sizeof(cl_mem), (void *)d_k));
    CL_FUNCTION(clSetKernelArg,(kernel_rk_add, 3, sizeof(double), (void *)&a));
    CL_FUNCTION(clEnqueueNDRangeKernel,(command_queue, kernel_rk_add, 1, NULL, 
                                    &global_n_threads_elem, &n_threads, 0, NULL, NULL));
}



void arraytostring(char *string, char *type, double *data, int size) {

    int i;
    char temp[100];
     
    strcpy(string,type);
    sprintf(temp,"[] = {%.025f", data[0]);
    strcat(string,temp); 
    
    for (i = 1; i < size; i++) {
        sprintf(temp,",%.025f", data[i]);
        strcat(string,temp);
    }

    sprintf(temp,"}; \n");
    strcat(string,temp);
}

void arraytostring_int(char *string, char *type, int *data, int size) {

    int i;
    char temp[100];
    
    strcpy(string,type);
    sprintf(temp,"[] = {%i", data[0]);
    strcat(string,temp); 
    
    for (i = 1; i < size; i++) {
        sprintf(temp,",%i", data[i]);
        strcat(string,temp);
    }

    sprintf(temp,"}; \n");
    strcat(string,temp);
}

void init_OpenCL(char *flux_CL_filename, char *ansatz_CL_filename, int *cpu_switch,
                    int NP, int num_elem, int num_sides, 
                    int n_quad3d, int n_quad2d, double *basis,
                    double *basis_grad_x, double *basis_grad_y, double *basis_grad_z,
                    double *basis_side, double *basis_vertex,
                    double *r3, double *s3, double *t3, double *w3,
                    double *r2, double *s2, double *w2, int *rotation_shift) {

	// Load the kernel source code into the array source
    FILE *fp;
    char *source_basis, *source_constants, *source_ansatz, *source_flux, *source_kernels;
    char *source; 
    size_t source_size;

    
    cl_platform_id platform_id = NULL;
    cl_device_id device_id = NULL;   
    cl_uint ret_num_platforms, numDevices;
    
    // Get platform and device information
    CL_FUNCTION(clGetPlatformIDs,(1, &platform_id, &ret_num_platforms));
    if (*cpu_switch) {
        printf("Choosing CPU as default device.\n");
        CL_FUNCTION(clGetDeviceIDs,(platform_id, CL_DEVICE_TYPE_CPU, 1, &device_id, &numDevices));
    } else {
        status = clGetDeviceIDs( platform_id, CL_DEVICE_TYPE_GPU, 1, &device_id, &numDevices);   
        if (status == CL_DEVICE_NOT_FOUND){ //no GPU available.
            printf("No GPU device available.\n");
            printf("Choosing CPU as default device.\n");
            *cpu_switch = 1;
            CL_FUNCTION(clGetDeviceIDs,(platform_id, CL_DEVICE_TYPE_CPU, 1, &device_id, &numDevices));
        }
        CL_CHECK_ERROR(status, "clGetDeviceIDs");
    }
  
  
    // Create an OpenCL context
    context = CL_OBJECT(clCreateContext,(NULL, 1, &device_id, NULL, NULL, &status));
    // Create a command queue
    command_queue = CL_OBJECT(clCreateCommandQueueWithProperties,(context, device_id, 0, &status));
  
    source_constants = (char*)malloc(MAX_SOURCE_SIZE);
    source_basis = (char*)malloc(MAX_SOURCE_SIZE);

	//Adds constant declarations to begining of kernel source file. 
	//This way they are constant at compile time. 
	sprintf(source_constants, "#define M (%d)    							  					    \n"
							  "#define NP (%d)  												    \n"
					   		  "#define num_elem (%d)									            \n"
					   		  "#define num_sides (%d)	      								        \n"
					   	 	  "#define n_quad3d (%d)           										\n"
					   		  "#define n_quad2d (%d)		             							\n"
					   		,M, NP, num_elem, num_sides, n_quad3d, n_quad2d);
	
	arraytostring(source_basis, "__constant double basis", basis, n_quad3d*NP);
	strcat(source_constants, source_basis); 
	arraytostring(source_basis, "__constant double basis_grad_x", basis_grad_x, n_quad3d*NP);
	strcat(source_constants, source_basis); 
	arraytostring(source_basis, "__constant double basis_grad_y", basis_grad_y, n_quad3d*NP);
	strcat(source_constants, source_basis);
    arraytostring(source_basis, "__constant double basis_grad_z", basis_grad_z, n_quad3d*NP);
    strcat(source_constants, source_basis);  
	arraytostring(source_basis, "__constant double basis_side", basis_side, 4*n_quad2d * NP);
	strcat(source_constants, source_basis); 
	arraytostring(source_basis, "__constant double basis_vertex", basis_vertex, 4* NP);
	strcat(source_constants, source_basis); 
	arraytostring(source_basis, "__constant double w3", w3, n_quad3d);
	strcat(source_constants, source_basis); 
	arraytostring(source_basis, "__constant double r3", r3, n_quad3d);
	strcat(source_constants, source_basis); 
	arraytostring(source_basis, "__constant double s3", s3, n_quad3d);
	strcat(source_constants, source_basis); 
    arraytostring(source_basis, "__constant double t3", t3, n_quad3d);
    strcat(source_constants, source_basis); 
	arraytostring(source_basis, "__constant double r2", r2, n_quad2d);
	strcat(source_constants, source_basis); 
    arraytostring(source_basis, "__constant double s2", s2, n_quad2d);
    strcat(source_constants, source_basis); 
    arraytostring(source_basis, "__constant double w2", w2, n_quad2d);
    strcat(source_constants, source_basis); 
    arraytostring_int(source_basis, "__constant int rotation_shift", rotation_shift, 3*n_quad2d);
    strcat(source_constants, source_basis); 


	//Read in Initial and boundary condition cl file
	source_ansatz = (char*)malloc(MAX_SOURCE_SIZE*sizeof(char));
	fp = fopen(ansatz_CL_filename, "r");
    if (!fp) {
        printf("Failed to load kernel %s.\n", ansatz_CL_filename);
        exit(1);
    }
    fread(source_ansatz, 1, MAX_SOURCE_SIZE, fp);
    fclose(fp);
    
    //Read in Flux cl file
    source_flux = (char*)malloc(MAX_SOURCE_SIZE*sizeof(char));
    fp = fopen(flux_CL_filename, "r");
    if (!fp) {
        printf("Failed to load kernel %s.\n", flux_CL_filename);
        exit(1);
    }
    fread(source_flux, 1, MAX_SOURCE_SIZE, fp);
    
   
    //Read in kernels cl file
    source_kernels = (char*)malloc(MAX_SOURCE_SIZE*sizeof(char));
    fp = fopen("../conserv_kernels.cl", "r");
    if (!fp) {
        printf("Failed to load kernel conserv_kernels.cl.\n");
        exit(1);
    }
    fread(source_kernels, 1, MAX_SOURCE_SIZE, fp);
    fclose(fp);

		
	source = strcat(source_constants,source_ansatz);
	source = strcat(source, source_flux);
	source = strcat(source, source_kernels);
	
	source_size = strlen(source);
	
		
	// Create a program from the kernel source
    program = CL_OBJECT(clCreateProgramWithSource,(context, 1, (const char **)&source, &source_size, &status));
    
    // Build the program
    status = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    //Output build log (Useful if the build fails)
    char *log;
    log = (char*)malloc(MAX_SOURCE_SIZE*sizeof(char));   
    clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, MAX_SOURCE_SIZE*sizeof(char),log,NULL);
    //status = clGetProgramInfo (program, CL_PROGRAM_SOURCE, MAX_SOURCE_SIZE*sizeof(char),log,NULL);
    printf("%s \n",log);
    CL_CHECK_ERROR(status, "clBuildProgram");
    
    
    // Create the OpenCL kernels
    kernel_preval_jacobian  = CL_OBJECT(clCreateKernel,(program, "preval_jacobian", &status));
    kernel_preval_side_area = CL_OBJECT(clCreateKernel,(program, "preval_side_area", &status));
    kernel_preval_min_height= CL_OBJECT(clCreateKernel,(program, "preval_min_height", &status));
	kernel_preval_normals = CL_OBJECT(clCreateKernel,(program, "preval_normals", &status));
	kernel_preval_normals_direction = CL_OBJECT(clCreateKernel,(program, "preval_normals_direction", &status));
	kernel_preval_partials = CL_OBJECT(clCreateKernel,(program, "preval_partials", &status));
	kernel_initial_conditions = CL_OBJECT(clCreateKernel,(program, "initial_conditions", &status));
	kernel_eval_u = CL_OBJECT(clCreateKernel,(program, "eval_u", &status));
	kernel_limit_c = CL_OBJECT(clCreateKernel,(program, "limit_c", &status));
	kernel_eval_global_lambda_h = CL_OBJECT(clCreateKernel,(program, "eval_global_lambda_h", &status));
	kernel_eval_surface = CL_OBJECT(clCreateKernel,(program, "eval_surface", &status));
	kernel_eval_volume = CL_OBJECT(clCreateKernel,(program, "eval_volume", &status));
	kernel_eval_rhs = CL_OBJECT(clCreateKernel,(program, "eval_rhs", &status));
	kernel_rk_add = CL_OBJECT(clCreateKernel,(program, "rk_add", &status));
	kernel_check_physical = CL_OBJECT(clCreateKernel,(program, "check_physical", &status));

    if (cpu_switch){
        n_threads = 32;
    } else {
        CL_FUNCTION(clGetDeviceInfo,(device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &n_threads, NULL));
    }
    global_n_threads_elem = ((num_elem  / n_threads) + ((num_elem  % n_threads) ? 1 : 0))*n_threads;
    global_n_threads_sides = ((num_sides / n_threads) + ((num_sides % n_threads) ? 1 : 0))*n_threads;

    //free local quadrature variables
    free(basis);
    free(basis_grad_x);
    free(basis_grad_y);
    free(basis_grad_z);
    free(basis_side);
    free(basis_vertex);
    free(w3);
    free(r3);
    free(s3);
    free(t3);
    free(w2);
    free(r2);
    free(s2);
    free(rotation_shift);
}

void create_buffers(int n, int NP, int num_elem, int num_sides,
                    double *V1x, double *V1y, double *V1z,
                    double *V2x, double *V2y, double *V2z,
                    double *V3x, double *V3y, double *V3z,
                    double *V4x, double *V4y, double *V4z,
                    int *left_side_number, int *right_side_number,
                    double *sides_x1, double *sides_y1, double *sides_z1,
                    double *sides_x2, double *sides_y2, double *sides_z2,
                    double *sides_x3, double *sides_y3, double *sides_z3,
                    int *elem_s1, int *elem_s2, int *elem_s3, int *elem_s4,
                    int *left_elem, int *right_elem, int *rotation,
                    cl_mem *d_c, cl_mem *d_J, cl_mem *d_h, 
                    cl_mem *d_V1x, cl_mem *d_V1y, cl_mem *d_V1z, 
                    cl_mem *d_V2x, cl_mem *d_V2y, cl_mem *d_V2z,
                    cl_mem *d_V3x, cl_mem *d_V3y, cl_mem *d_V3z,
                    cl_mem *d_V4x, cl_mem *d_V4y, cl_mem *d_V4z,
                    cl_mem *d_s_V1x, cl_mem *d_s_V1y, cl_mem *d_s_V1z, 
                    cl_mem *d_s_V2x, cl_mem *d_s_V2y, cl_mem *d_s_V2z,
                    cl_mem *d_s_V3x, cl_mem *d_s_V3y, cl_mem *d_s_V3z,
                    cl_mem *d_s_area,
                    cl_mem *d_elem_s1, cl_mem *d_elem_s2, cl_mem *d_elem_s3, cl_mem *d_elem_s4, 
                    cl_mem *d_left_side_number, cl_mem *d_right_side_number,
                    cl_mem *d_left_elem, cl_mem *d_right_elem, cl_mem *d_rotation,
                    cl_mem *d_Nx, cl_mem *d_Ny, cl_mem *d_Nz,  
                    cl_mem *d_xr, cl_mem *d_yr, cl_mem *d_zr,  
                    cl_mem *d_xs, cl_mem *d_ys, cl_mem *d_zs,
                    cl_mem *d_xt, cl_mem *d_yt, cl_mem *d_zt) {


    double total_memory = num_elem*23*sizeof(double)  +
                   num_elem*4*sizeof(int)      +
                   num_sides*4*sizeof(double) + 
                   num_sides*5*sizeof(int)     +
                   M*num_elem*NP*2*sizeof(double) +
                   M*num_sides*NP*2*sizeof(double);

    //RK temp storage
    total_memory += (n+1)*M*num_elem*NP*sizeof(double);;
    

    if (total_memory < 1e3) {
        printf("Total memory required: %lf B\n", total_memory);
    } else if (total_memory >= 1e3 && total_memory < 1e6) {
        printf("Total memory required: %lf KB\n", total_memory * 1e-3);
    } else if (total_memory >= 1e6 && total_memory < 1e9) {
        printf("Total memory required: %lf MB\n", total_memory * 1e-6);
    } else {
        printf("Total memory required: %lf GB\n", total_memory * 1e-9);
    }

    *d_c = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, M*num_elem*NP * sizeof(double),NULL, &status));   
    *d_J = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, num_elem* sizeof(double),NULL, &status));
    *d_h = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, num_elem* sizeof(double),NULL, &status));

    *d_V1x = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_elem* sizeof(double),V1x, &status));
    *d_V1y = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_elem* sizeof(double),V1y, &status));
    *d_V1z = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_elem* sizeof(double),V1z, &status));
    *d_V2x = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_elem* sizeof(double),V2x, &status));
    *d_V2y = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_elem* sizeof(double),V2y, &status));
    *d_V2z = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_elem* sizeof(double),V2z, &status));
    *d_V3x = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_elem* sizeof(double),V3x, &status));
    *d_V3y = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_elem* sizeof(double),V3y, &status));
    *d_V3z = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_elem* sizeof(double),V3z, &status));
    *d_V4x = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_elem* sizeof(double),V4x, &status));
    *d_V4y = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_elem* sizeof(double),V4y, &status));
    *d_V4z = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_elem* sizeof(double),V4z, &status));

    *d_s_area= CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE,num_sides* sizeof(double),NULL,&status));
    *d_s_V1x = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR,num_sides* sizeof(double),sides_x1, &status));
    *d_s_V1y = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR,num_sides* sizeof(double),sides_y1, &status));
    *d_s_V1z = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR,num_sides* sizeof(double),sides_z1, &status));
    *d_s_V2x = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR,num_sides* sizeof(double),sides_x2, &status));
    *d_s_V2y = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR,num_sides* sizeof(double),sides_y2, &status));
    *d_s_V2z = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR,num_sides* sizeof(double),sides_z2, &status));
    *d_s_V3x = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR,num_sides* sizeof(double),sides_x3, &status));
    *d_s_V3y = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR,num_sides* sizeof(double),sides_y3, &status));
    *d_s_V3z = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR,num_sides* sizeof(double),sides_z3, &status));

    *d_elem_s1 = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_elem* sizeof(int),elem_s1, &status));
    *d_elem_s2 = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_elem* sizeof(int),elem_s2, &status));
    *d_elem_s3 = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_elem* sizeof(int),elem_s3, &status));
    *d_elem_s4 = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_elem* sizeof(int),elem_s4, &status));
    
    *d_left_side_number  = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_sides  * sizeof(int),left_side_number, &status));
    *d_right_side_number = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_sides  * sizeof(int),right_side_number, &status));
    *d_left_elem  = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_sides * sizeof(int),left_elem, &status));
    *d_right_elem = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_sides * sizeof(int),right_elem,&status));
    *d_rotation   = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, num_sides * sizeof(int),rotation,&status));

    *d_Nx = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE,num_sides * sizeof(double),NULL, &status));
    *d_Ny = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE,num_sides * sizeof(double),NULL, &status));
    *d_Nz = CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE,num_sides * sizeof(double),NULL, &status));
    
    *d_xr= CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, num_elem * sizeof(double),NULL, &status));
    *d_yr= CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, num_elem * sizeof(double),NULL, &status));
    *d_zr= CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, num_elem * sizeof(double),NULL, &status));
    *d_xs= CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, num_elem * sizeof(double),NULL, &status));
    *d_ys= CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, num_elem * sizeof(double),NULL, &status));
    *d_zs= CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, num_elem * sizeof(double),NULL, &status));
    *d_xt= CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, num_elem * sizeof(double),NULL, &status));
    *d_yt= CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, num_elem * sizeof(double),NULL, &status));
    *d_zt= CL_OBJECT(clCreateBuffer,(context, CL_MEM_READ_WRITE, num_elem * sizeof(double),NULL, &status));

    //These kernels are run repeatedly so we set what argumetns we can now
    CL_FUNCTION(clSetKernelArg,(kernel_limit_c, 1, sizeof(cl_mem), (void *)d_elem_s1));
    CL_FUNCTION(clSetKernelArg,(kernel_limit_c, 2, sizeof(cl_mem), (void *)d_elem_s2));
    CL_FUNCTION(clSetKernelArg,(kernel_limit_c, 3, sizeof(cl_mem), (void *)d_elem_s3));
    CL_FUNCTION(clSetKernelArg,(kernel_limit_c, 4, sizeof(cl_mem), (void *)d_elem_s4));
    CL_FUNCTION(clSetKernelArg,(kernel_limit_c, 5, sizeof(cl_mem), (void *)d_left_elem));
    CL_FUNCTION(clSetKernelArg,(kernel_limit_c, 6, sizeof(cl_mem), (void *)d_right_elem));
    
    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h,  0, sizeof(cl_mem), (void *)d_c));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h,  2, sizeof(cl_mem), (void *)d_h));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h,  3, sizeof(cl_mem), (void *)d_V1x));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h,  4, sizeof(cl_mem), (void *)d_V1y));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h,  5, sizeof(cl_mem), (void *)d_V1z));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h,  6, sizeof(cl_mem), (void *)d_V2x));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h,  7, sizeof(cl_mem), (void *)d_V2y));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h,  8, sizeof(cl_mem), (void *)d_V2z));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h,  9, sizeof(cl_mem), (void *)d_V3x));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h, 10, sizeof(cl_mem), (void *)d_V3y));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h, 11, sizeof(cl_mem), (void *)d_V3z));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h, 12, sizeof(cl_mem), (void *)d_V4x));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h, 13, sizeof(cl_mem), (void *)d_V4y));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_global_lambda_h, 14, sizeof(cl_mem), (void *)d_V4z));
    
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface,  3, sizeof(cl_mem), (void *)d_s_area));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface,  4, sizeof(cl_mem), (void *)d_V1x));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface,  5, sizeof(cl_mem), (void *)d_V1y));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface,  6, sizeof(cl_mem), (void *)d_V1z));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface,  7, sizeof(cl_mem), (void *)d_V2x));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface,  8, sizeof(cl_mem), (void *)d_V2y));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface,  9, sizeof(cl_mem), (void *)d_V2z));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface, 10, sizeof(cl_mem), (void *)d_V3x));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface, 11, sizeof(cl_mem), (void *)d_V3y));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface, 12, sizeof(cl_mem), (void *)d_V3z));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface, 13, sizeof(cl_mem), (void *)d_V4x));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface, 14, sizeof(cl_mem), (void *)d_V4y));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface, 15, sizeof(cl_mem), (void *)d_V4z));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface, 16, sizeof(cl_mem), (void *)d_left_elem));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface, 17, sizeof(cl_mem), (void *)d_right_elem));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface, 18, sizeof(cl_mem), (void *)d_left_side_number));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface, 19, sizeof(cl_mem), (void *)d_right_side_number));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface, 20, sizeof(cl_mem), (void *)d_rotation));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface, 21, sizeof(cl_mem), (void *)d_Nx));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface, 22, sizeof(cl_mem), (void *)d_Ny));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_surface, 23, sizeof(cl_mem), (void *)d_Nz));
    
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume,  2, sizeof(cl_mem), (void *)d_xr));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume,  3, sizeof(cl_mem), (void *)d_yr));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume,  4, sizeof(cl_mem), (void *)d_zr));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume,  5, sizeof(cl_mem), (void *)d_xs));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume,  6, sizeof(cl_mem), (void *)d_ys));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume,  7, sizeof(cl_mem), (void *)d_zs));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume,  8, sizeof(cl_mem), (void *)d_xt));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume,  9, sizeof(cl_mem), (void *)d_yt));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume, 10, sizeof(cl_mem), (void *)d_zt));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume, 11, sizeof(cl_mem), (void *)d_V1x));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume, 12, sizeof(cl_mem), (void *)d_V1y));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume, 13, sizeof(cl_mem), (void *)d_V1z));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume, 14, sizeof(cl_mem), (void *)d_V2x));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume, 15, sizeof(cl_mem), (void *)d_V2y));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume, 16, sizeof(cl_mem), (void *)d_V2z));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume, 17, sizeof(cl_mem), (void *)d_V3x));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume, 18, sizeof(cl_mem), (void *)d_V3y));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume, 19, sizeof(cl_mem), (void *)d_V3z));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume, 20, sizeof(cl_mem), (void *)d_V4x));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume, 21, sizeof(cl_mem), (void *)d_V4y));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume, 22, sizeof(cl_mem), (void *)d_V4z));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_volume, 23, sizeof(cl_mem), (void *)d_J));
    
    CL_FUNCTION(clSetKernelArg,(kernel_eval_rhs,  4, sizeof(cl_mem), (void *)d_elem_s1));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_rhs,  5, sizeof(cl_mem), (void *)d_elem_s2));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_rhs,  6, sizeof(cl_mem), (void *)d_elem_s3));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_rhs,  7, sizeof(cl_mem), (void *)d_elem_s4));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_rhs,  8, sizeof(cl_mem), (void *)d_left_elem));
    CL_FUNCTION(clSetKernelArg,(kernel_eval_rhs,  9, sizeof(cl_mem), (void *)d_J));
     
    CL_FUNCTION(clFinish,(command_queue));

    // no longer need any of these CPU variables
    free(elem_s1);
    free(elem_s2);
    free(elem_s3);
    free(elem_s4);
    free(sides_x1);
    free(sides_x2);
    free(sides_x3);
    free(sides_y1);
    free(sides_y2);
    free(sides_y3);
    free(sides_z1);
    free(sides_z2);
    free(sides_z3);
    free(left_elem);
    free(right_elem);
    free(left_side_number);
    free(right_side_number);
    free(rotation);
}