#!/usr/bin/python
import numpy as np
import time


"""
genmesh.py

This file is responsible for reading the contents of the .msh file 
and creating the mesh mappings for use in DGOpenCL
"""

from sys import argv

def genmesh(inFilename, outFilename):
    inFile  = open(inFilename, "rb")
    outFile = open(outFilename, "wb")

    print "Reading file: %s..." % inFilename
    line = inFile.readline()
    while "$Nodes" not in line:
        line = inFile.readline()

    # the next line is the number of vertices
    num_verticies = int(inFile.readline())
    print num_verticies, "verticies..."

    vertex_list = []
    for i in xrange(0,num_verticies):
        s = inFile.readline().split()
        vertex_list.append((float(s[1]), float(s[2]),float(s[3])))
    
    # next two lines are just filler
    inFile.readline()
    inFile.readline()

    # next line is the number of elements
    num_elements = int(inFile.readline())

    elem_list = []     #list of elements
    boundary_set = {}  #set of boundary sides
    boundaryflag = [] #type of boundary
    # add the vertices for each element into elem_list
    for i in xrange(0,num_elements):

        s = inFile.readline().split()

        # these are sides
        if len(s) == 8:
            boundary = int(s[3])
            v1 = int(s[5]) - 1
            v2 = int(s[6]) - 1
            v3 = int(s[7]) - 1

            #store the boundary side
            boundary_set[(v1,v2,v3)] = i
            boundaryflag.append(boundary)

        # and these are tetrahedrons
        if len(s) == 9:
            v1 = int(s[5]) - 1
            v2 = int(s[6]) - 1
            v3 = int(s[7]) - 1
            v4 = int(s[8]) - 1
                      
            # store the index of the verticies
            elem_list.append((v1, v2, v3, v4))

    print len(elem_list), "elements..."
    print len(boundaryflag), "boundary faces..."

    ##################################################
    # now that we've read in the verticies for the elements and sides,
    # we can begin creating our mesh
    ##################################################

    numsides = 0

    # stores the side number [0, 1, 2, 3] of the left and right element's sides
    left_side_number  = [0] * (num_elements * 4) 
    right_side_number = [0] * (num_elements * 4) 

    # stores the index of the left & right elements
    left_elem  = [0] * (num_elements * 4)
    right_elem = [0] * (num_elements * 4)

    # links to side [0, 1, 2, 3] of the element
    elem_sides = [[0] * 4 for x in xrange(0,num_elements)]

    sidelist = [0] * (num_elements * 4) #all sides
    rotation = [0] * (num_elements * 4) #all sides

    #set of previously added sides
    side_set = {}
    
    print "Creating mappings..."
    #loop over each elements
    for i, e in enumerate(elem_list):
        #loop over each side in current element
        for k, side in enumerate([(e[0], e[1], e[2]), (e[1], e[0], e[3]), (e[2], e[3],e[0]), (e[3], e[2],e[1])]):
            side_flag = 1
            # check to see if side is already added
            for perm in [(side[0],side[2],side[1]),(side[2],side[1],side[0]),(side[1],side[0],side[2])]:
                if perm in side_set:
                    side_flag = 0             # this is not a new side
                    j = side_set[perm]        # get the index of that face
                    right_elem[j] = i         # link that edge to this element
                    elem_sides[i][k] = j      # link this element to that edge
                    right_side_number[j] = k  # map this edge to the same side
                    if perm[0] == side[0]:    #the left element side 1st vertex == right element's first vertex
                        rotation[j] = 0;
                    elif perm[1] == side[0]:
                        rotation[j] = 1;
                    else:
                        rotation[j] = 2;
                    break

            # if we haven't added the side already, add it
            if side_flag == 1:
                side_set[(side[0], side[1], side[2])] = numsides
                sidelist[numsides] = (side[0], side[1], side[2])
                left_side_number[numsides] = k
                elem_sides[i][k] = numsides
                left_elem[numsides] = i

                # see if this is a boundary side
                for perm in [(side[0],side[2],side[1]),(side[2],side[1],side[0]),(side[1],side[0],side[2]),(side[0],side[1],side[2]),(side[2],side[0],side[1]),(side[1],side[2],side[0])]:
                    if perm in boundary_set:
                        j = boundary_set[perm]
                        if (boundaryflag[j] == 10000):
                            right_elem[numsides] = -1
                        if (boundaryflag[j] == 20000):
                            right_elem[numsides] = -2
                        if (boundaryflag[j] == 30000):
                            right_elem[numsides] = -3
                        break

                numsides += 1

        
                
    print numsides, "sides..."
    print "Sorting mesh..."
    # sort the mesh so that right element = -1 items are first, -2 second, -3 third
    j = 0 # location after the latest right element
    for N in [-1, -2, -3]:
        for i in xrange(0, numsides):
            if right_elem[i] == N:

                # update index for left_elem[j]
                if left_side_number[j] == 0:
                    elem_sides[left_elem[j]][0] = i
                elif left_side_number[j] == 1:
                    elem_sides[left_elem[j]][1] = i
                elif left_side_number[j] == 2:
                    elem_sides[left_elem[j]][2] = i
                elif left_side_number[j] == 3:
                    elem_sides[left_elem[j]][3] = i

                # update index for right_elem[j]
                if right_side_number[j] != -1:
                    if right_side_number[j] == 0:
                        elem_sides[right_elem[j]][0] = i
                    elif right_side_number[j] == 1:
                        elem_sides[right_elem[j]][1] = i
                    elif right_side_number[j] == 2:
                        elem_sides[right_elem[j]][2] = i
                    elif right_side_number[j] == 3:
                        elem_sides[right_elem[j]][3] = i

                # update index for left_elem[i]
                if left_side_number[i] == 0:
                    elem_sides[left_elem[i]][0] = j
                if left_side_number[i] == 1:
                    elem_sides[left_elem[i]][1] = j
                if left_side_number[i] == 2:
                    elem_sides[left_elem[i]][2] = j
                if left_side_number[i] == 3:
                    elem_sides[left_elem[i]][3] = j

                # swap sides i and j
                sidelist[i], sidelist[j] = sidelist[j], sidelist[i]
                left_elem[i] , left_elem[j]  = left_elem[j] , left_elem[i]
                right_elem[i], right_elem[j] = right_elem[j], right_elem[i]
                left_side_number[i] , left_side_number[j]  = left_side_number[j] , left_side_number[i]
                right_side_number[i], right_side_number[j] = right_side_number[j], right_side_number[i]
                rotation[i],rotation[j] = rotation[j],rotation[i];
                # increment j
                j += 1

    elem_s1 = [elem_sides[t][0] for t in xrange(0, num_elements)]
    elem_s2 = [elem_sides[t][1] for t in xrange(0, num_elements)]
    elem_s3 = [elem_sides[t][2] for t in xrange(0, num_elements)]
    elem_s4 = [elem_sides[t][3] for t in xrange(0, num_elements)]

    # write the mesh to file
    print "Writing file: %s..." % outFilename
    outFile.write(str(len(elem_list)) + "\n")
    for elem, s1, s2, s3, s4 in zip(elem_list, elem_s1, elem_s2, elem_s3, elem_s4):

        V1x = vertex_list[elem[0]][0]
        V1y = vertex_list[elem[0]][1]
        V1z = vertex_list[elem[0]][2]
        V2x = vertex_list[elem[1]][0]
        V2y = vertex_list[elem[1]][1]
        V2z = vertex_list[elem[1]][2]
        V3x = vertex_list[elem[2]][0]
        V3y = vertex_list[elem[2]][1]
        V3z = vertex_list[elem[2]][2]
        V4x = vertex_list[elem[3]][0]
        V4y = vertex_list[elem[3]][1]
        V4z = vertex_list[elem[3]][2]

        outFile.write("%.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %i %i %i %i\n" % 
                                                             (V1x, V1y, V1z, V2x, V2y, V2z, V3x, V3y, V3z, V4x, V4y, V4z,
                                                              s1, s2, s3, s4))

    outFile.write(str(numsides) + "\n")
    for i in xrange(0, numsides):
        outFile.write("%.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %i %i %i %i %i\n" % 
                                        (vertex_list[sidelist[i][0]][0], vertex_list[sidelist[i][0]][1],vertex_list[sidelist[i][0]][2],
                                         vertex_list[sidelist[i][1]][0], vertex_list[sidelist[i][1]][1],vertex_list[sidelist[i][1]][2],
                                         vertex_list[sidelist[i][2]][0], vertex_list[sidelist[i][2]][1],vertex_list[sidelist[i][2]][2],
                                         left_elem[i], right_elem[i], 
                                         left_side_number[i], right_side_number[i], rotation[i]))

    outFile.close()

if __name__ == "__main__":
    try:
        start = time.clock()
        inFilename  = argv[1] 
        outFilename = argv[2]
        genmesh(inFilename, outFilename)
        elapsed = (time.clock() - start)
        print "Elapsed time: ",elapsed
    except Exception:
        print "Usage: genmesh [infile] [outfile]"
