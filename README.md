# DGOpenCL #

An implementation of the discontinuous Galerkin finite element method using the [OpenCL](https://www.khronos.org/opencl) GPU computing language. 

### Key Features ###

* 1D, 2D (triangular cells), and 3D (tetrahedral cells) implementations
* Fully parallel on GPU/CPU
* Fork of [DGCUDA](https://bitbucket.org/mfuhry/dgcuda), an implementation of DG using Nvidia's CUDA language.  

### Installation ###

* Dependencies
    * OpenCL ([AMD APP](http://developer.amd.com/tools-and-sdks/opencl-zone/amd-accelerated-parallel-processing-app-sdk))
    * [GMSH](http://geuz.org/gmsh)
    * Python + NumPy
* Install dependencies using 
    `sudo apt-get install python python-numpy gmsh`

* Install AMD APP SDK via http://developer.amd.com/tools-and-sdks/opencl-zone/amd-accelerated-parallel-processing-app-sdk/ and assign enviroment variable `$AMDAPPSDKROOT`


### Usage ###

We will use a simple linear advection problem for an example use. This example is located in `dgopencl/src/2d/advection/` and `dgopencl/src/2d/advection/linear`. 

First, the mesh geometry is scripted via a `.geo` file e.g. `rh.geo` in `linear/mesh/`. The geometry is then meshed using GMSH and outputted to a `.msh` file

    gmsh -2 rh.geo

The `rh.msh` file is then pre-processed using the python script `genmesh.py` located in `dgopencl/src/2d/`, and outputted to a `.pmsh` file. 

    python genmesh.py advection/linear/mesh/rh.msh advection/linear/mesh/rh.pmsh

The executable is then built in `dgopencl/src/2d/advection`

    make dglinear

and the code is run 

    ./dglilnear -n 1 -T 0.5 linear/mesh/rh.pmsh

This line runs `dglinear` with `n=1` to a final time of `T=0.5`.